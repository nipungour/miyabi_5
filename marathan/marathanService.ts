import { HeaderProvider } from './../header/header';
import { Injectable } from '@angular/core';
import { environment } from '../src/environments/environment';
import { Http } from '@angular/http';

@Injectable()
export class MarathanProvider {
  public headers;
  timeZone: any;
  messagesdata: any;
  constructor(public http: Http,
    public authorizeHeader: HeaderProvider) {
    this.timeZone = (new Date()).getTimezoneOffset();
    // console.log('Hello MarathanProvider Provider');
  }

  getMarathanList(event) {
    return this.http.get(environment.baseUrl + 'Marathon/GetMarathons' + '?pageSize=' + event.pageSize +
      '&pageIndex=' + event.pageIndex + '&timeZoneOffSet=' + this.timeZone, { headers: this.authorizeHeader.Headers() });
  }

  getOrderList() {
    return this.http.get(environment.baseUrl + 'order/GetUserOrders' + '?timeZoneOffSet=' + this.timeZone, { headers: this.authorizeHeader.Headers() });
  }

  getUserOrderList() {
    alert('user login')
    const res= this.http.get(environment.baseUrl + 'Order/GetUserOrders' + '?timeZoneOffSet=' + this.timeZone, { headers: this.authorizeHeader.Headers() });
    console.log("res",res)
    return res;
  }
  // getmarathonextensiondetails
  getExtensionDetail(id) {
    return this.http.get(environment.baseUrl + 'Marathon/ExtensionDescription' + '?marathonId=' + id, { headers: this.authorizeHeader.Headers() });
  }

  //  start marathon page 
  getUserMarathonDetail(id) {
    return this.http.get(environment.baseUrl + 'UserMarathon/StartMarathon' + '?marathonId=' + id + '&timeZoneOffSet=' + this.timeZone,
      { headers: this.authorizeHeader.Headers() });
      console.log("hello",id);
  }



  // get marathon day excercise and detail;
  getUserDayExercise(id) {
    let marathanId = localStorage.getItem("marathonId");
    return this.http.get(environment.baseUrl + 'UserMarathon/GetDayExercise' + '?marathonId=' + marathanId + '&dayId=' + id + '&timeZoneOffSet=' + this.timeZone, { headers: this.authorizeHeader.Headers() }).toPromise();

  }

  getUserNotification(id) {
    return this.http.get(environment.baseUrl + 'Notification/GetExerciseNotification?dayId=' + id, { headers: this.authorizeHeader.Headers() });
  }

  //  SET EXERCISE STATUS IF USER HAVE MARKED IT COMPLETED (CHECK MARK) ================

  exerciseStatus(data) {
    return this.http.post(environment.baseUrl + 'UserMarathon/SetUserExerciseStatus', data,
      { headers: this.authorizeHeader.Headers() }).toPromise();
  }

  /**get comments lists in miyabi marathon */
  getMarathoncomment(id, MarathonId, extension) {
    return this.http.get(environment.baseUrl + 'UserMarathon/GetComments' + '?exerciseId=' + id + '&marathonId=' +
      MarathonId + '&isExtension=' + extension + '&timeZoneOffSet=' + this.timeZone, { headers: this.authorizeHeader.Headers() }).toPromise();

  }

  changeMessageStatus(data) {
    return this.http.post(environment.baseUrl + 'Marathon/UpdateCommentStatus', data, { headers: this.authorizeHeader.Headers() }).toPromise();

  }

  getChildCmnt(event, isExtension) {

    return this.http.get(environment.baseUrl + 'UserMarathon/GetChildComments' + '?commentId=' + event.commentId +
      '&exerciseId=' + event.exerciseId + '&marathonId=' + event.marathonId + '&pageSize=' + event.pageSize + '&pageIndex=' +
      event.pageIndex + '&timeZoneOffSet=' + this.timeZone + '&isExtension=' + isExtension, { headers: this.authorizeHeader.Headers() }).toPromise();
  }

  /**post comment of user in miyabi admin */
  commentMessage(data) {
    return this.http.post(environment.baseUrl + 'UserMarathon/CreateComment', data, { headers: this.authorizeHeader.Headers() }).toPromise();

  }

  getSuggestion(comment) {
    return this.http.get(environment.baseUrl + 'marathon/GetQuestionUsingComment?comment=' + comment, { headers: this.authorizeHeader.Headers() });
  }

  readNotification(data) {
    return this.http.post(environment.baseUrl + 'Notification/SaveUserExerciseNotificationState', data,
      { headers: this.authorizeHeader.Headers() });
  }

  GetMarathons() {
    return this.http.get(environment.baseUrl + 'UserMarathon/GetMarathons' + '?timeZoneOffSet=' + this.timeZone);
  }

  messages(data) {
    this.messagesdata = data
  }

  messagesdatasend() {

    return this.messagesdata
  }

  getWelcomeMessage(id) {
    return this.http.get(environment.baseUrl + 'Marathon/GetMarathonWelcomeMessage' + '?marathonId=' + id + '&timeZoneOffSet=' + this.timeZone, { headers: this.authorizeHeader.Headers() })

  }

  createOrderForTransaction(id) {
    return this.http.get(environment.baseUrl + 'Order/CreateOrder' + '?marathonId=' + id, { headers: this.authorizeHeader.Headers() });
  }

  CheckForMarathonPurchased() {

    return this.http.get(environment.baseUrl + 'Order/CheckForMarathonPurchased' + '?timeZoneOffSet=' + this.timeZone,
      { headers: this.authorizeHeader.Headers() });
  }

  editprofile(editprofiledata) {
    console.log("editprofiledataeditprofiledataAtService", editprofiledata);
    return this.http.post(environment.baseUrl + 'Admin/UpdateProfile', editprofiledata, { headers: this.authorizeHeader.Headers() });
  }


  GetUserProfileDetail() {
    return this.http.get(environment.baseUrl + 'User/GetUserProfileDetail', { headers: this.authorizeHeader.Headers() })
  }
  UpdateProfile(data) {

    return this.http.post(environment.baseUrl + 'User/UpdateProfile', data, { headers: this.authorizeHeader.createHeaderForMarathon() });
  }

  changepassword(changepassworddata) {
    return this.http.post(environment.baseUrl + 'User/UserChangepassword', changepassworddata, { headers: this.authorizeHeader.Headers() });
  }


  getMarathonTitle(id) {
    return this.http.get(environment.baseUrl + 'Marathon/GetMarathon?id=' + id + '&timeZoneOffSet=' + this.timeZone, { headers: this.authorizeHeader.Headers() })

  }


  checkDiscount(orderNumber, couponCode) {
    return this.http.get(environment.baseUrl + 'order/CheckDiscount?orderNumber=' + orderNumber + '&couponCode=' + couponCode + '&timeZoneOffSet=' + this.timeZone, { headers: this.authorizeHeader.Headers() });
  }

  PaymentDetail(id) {
    return this.http.get(environment.baseUrl + 'Marathon/ExtensionDescription' + '?marathonid=' + id, { headers: this.authorizeHeader.Headers() });
  }
  purchaseMarathon(orderNumber, couponCode) {
    return this.http.get(environment.baseUrl + 'order/PurchaseMarathon?orderNumber=' + orderNumber + '&couponCode=' + couponCode + '&timeZoneOffSet=' + this.timeZone, { headers: this.authorizeHeader.Headers() });
  }

  CheckSelfMarathonPurchased(id) {
    return this.http.get(environment.baseUrl + 'Order/CheckSelfMarathonPurchased?marathanId=' + id, { headers: this.authorizeHeader.Headers() });
  }

  particpentStatus(data) {
    return this.http.get(environment.baseUrl + 'Contest/TakePartinContest?marathonId=' + data.marathonId + '&isContestParticipated=' + data.isContestParticipated, { headers: this.authorizeHeader.Headers() }).toPromise()
  }
  OrderAsGift(orderNumber, isGift) {
    return this.http.get(environment.baseUrl + 'order/OrderAsGift?orderNumber=' + orderNumber + '&isGift=' + isGift, { headers: this.authorizeHeader.Headers() });
  }


  getCoupon(orderNumber) {
    return this.http.get(environment.baseUrl + 'order/GetCoupoun?orderNumber=' + orderNumber, { headers: this.authorizeHeader.Headers() });
  }

  getGeneralSetting() {
    return this.http.get(environment.baseUrl + 'GeneralSetting/GetGeneralSetting');
  }

  DownloadUserImage(id) {
    return this.http.get(environment.baseUrl + 'Contest/DownloadcollageImageForUser?marathonId=' + id, { headers: this.authorizeHeader.Headers() })
  }


}
