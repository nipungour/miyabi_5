import { Injectable } from '@angular/core'; 
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { Http } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';
import { Platform } from '@ionic/angular';
import { tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  isLoggedIn = false;
  token:any;
 constructor( 
   private storage: Storage,
   public http: Http ,
   public platform: Platform) { }


  login(username: string, password: string, grant_type: string, refresh_token?) {
    return this.http.post(environment.baseUrl + 'Token/auth', { username: username, password: password,
      grant_type: grant_type,refresh_token } )
      .pipe(
        tap(token => {
          this.storage.set('token', token)
          .then(
            () => {
              console.log('Token Stored');
            },
            error => console.error('Error storing item', error)
          );
          this.token = token;
          this.isLoggedIn = true;
          return token;
        }),
      );
  }

  
  UserRegister(data){
    return this.http.post(environment.baseUrl + 'User/UserRegister', data);
    }
    
    // user() {
    //   const headers = new HttpHeaders({
    //     'Authorization': this.token["token_type"]+" "+this.token["access_token"]
    //   });
    //   return this.http.get<User>(environment.baseUrl + 'auth/user', { headers: headers })
    //   .pipe(
    //     tap(user => {
    //       return user ;
    //     })
    //   )
    // }

    getToken() {
      return this.storage.get('token').then(
        data => {
          this.token = data;
          if(this.token != null) {
            this.isLoggedIn=true;
          } else {
            this.isLoggedIn=false;
          }
        },
        error => {
          this.token = null;
          this.isLoggedIn=false;
        }
      );
    } 
    
}


