import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StartmarathonPageRoutingModule } from './startmarathon-routing.module';
import { StartmarathonPage } from './startmarathon.page';


@NgModule({
  imports: [
  
  CommonModule,
    FormsModule,
    IonicModule,
    StartmarathonPageRoutingModule,
   
  ],
  declarations: [StartmarathonPage]
})
export class StartmarathonPageModule {}
