import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StartmarathonPage } from './startmarathon.page';

describe('StartmarathonPage', () => {
  let component: StartmarathonPage;
  let fixture: ComponentFixture<StartmarathonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartmarathonPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StartmarathonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
