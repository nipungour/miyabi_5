import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { LanguageServiceProvider } from '../language-service/language-service';
import { MarathanProvider } from '../../../marathan/marathanService';
import { NavController,MenuController,NavParams, AlertController } from '@ionic/angular';
import { UsermarathonPage } from '../usermarathon/usermarathon.page';
import { DomSanitizer } from '@angular/platform-browser';
import { OrderlistPage } from '../orderlist/orderlist.page';
@Component({
  selector: 'app-startmarathon',
  templateUrl: './startmarathon.page.html',
  styleUrls: ['./startmarathon.page.scss'],
})
export class StartmarathonPage {

  public iserror;
  public exerciseSucess;
  public exerciseError;
  public show;

  monthDay:any;
  daydata:any;
  daydetails:any;
  day = [];
  dayColor:any;
  color:any;
  user_marathan_id:any;
  languageType:any;
  langCode:any;
  loader:any; 
  checkfinalmessages:boolean;
  newFinalMessageArray = [];
  pagehead: boolean;
  id:any;
  data: any;
  today: number = Date.now();
  closetime: boolean;
  website_error;
  showContent: boolean;
  videoUrl: any;
  reversArray: any;
  public sampledata = [];
  public emptyCol: string
  firstDayId: any;
  countDownDate: number;
  now: number;
  distance: number;
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
  split: any;
  countDown: boolean;
  texterror: any;
  dayInWeek = [];
  daysOfWeek = [];
  MarathonName: any;
  splitString: any;
  mDta: any;
  marathanDays = [];
  windowclose: any;
  role: any
  exercise_daily_error: any;
  responce: any;
  finalMessageData: any[] = [];
  isActive: boolean;
  datashoe: any;
  title: any;
  Subtitle: any;
  rules: any;
  showfinalmessgas: boolean;
  showtime: boolean;
  welcomeMessage: any;
  orderId: any;
  showOrderDetailMessage: boolean;
  orderDetailMessage: any;
  extension: any;
  firstDayIdGreatExtension: any;
  sampledataGreatExtension: any;
  isCourse: string
  daysOfWeekGreatExtension = [];
  dayGreatExtension = [];
  isGreatExtension: string;
  showwelcomeMessage: any;
  Rules: any;
  isContest: any;
  marathonId: any;
  paymet: any;
  finalDescription: any;
  description: any;
  extensiondetail: any;
  titleLength: boolean = false;
   constructor(public authService: AuthenticationService, public alrtCtrl: AlertController,public navParams: NavParams,public sanitizer: DomSanitizer,private router: Router ,private languageService:LanguageServiceProvider,private navCtrl:NavController,public marathanService: MarathanProvider,private menuCtrl:MenuController) 
      //<......static data days
     {
      this.getdays();
      // this.monthDay=['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','19','20','21',];
      this.monthDay= [{ 'week' : 2 , 'days' :[21,20,19,18,17,16,15]},{ 'week' : 1 , 'days' :[14,13,12,11,10,9,8]},{ 'week' : 0 , 'days' :[7,6,5,4,3,2,1]}];
      this.dayColor=[ '#a9cedc', '#a9cedc', '#b6bbdd' , '#b6bbdd' , '#b2acd9', '#aa9fd4','#8c87c2'];
      this.setLanguage();
      this.getMarathanList();
      }
      getMarathanList(){
        let event={
          pageSize:10,
          pageIndex:0
        }
        this.marathanService.getMarathanList(event).subscribe(response => {
          let data = response.json();
            console.log('getMarathanList',data)
        })
      }
      ionViewWillEnter() {
        this.menuCtrl.enable(true);
      }
      ionViewDidLoad() {
        // this.marathanService.getUserMarathonDetail(this.user_marathan_id).subscribe(response => {

        // })
        this.isGreatExtension = localStorage.getItem('isGreatExtension')
        this.isCourse = localStorage.getItem('isCourse')
        this.marathonId = localStorage.getItem("marathonid")
        this.isContest = localStorage.getItem('isContest')
    
        var languageCode = localStorage.getItem("languageType");
        this.languageType = this.languageService.languageCode(languageCode);
        this.extension = localStorage.getItem('extension');
        this.show = true;
        this.user_marathan_id = this.navParams.get('user_marathan_id');
        console.log("this.data ", this.user_marathan_id)
        this.marathanService.getUserMarathonDetail(this.user_marathan_id).subscribe(response => {
          this.iserror = false;
          this.show = true;
          this.showOrderDetailMessage = false;
          this.data = response.json();
          localStorage.setItem('product', this.data['productType']);
          if (this.data.title.length >= 25) {
            this.titleLength = true;
          } else {
            this.titleLength = false;
          }
          console.log('Data tstf', this.data);
          if (this.data.leftDays <= 3 && this.data.productType == 'GreatExtension') {
            if (this.data.day != 27) {
              this.data.greatExtensionDays.push({ leftDays: 2, isEmpty: true })
            }
    
            // this.day_exercise_Data.greatExtensionDays.push({ leftDays: 2, isEmpty: true })
          }
    
          // var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
          // this.startdate = this.data.startDate.toLocaleDateString('ar-EG', options)
          localStorage.setItem('isContest', this.data['contest'])
          localStorage.setItem('isComment', this.data['comment'])
          // scroll-content
          // header
    
    
          localStorage.setItem('extension', this.data['isExtension'])
          this.extension = localStorage.getItem('extension');
          if (!this.data.isMarathonStart) {
            this.closetime = false
          }
    
    
          if (this.data.welcomeMessage != null) {
            let showwelcomeMessage = this.data.welcomeMessage.welcomeMessage.replace(new RegExp("Froala Editor", "g"), "");
            let showwelcomeMessage1 = showwelcomeMessage.replace(new RegExp("Powered by", "g"), "");
            this.welcomeMessage = showwelcomeMessage1.replace(new RegExp("{{order_number}}", "g"), localStorage.getItem("order_number"));
          }
    
          if (this.data.rule != null) {
            let Rules = this.data.rule.rule.replace(new RegExp("Froala Editor", "g"), "");
            this.Rules = Rules.replace(new RegExp("Powered by", "g"), "");
          }
    
          // || this.data.greatExtensionDays != null && this.data.greatExtensionDays.length == 0
          if (this.data.marathonFinalMessages != null) {
            for (let index = 0; index < this.data.marathonDays.length; index++) {
              var a = this.data.marathonDays.length - 1;
              if (this.data.marathonDays[a].isActive == true) {
    
                this.showtime = false;
                this.showfinalmessgas = false
                break
              } else {
                this.showfinalmessgas = false;
    
                this.showtime = true;
                if (a == index) {
    
                  if (this.data.marathonDays[a].isActive == false) {
    
                    // 
                    var b = this.data.marathonFinalMessages.length - 1
                    // finamessageid
                    if (b == 0) {
                      this.showfinalmessgas = true
                      // document.getElementById("finamessageid2").click()
                      setTimeout(() => {
                        if (this.data.greatExtensionDays == null) {
                          document.getElementById("finamessageid2").click()
                        }
                      }, 100);
    
                    } else if (b == 1) {
                      this.showfinalmessgas = true
                      // document.getElementById("finamessageid1").click()
                      setTimeout(() => {
                        if (this.data.greatExtensionDays == null) {
                          document.getElementById("finamessageid1").click()
                        }
                      }, 100);
                    } else if (b == 2) {
                      this.showfinalmessgas = true
                      setTimeout(() => {
                        if (this.data.greatExtensionDays == null) {
                          document.getElementById("finamessageid0").click()
                        }
                      }, 100);
    
                    }
    
                  }
                }
    
              }
    
            }
          }
    
          if (this.data['productType'] == "Marathon" && this.data['marathonFinalMessages'] != null) {
            this.finalMessageData = [];
            for (let i = 0; i < this.data['marathonFinalMessages'].length; i++) {
              if (this.data.leftDays != null && this.data.leftDays <= 3) {
                if (i != 1) {
                  this.finalMessageData.push(this.data['marathonFinalMessages'][i])
                }
              } else {
                this.finalMessageData.push(this.data['marathonFinalMessages'][i])
              }
            }
          }
    
          if (this.data['productType'] == "GreatExtension" && this.data['marathonFinalMessages'] != null) {
            this.finalMessageData = [];
            for (let i = 0; i < this.data['marathonFinalMessages'].length; i++) {
              if (this.data.leftDays != null) {
                if (i != 1) {
                  this.finalMessageData.push(this.data['marathonFinalMessages'][i])
                }
              } else {
                this.finalMessageData.push(this.data['marathonFinalMessages'][i])
              }
            }
          }
          if (this.data['productType'] == "Extension" && this.data['marathonFinalMessages'] != null) {
    
            this.finalMessageData = [];
            for (let i = 0; i < this.data['marathonFinalMessages'].length; i++) {
              if (this.data.leftDays != null) {
                if (i != 1) {
                  this.finalMessageData.push(this.data['marathonFinalMessages'][i])
                }
              } else {
                this.finalMessageData.push(this.data['marathonFinalMessages'][i])
              }
            }
          }
    
    
    
          localStorage.setItem("startDate", this.data.startDate);
    
          if (this.data.isMarathonStart) {
            if (this.data.welcomeMessage.contentType == 'video') {
              if (this.data.welcomeMessage.videoUrl.split("/")[2] == "youtu.be") {
                this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.data.welcomeMessage.videoUrl.replace("youtu.be", "www.youtube.com/embed/"))
              } else {
                this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.data.welcomeMessage.videoUrl.replace("watch?v=", "embed/"))
              }
            }
          }
    
          if (!this.data.isMarathonStart) {
            setInterval(() => {
              this.timer();
            }, 1000);
          } else {
            this.showtime = true;
          }
          this.sampledata = this.data.marathonDays;
          this.sampledataGreatExtension = this.data.greatExtensionDays;
          // this.sampledataGreatExtension = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
          if (this.data.marathonDays != null || this.data.greatExtensionDays != null) {
    
    
            let daysCount = this.data.marathonDays.length;
            let startColCount = 2;
            let totalColCount = daysCount + startColCount;
            let colPerRow = 3;
            let totalRow = Math.ceil((totalColCount / colPerRow));
            let emptyCol = (totalRow * colPerRow) - totalColCount;
            localStorage.setItem("marathon_name", this.data.title);
            this.firstDayId = this.sampledata[this.sampledata.length - 1].id
    
            for (let i = 0; i < this.sampledata.length; i++) {
              this.daysOfWeek.push(this.sampledata[i]);
              if ((i + 1) % 7 === 0) {
                this.day.push(this.daysOfWeek);
                this.daysOfWeek = [];
              }
            }
    
            if (this.daysOfWeek.length) {
              this.day.push(this.daysOfWeek);
            }
            this.day.reverse()
            for (let j = 0; j < this.day.length; j++) {
              this.day[j].reverse()
            }
            let lastWeekdaysCount = this.day[0].length;
            let lastWeektotalColCount = lastWeekdaysCount + startColCount;
            let lastWeektotalRow = Math.ceil((lastWeektotalColCount / colPerRow));
            let lastWeekemptyCol = (lastWeektotalRow * colPerRow) - lastWeektotalColCount;
            for (let k = 0; k < lastWeekemptyCol; k++) {
    
              if (this.data.leftDays > 3 && this.data.productType == 'Marathon') {
                this.day[0].unshift({ isEmpty: true })
              }
            }
            this.split = this.data.startDate
            let fcolPerRow = 3;
            let fstartColCount = 3;
    
            if (this.finalMessageData != null) {
    
              let flastWeekdaysCount = this.finalMessageData.length
              let flastWeektotalColCount = flastWeekdaysCount + fstartColCount;
              let flastWeektotalRow = Math.ceil((flastWeektotalColCount / fcolPerRow));
              let flastWeekemptyCol = (flastWeektotalRow * fcolPerRow) - flastWeektotalColCount;
              for (let k = 0; k < flastWeekemptyCol; k++) {
                if (this.data.leftDays > 3) {
    
                  if (this.data.leftDays > 3 && this.data.productType == 'Marathon') {
                    this.finalMessageData.unshift({ isEmpty: true })
                  }
                }
              }
            }
    
            if (this.finalMessageData != null) {
              this.finalMessageData.sort((a, b) => {
                return <any>(b.order) - <any>(a.order);
              });
            }
            if (this.data.greatExtensionDays != null) {
    
              let daysCountGreatExtension = this.data.greatExtensionDays.length;
    
              let startColCountGreatExtension = 2;
              let totalColCountGreatExtension = daysCountGreatExtension + startColCountGreatExtension;
              let colPerRowGreatExtension = 3;
              let totalRowGreatExtension = Math.ceil((totalColCountGreatExtension / colPerRowGreatExtension));
              let emptyColGreatExtension = (totalRowGreatExtension * colPerRowGreatExtension) - totalColCountGreatExtension;
              localStorage.setItem("marathon_name", this.data.title);
              this.firstDayIdGreatExtension = this.sampledataGreatExtension[this.sampledataGreatExtension.length - 1].id;
    
              for (let i = 0; i < this.sampledataGreatExtension.length; i++) {
                this.daysOfWeekGreatExtension.push(this.sampledataGreatExtension[i]);
                if ((i + 1) % 7 === 0) {
                  // if (this.dayGreatExtension.length <= 4) {
                  this.dayGreatExtension.push(this.daysOfWeekGreatExtension);
                  this.daysOfWeekGreatExtension = [];
                  // }
    
                }
              }
              //  && this.dayGreatExtension.length <= 4
              if (this.daysOfWeekGreatExtension.length) {
                this.dayGreatExtension.push(this.daysOfWeekGreatExtension);
              }
    
              this.dayGreatExtension.reverse()
              for (let j = 0; j < this.dayGreatExtension.length; j++) {
                this.dayGreatExtension[j].reverse()
              }
              let lastWeekdaysCountGreatExtension = this.dayGreatExtension[0].length;
              let lastWeektotalColCountGreatExtension = lastWeekdaysCountGreatExtension + startColCountGreatExtension;
              let lastWeektotalRowGreatExtension = Math.ceil((lastWeektotalColCountGreatExtension / colPerRowGreatExtension));
              let lastWeekemptyColGreatExtension = (lastWeektotalRowGreatExtension * colPerRowGreatExtension) - lastWeektotalColCountGreatExtension;
              if (this.data.leftDays <= 3 && lastWeekemptyColGreatExtension == 1) {
                lastWeekemptyColGreatExtension = 0
              } else if (this.data.leftDays <= 3 && lastWeekemptyColGreatExtension == 0) {
                lastWeekemptyColGreatExtension = 2
              } else if (this.data.leftDays <= 3 && lastWeekemptyColGreatExtension == 2) {
                lastWeekemptyColGreatExtension = 1
              }
    
    
              // if (this.data.leftDays <= 3) {
              //   this.dayGreatExtension[0].splice(0, 1)
    
              // }
    
              for (let k = 0; k < lastWeekemptyColGreatExtension; k++) {
                this.dayGreatExtension[0].unshift({ isEmpty: true })
              }
              this.split = this.data.startDate;
    
              if (this.data.leftDays <= 3) {
                this.dayGreatExtension[0].splice(0, 1)
    
              }
              this.extensiondetail = this.extensiondetail.replace(new RegExp("Froala Editor", "g"), "")
                .replace(new RegExp("Powered by", "g"), "").replace(new RegExp('"', "g"), "").replace(new RegExp("@simpleExtensionButton", "g"), "")
    
              // this.split = this.data.startDate
              // let fcolPerRow = 3;
              // let fstartColCount = 3;
            }
          }
        }, error => {
          this.iserror = true;
          this.show = false;
          this.exerciseError = error.text();
          console.log('maraErr', this.exerciseError);
          if (this.exerciseError == "Marathon is over") {
            this.alrtCtrl.create({
              message: 'Марафон окончен',
              buttons: [{
                text: 'OK',
                handler: () => {
                  
                  this.router.navigate([OrderlistPage]);
                }
              }]
            })
          }
        });
      }
      //>......................
      onGoToMarathanDayExercise(id, day) {
        localStorage.setItem("marathonId", this.data.marathonId);
        localStorage.setItem("saveDayID", id);
        this.router.navigate(['usermarathon', id]);
      }
      setLanguage(){
        var  languageCode =localStorage.getItem("languageType");
        this.languageType =  this.languageService.languageCode(languageCode);
        console.log(this.languageType,'test');
      }
      english() {
        localStorage.setItem("languageType", "Eng")
        this.langCode = "Eng"
        this.languageType = this.languageService.languageCode("Eng");
      }
    
      russian() {
        localStorage.setItem("languageType", "Rus")
        this.langCode = "Rus"
        this.languageType = this.languageService.languageCode("Rus");
      }
      italian() {
        localStorage.setItem("languageType", "Ita")
        this.langCode = "Ita"
        this.languageType = this.languageService.languageCode("Ita");
      }
      goToLandingPage() {
        this.checkfinalmessages = false;
        this.pagehead = false;
        for (let i = 0; i < this.newFinalMessageArray.length; i++) {
          this.newFinalMessageArray[i]['IsActive'] = false;
        }
      }
      
     menu(){
      this.router.navigate(['menu']);
     }
     userpage(){
      this.router.navigate(['usermarathon']);
    }
   moveUp = function () {
      var dimensions = this.container.getContentDimensions();
      this.container.scrollTo(0, -dimensions.contentHeight, 0);
  };
  russianMonths(month) {
    // console.log("month" , month);
    if (month != undefined) {
      if (month.indexOf('-') == -1) {
        if (this.languageType.language.language == "Eng") {
          var monthArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        } else if (this.languageType.language.language == "Ita") {
          var monthArray = ['gennaio', 'febbraio', 'marcia', 'aprile', 'maggio', 'giugno', 'luglio', 'agosto', 'settembre', 'ottobre', 'novembre', 'dicembre'];
        }
        else {
          var monthArray = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
        }
        let monthOrder = month.split('/')[0];
        return monthArray[monthOrder - 1]
      } else {
        if (this.languageType.language.language == "Eng") {
          var monthArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        }
        else if (this.languageType.language.language == "Ita") {
          var monthArray = ['gennaio', 'febbraio', 'marcia', 'aprile', 'maggio', 'giugno', 'luglio', 'agosto', 'settembre', 'ottobre', 'novembre', 'dicembre'];
        }
        else {
          var monthArray = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
        }
        let monthOrder = month.split('-')[1];
        return monthArray[monthOrder - 1]
      }
    }


  }
  timer() {

    // Update the count down every 1 second
    this.splitString = this.data.startDate
    this.countDownDate = new Date(this.splitString).getTime();
    this.MarathonName = this.data.title

    // Get todays date and time
    this.now = new Date().getTime();

    // Find the distance between now and the count down date
    this.distance = this.countDownDate - this.now;

    // Time calculations for days, hours, minutes and seconds
    this.days = Math.floor(this.distance / (1000 * 60 * 60 * 24));
    this.hours = Math.floor((this.distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    this.minutes = Math.floor((this.distance % (1000 * 60 * 60)) / (1000 * 60));
    this.seconds = Math.floor((this.distance % (1000 * 60)) / 1000);
    this.countDown = true

  }
  
    //<.....static data........
      getdays(){
        this.daydata =[
        
          {
            "details" :`<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Привет, мои дорогие!😍</span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Как вы там у меня?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Каков ваш настрой? Боевой?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Или всё, решили расслабиться? 😄</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Наш марафон подошёл к концу и теперь вам предстоит самостоятельная работа.&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я очень надеюсь, что за эти три недели вы узнали для себя что-то новое и приобрели знания, которые пойдут вам на пользу.🙏🏼😇</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы мне написали так много тёплых слов!!!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мои дорогие, я тоже хочу сказать вам всем от себя</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">БОЛЬШОЕ СПАСИБО ❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше доверие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше терпение, за вашу силу воли, за ваше стремление к лучшему!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше трудолюбие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я уже говорила вам, вы моя гордость, моя мотивация!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы вносите большой вклад в становление Системы Мияби.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы многому меня учите. Именно благодаря вам &nbsp;Мияби совершенствуется.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо вам за это!🙏🏻❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мне с вами было очень приятно работать! 😇 Не скажу, что легко, но очень интересно!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Очень хочется надеяться, что после окончания марафона наше с вами общение будет продолжаться. 🙏🏼😇😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">На своей основной страничке @vita_bodylove сегодня я опубликую пост &nbsp;&quot;Отзывы&quot; с обращением к моим подписчикам и вам, участникам последнего марафона.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Дорогие мои, я буду вам очень благодарна, если вы найдёте минутку, и оставите в комментариях к публикации свой отзыв о марафоне, напишите о своих впечатлениях, или о своих изменениях. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Ваши отзывы очень важны не только мне, но всем, кто только встал на путь омоложения.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">А я в свою очередь, в благодарность за оставленные отзывы, между всеми, кто меня поддержит, разыграю 10 призовых билетов в любые из марафонов Мияби. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">И пишите мне почаще в основной аккаунт! Делитесь своими результатами! Я буду ждать!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Ваш Тренер.&nbsp;<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/2764.svg);">&nbsp;</span> </span></p><p data-f-id="pbf" style="text-align: center; font-size: 0px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Edi</a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a></p>`,
            "day": `01`,
            "week": `Monday`
          },
          {
            "details" :`<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Привет, мои дорогие!😍</span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Как вы там у меня?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Каков ваш настрой? Боевой?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Или всё, решили расслабиться? 😄</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Наш марафон подошёл к концу и теперь вам предстоит самостоятельная работа.&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я очень надеюсь, что за эти три недели вы узнали для себя что-то новое и приобрели знания, которые пойдут вам на пользу.🙏🏼😇</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы мне написали так много тёплых слов!!!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мои дорогие, я тоже хочу сказать вам всем от себя</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">БОЛЬШОЕ СПАСИБО ❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше доверие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше терпение, за вашу силу воли, за ваше стремление к лучшему!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше трудолюбие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я уже говорила вам, вы моя гордость, моя мотивация!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы вносите большой вклад в становление Системы Мияби.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы многому меня учите. Именно благодаря вам &nbsp;Мияби совершенствуется.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо вам за это!🙏🏻❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мне с вами было очень приятно работать! 😇 Не скажу, что легко, но очень интересно!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Очень хочется надеяться, что после окончания марафона наше с вами общение будет продолжаться. 🙏🏼😇😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">На своей основной страничке @vita_bodylove сегодня я опубликую пост &nbsp;&quot;Отзывы&quot; с обращением к моим подписчикам и вам, участникам последнего марафона.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Дорогие мои, я буду вам очень благодарна, если вы найдёте минутку, и оставите в комментариях к публикации свой отзыв о марафоне, напишите о своих впечатлениях, или о своих изменениях. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Ваши отзывы очень важны не только мне, но всем, кто только встал на путь омоложения.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">А я в свою очередь, в благодарность за оставленные отзывы, между всеми, кто меня поддержит, разыграю 10 призовых билетов в любые из марафонов Мияби. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">И пишите мне почаще в основной аккаунт! Делитесь своими результатами! Я буду ждать!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Ваш Тренер.&nbsp;<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/2764.svg);">&nbsp;</span> </span></p><p data-f-id="pbf" style="text-align: center; font-size: 0px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Edi</a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a></p>`,
            "day": `02`,
            "week": `Tuesday`
          },
          {
            "details" :`<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Привет, мои дорогие!😍</span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Как вы там у меня?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Каков ваш настрой? Боевой?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Или всё, решили расслабиться? 😄</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Наш марафон подошёл к концу и теперь вам предстоит самостоятельная работа.&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я очень надеюсь, что за эти три недели вы узнали для себя что-то новое и приобрели знания, которые пойдут вам на пользу.🙏🏼😇</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы мне написали так много тёплых слов!!!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мои дорогие, я тоже хочу сказать вам всем от себя</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">БОЛЬШОЕ СПАСИБО ❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше доверие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше терпение, за вашу силу воли, за ваше стремление к лучшему!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше трудолюбие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я уже говорила вам, вы моя гордость, моя мотивация!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы вносите большой вклад в становление Системы Мияби.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы многому меня учите. Именно благодаря вам &nbsp;Мияби совершенствуется.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо вам за это!🙏🏻❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мне с вами было очень приятно работать! 😇 Не скажу, что легко, но очень интересно!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Очень хочется надеяться, что после окончания марафона наше с вами общение будет продолжаться. 🙏🏼😇😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">На своей основной страничке @vita_bodylove сегодня я опубликую пост &nbsp;&quot;Отзывы&quot; с обращением к моим подписчикам и вам, участникам последнего марафона.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Дорогие мои, я буду вам очень благодарна, если вы найдёте минутку, и оставите в комментариях к публикации свой отзыв о марафоне, напишите о своих впечатлениях, или о своих изменениях. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Ваши отзывы очень важны не только мне, но всем, кто только встал на путь омоложения.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">А я в свою очередь, в благодарность за оставленные отзывы, между всеми, кто меня поддержит, разыграю 10 призовых билетов в любые из марафонов Мияби. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">И пишите мне почаще в основной аккаунт! Делитесь своими результатами! Я буду ждать!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Ваш Тренер.&nbsp;<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/2764.svg);">&nbsp;</span> </span></p><p data-f-id="pbf" style="text-align: center; font-size: 0px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Edi</a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a></p>`,
            "day": `03`,
            "week": `Wensday`
          },
          {
            "details" :`<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Привет, мои дорогие!😍</span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Как вы там у меня?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Каков ваш настрой? Боевой?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Или всё, решили расслабиться? 😄</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Наш марафон подошёл к концу и теперь вам предстоит самостоятельная работа.&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я очень надеюсь, что за эти три недели вы узнали для себя что-то новое и приобрели знания, которые пойдут вам на пользу.🙏🏼😇</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы мне написали так много тёплых слов!!!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мои дорогие, я тоже хочу сказать вам всем от себя</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">БОЛЬШОЕ СПАСИБО ❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше доверие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше терпение, за вашу силу воли, за ваше стремление к лучшему!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше трудолюбие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я уже говорила вам, вы моя гордость, моя мотивация!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы вносите большой вклад в становление Системы Мияби.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы многому меня учите. Именно благодаря вам &nbsp;Мияби совершенствуется.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо вам за это!🙏🏻❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мне с вами было очень приятно работать! 😇 Не скажу, что легко, но очень интересно!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Очень хочется надеяться, что после окончания марафона наше с вами общение будет продолжаться. 🙏🏼😇😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">На своей основной страничке @vita_bodylove сегодня я опубликую пост &nbsp;&quot;Отзывы&quot; с обращением к моим подписчикам и вам, участникам последнего марафона.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Дорогие мои, я буду вам очень благодарна, если вы найдёте минутку, и оставите в комментариях к публикации свой отзыв о марафоне, напишите о своих впечатлениях, или о своих изменениях. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Ваши отзывы очень важны не только мне, но всем, кто только встал на путь омоложения.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">А я в свою очередь, в благодарность за оставленные отзывы, между всеми, кто меня поддержит, разыграю 10 призовых билетов в любые из марафонов Мияби. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">И пишите мне почаще в основной аккаунт! Делитесь своими результатами! Я буду ждать!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Ваш Тренер.&nbsp;<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/2764.svg);">&nbsp;</span> </span></p><p data-f-id="pbf" style="text-align: center; font-size: 0px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Edi</a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a></p>`,
            "day": `04`,
            "week": `Thrusday`
          },
          {
            "details" :`<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Привет, мои дорогие!😍</span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Как вы там у меня?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Каков ваш настрой? Боевой?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Или всё, решили расслабиться? 😄</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Наш марафон подошёл к концу и теперь вам предстоит самостоятельная работа.&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я очень надеюсь, что за эти три недели вы узнали для себя что-то новое и приобрели знания, которые пойдут вам на пользу.🙏🏼😇</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы мне написали так много тёплых слов!!!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мои дорогие, я тоже хочу сказать вам всем от себя</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">БОЛЬШОЕ СПАСИБО ❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше доверие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше терпение, за вашу силу воли, за ваше стремление к лучшему!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше трудолюбие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я уже говорила вам, вы моя гордость, моя мотивация!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы вносите большой вклад в становление Системы Мияби.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы многому меня учите. Именно благодаря вам &nbsp;Мияби совершенствуется.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо вам за это!🙏🏻❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мне с вами было очень приятно работать! 😇 Не скажу, что легко, но очень интересно!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Очень хочется надеяться, что после окончания марафона наше с вами общение будет продолжаться. 🙏🏼😇😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">На своей основной страничке @vita_bodylove сегодня я опубликую пост &nbsp;&quot;Отзывы&quot; с обращением к моим подписчикам и вам, участникам последнего марафона.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Дорогие мои, я буду вам очень благодарна, если вы найдёте минутку, и оставите в комментариях к публикации свой отзыв о марафоне, напишите о своих впечатлениях, или о своих изменениях. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Ваши отзывы очень важны не только мне, но всем, кто только встал на путь омоложения.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">А я в свою очередь, в благодарность за оставленные отзывы, между всеми, кто меня поддержит, разыграю 10 призовых билетов в любые из марафонов Мияби. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">И пишите мне почаще в основной аккаунт! Делитесь своими результатами! Я буду ждать!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Ваш Тренер.&nbsp;<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/2764.svg);">&nbsp;</span> </span></p><p data-f-id="pbf" style="text-align: center; font-size: 0px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Edi</a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a></p>`,
            "day": `05`,
            "week": `Friday`
          },
          {
            "details" :`<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Привет, мои дорогие!😍</span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Как вы там у меня?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Каков ваш настрой? Боевой?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Или всё, решили расслабиться? 😄</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Наш марафон подошёл к концу и теперь вам предстоит самостоятельная работа.&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я очень надеюсь, что за эти три недели вы узнали для себя что-то новое и приобрели знания, которые пойдут вам на пользу.🙏🏼😇</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы мне написали так много тёплых слов!!!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мои дорогие, я тоже хочу сказать вам всем от себя</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">БОЛЬШОЕ СПАСИБО ❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше доверие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше терпение, за вашу силу воли, за ваше стремление к лучшему!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше трудолюбие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я уже говорила вам, вы моя гордость, моя мотивация!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы вносите большой вклад в становление Системы Мияби.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы многому меня учите. Именно благодаря вам &nbsp;Мияби совершенствуется.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо вам за это!🙏🏻❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мне с вами было очень приятно работать! 😇 Не скажу, что легко, но очень интересно!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Очень хочется надеяться, что после окончания марафона наше с вами общение будет продолжаться. 🙏🏼😇😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">На своей основной страничке @vita_bodylove сегодня я опубликую пост &nbsp;&quot;Отзывы&quot; с обращением к моим подписчикам и вам, участникам последнего марафона.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Дорогие мои, я буду вам очень благодарна, если вы найдёте минутку, и оставите в комментариях к публикации свой отзыв о марафоне, напишите о своих впечатлениях, или о своих изменениях. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Ваши отзывы очень важны не только мне, но всем, кто только встал на путь омоложения.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">А я в свою очередь, в благодарность за оставленные отзывы, между всеми, кто меня поддержит, разыграю 10 призовых билетов в любые из марафонов Мияби. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">И пишите мне почаще в основной аккаунт! Делитесь своими результатами! Я буду ждать!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Ваш Тренер.&nbsp;<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/2764.svg);">&nbsp;</span> </span></p><p data-f-id="pbf" style="text-align: center; font-size: 0px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Edi</a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a></p>`,
            "day": `06`,
            "week": `Saturday`
          },
          {
            "details" :`<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Привет, мои дорогие!😍</span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Как вы там у меня?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Каков ваш настрой? Боевой?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Или всё, решили расслабиться? 😄</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Наш марафон подошёл к концу и теперь вам предстоит самостоятельная работа.&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я очень надеюсь, что за эти три недели вы узнали для себя что-то новое и приобрели знания, которые пойдут вам на пользу.🙏🏼😇</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы мне написали так много тёплых слов!!!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мои дорогие, я тоже хочу сказать вам всем от себя</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">БОЛЬШОЕ СПАСИБО ❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше доверие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше терпение, за вашу силу воли, за ваше стремление к лучшему!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше трудолюбие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я уже говорила вам, вы моя гордость, моя мотивация!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы вносите большой вклад в становление Системы Мияби.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы многому меня учите. Именно благодаря вам &nbsp;Мияби совершенствуется.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо вам за это!🙏🏻❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мне с вами было очень приятно работать! 😇 Не скажу, что легко, но очень интересно!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Очень хочется надеяться, что после окончания марафона наше с вами общение будет продолжаться. 🙏🏼😇😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">На своей основной страничке @vita_bodylove сегодня я опубликую пост &nbsp;&quot;Отзывы&quot; с обращением к моим подписчикам и вам, участникам последнего марафона.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Дорогие мои, я буду вам очень благодарна, если вы найдёте минутку, и оставите в комментариях к публикации свой отзыв о марафоне, напишите о своих впечатлениях, или о своих изменениях. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Ваши отзывы очень важны не только мне, но всем, кто только встал на путь омоложения.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">А я в свою очередь, в благодарность за оставленные отзывы, между всеми, кто меня поддержит, разыграю 10 призовых билетов в любые из марафонов Мияби. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">И пишите мне почаще в основной аккаунт! Делитесь своими результатами! Я буду ждать!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Ваш Тренер.&nbsp;<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/2764.svg);">&nbsp;</span> </span></p><p data-f-id="pbf" style="text-align: center; font-size: 0px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Edi</a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a></p>`,
            "day": `07`,
            "week": `Sunday`
          },
          {
            "details" :`<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Привет, мои дорогие!😍</span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Как вы там у меня?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Каков ваш настрой? Боевой?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Или всё, решили расслабиться? 😄</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Наш марафон подошёл к концу и теперь вам предстоит самостоятельная работа.&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я очень надеюсь, что за эти три недели вы узнали для себя что-то новое и приобрели знания, которые пойдут вам на пользу.🙏🏼😇</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы мне написали так много тёплых слов!!!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мои дорогие, я тоже хочу сказать вам всем от себя</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">БОЛЬШОЕ СПАСИБО ❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше доверие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше терпение, за вашу силу воли, за ваше стремление к лучшему!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше трудолюбие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я уже говорила вам, вы моя гордость, моя мотивация!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы вносите большой вклад в становление Системы Мияби.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы многому меня учите. Именно благодаря вам &nbsp;Мияби совершенствуется.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо вам за это!🙏🏻❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мне с вами было очень приятно работать! 😇 Не скажу, что легко, но очень интересно!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Очень хочется надеяться, что после окончания марафона наше с вами общение будет продолжаться. 🙏🏼😇😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">На своей основной страничке @vita_bodylove сегодня я опубликую пост &nbsp;&quot;Отзывы&quot; с обращением к моим подписчикам и вам, участникам последнего марафона.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Дорогие мои, я буду вам очень благодарна, если вы найдёте минутку, и оставите в комментариях к публикации свой отзыв о марафоне, напишите о своих впечатлениях, или о своих изменениях. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Ваши отзывы очень важны не только мне, но всем, кто только встал на путь омоложения.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">А я в свою очередь, в благодарность за оставленные отзывы, между всеми, кто меня поддержит, разыграю 10 призовых билетов в любые из марафонов Мияби. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">И пишите мне почаще в основной аккаунт! Делитесь своими результатами! Я буду ждать!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Ваш Тренер.&nbsp;<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/2764.svg);">&nbsp;</span> </span></p><p data-f-id="pbf" style="text-align: center; font-size: 0px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Edi</a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a></p>`,
            "day": `08`,
            "week": `Monday`
          },
          {
            "details" :`<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Привет, мои дорогие!😍</span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Как вы там у меня?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Каков ваш настрой? Боевой?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Или всё, решили расслабиться? 😄</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Наш марафон подошёл к концу и теперь вам предстоит самостоятельная работа.&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я очень надеюсь, что за эти три недели вы узнали для себя что-то новое и приобрели знания, которые пойдут вам на пользу.🙏🏼😇</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы мне написали так много тёплых слов!!!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мои дорогие, я тоже хочу сказать вам всем от себя</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">БОЛЬШОЕ СПАСИБО ❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше доверие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше терпение, за вашу силу воли, за ваше стремление к лучшему!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше трудолюбие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я уже говорила вам, вы моя гордость, моя мотивация!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы вносите большой вклад в становление Системы Мияби.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы многому меня учите. Именно благодаря вам &nbsp;Мияби совершенствуется.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо вам за это!🙏🏻❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мне с вами было очень приятно работать! 😇 Не скажу, что легко, но очень интересно!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Очень хочется надеяться, что после окончания марафона наше с вами общение будет продолжаться. 🙏🏼😇😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">На своей основной страничке @vita_bodylove сегодня я опубликую пост &nbsp;&quot;Отзывы&quot; с обращением к моим подписчикам и вам, участникам последнего марафона.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Дорогие мои, я буду вам очень благодарна, если вы найдёте минутку, и оставите в комментариях к публикации свой отзыв о марафоне, напишите о своих впечатлениях, или о своих изменениях. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Ваши отзывы очень важны не только мне, но всем, кто только встал на путь омоложения.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">А я в свою очередь, в благодарность за оставленные отзывы, между всеми, кто меня поддержит, разыграю 10 призовых билетов в любые из марафонов Мияби. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">И пишите мне почаще в основной аккаунт! Делитесь своими результатами! Я буду ждать!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Ваш Тренер.&nbsp;<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/2764.svg);">&nbsp;</span> </span></p><p data-f-id="pbf" style="text-align: center; font-size: 0px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Edi</a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a></p>`,
            "day": `09`,
            "week": `Tuesday`
          },
          {
            "details" :`<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Привет, мои дорогие!😍</span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Как вы там у меня?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Каков ваш настрой? Боевой?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Или всё, решили расслабиться? 😄</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Наш марафон подошёл к концу и теперь вам предстоит самостоятельная работа.&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я очень надеюсь, что за эти три недели вы узнали для себя что-то новое и приобрели знания, которые пойдут вам на пользу.🙏🏼😇</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы мне написали так много тёплых слов!!!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мои дорогие, я тоже хочу сказать вам всем от себя</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">БОЛЬШОЕ СПАСИБО ❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше доверие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше терпение, за вашу силу воли, за ваше стремление к лучшему!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше трудолюбие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я уже говорила вам, вы моя гордость, моя мотивация!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы вносите большой вклад в становление Системы Мияби.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы многому меня учите. Именно благодаря вам &nbsp;Мияби совершенствуется.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо вам за это!🙏🏻❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мне с вами было очень приятно работать! 😇 Не скажу, что легко, но очень интересно!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Очень хочется надеяться, что после окончания марафона наше с вами общение будет продолжаться. 🙏🏼😇😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">На своей основной страничке @vita_bodylove сегодня я опубликую пост &nbsp;&quot;Отзывы&quot; с обращением к моим подписчикам и вам, участникам последнего марафона.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Дорогие мои, я буду вам очень благодарна, если вы найдёте минутку, и оставите в комментариях к публикации свой отзыв о марафоне, напишите о своих впечатлениях, или о своих изменениях. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Ваши отзывы очень важны не только мне, но всем, кто только встал на путь омоложения.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">А я в свою очередь, в благодарность за оставленные отзывы, между всеми, кто меня поддержит, разыграю 10 призовых билетов в любые из марафонов Мияби. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">И пишите мне почаще в основной аккаунт! Делитесь своими результатами! Я буду ждать!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Ваш Тренер.&nbsp;<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/2764.svg);">&nbsp;</span> </span></p><p data-f-id="pbf" style="text-align: center; font-size: 0px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Edi</a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a></p>`,
            "day": `10`,
            "week": `Wensday`
          },
          {
            "details" :`<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Привет, мои дорогие!😍</span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Как вы там у меня?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Каков ваш настрой? Боевой?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Или всё, решили расслабиться? 😄</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Наш марафон подошёл к концу и теперь вам предстоит самостоятельная работа.&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я очень надеюсь, что за эти три недели вы узнали для себя что-то новое и приобрели знания, которые пойдут вам на пользу.🙏🏼😇</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы мне написали так много тёплых слов!!!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мои дорогие, я тоже хочу сказать вам всем от себя</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">БОЛЬШОЕ СПАСИБО ❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше доверие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше терпение, за вашу силу воли, за ваше стремление к лучшему!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше трудолюбие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я уже говорила вам, вы моя гордость, моя мотивация!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы вносите большой вклад в становление Системы Мияби.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы многому меня учите. Именно благодаря вам &nbsp;Мияби совершенствуется.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо вам за это!🙏🏻❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мне с вами было очень приятно работать! 😇 Не скажу, что легко, но очень интересно!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Очень хочется надеяться, что после окончания марафона наше с вами общение будет продолжаться. 🙏🏼😇😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">На своей основной страничке @vita_bodylove сегодня я опубликую пост &nbsp;&quot;Отзывы&quot; с обращением к моим подписчикам и вам, участникам последнего марафона.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Дорогие мои, я буду вам очень благодарна, если вы найдёте минутку, и оставите в комментариях к публикации свой отзыв о марафоне, напишите о своих впечатлениях, или о своих изменениях. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Ваши отзывы очень важны не только мне, но всем, кто только встал на путь омоложения.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">А я в свою очередь, в благодарность за оставленные отзывы, между всеми, кто меня поддержит, разыграю 10 призовых билетов в любые из марафонов Мияби. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">И пишите мне почаще в основной аккаунт! Делитесь своими результатами! Я буду ждать!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Ваш Тренер.&nbsp;<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/2764.svg);">&nbsp;</span> </span></p><p data-f-id="pbf" style="text-align: center; font-size: 0px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Edi</a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a></p>`,
            "day": `11`,
            "week": `Thrusday`
          },
          {
            "details" :`<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Привет, мои дорогие!😍</span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Как вы там у меня?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Каков ваш настрой? Боевой?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Или всё, решили расслабиться? 😄</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Наш марафон подошёл к концу и теперь вам предстоит самостоятельная работа.&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я очень надеюсь, что за эти три недели вы узнали для себя что-то новое и приобрели знания, которые пойдут вам на пользу.🙏🏼😇</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы мне написали так много тёплых слов!!!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мои дорогие, я тоже хочу сказать вам всем от себя</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">БОЛЬШОЕ СПАСИБО ❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше доверие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше терпение, за вашу силу воли, за ваше стремление к лучшему!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше трудолюбие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я уже говорила вам, вы моя гордость, моя мотивация!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы вносите большой вклад в становление Системы Мияби.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы многому меня учите. Именно благодаря вам &nbsp;Мияби совершенствуется.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо вам за это!🙏🏻❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мне с вами было очень приятно работать! 😇 Не скажу, что легко, но очень интересно!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Очень хочется надеяться, что после окончания марафона наше с вами общение будет продолжаться. 🙏🏼😇😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">На своей основной страничке @vita_bodylove сегодня я опубликую пост &nbsp;&quot;Отзывы&quot; с обращением к моим подписчикам и вам, участникам последнего марафона.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Дорогие мои, я буду вам очень благодарна, если вы найдёте минутку, и оставите в комментариях к публикации свой отзыв о марафоне, напишите о своих впечатлениях, или о своих изменениях. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Ваши отзывы очень важны не только мне, но всем, кто только встал на путь омоложения.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">А я в свою очередь, в благодарность за оставленные отзывы, между всеми, кто меня поддержит, разыграю 10 призовых билетов в любые из марафонов Мияби. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">И пишите мне почаще в основной аккаунт! Делитесь своими результатами! Я буду ждать!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Ваш Тренер.&nbsp;<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/2764.svg);">&nbsp;</span> </span></p><p data-f-id="pbf" style="text-align: center; font-size: 0px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Edi</a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a></p>`,
            "day": `12`,
            "week": `Friday`
          },
          {
            "details" :`<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Привет, мои дорогие!😍</span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Как вы там у меня?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Каков ваш настрой? Боевой?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Или всё, решили расслабиться? 😄</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Наш марафон подошёл к концу и теперь вам предстоит самостоятельная работа.&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я очень надеюсь, что за эти три недели вы узнали для себя что-то новое и приобрели знания, которые пойдут вам на пользу.🙏🏼😇</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы мне написали так много тёплых слов!!!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мои дорогие, я тоже хочу сказать вам всем от себя</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">БОЛЬШОЕ СПАСИБО ❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше доверие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше терпение, за вашу силу воли, за ваше стремление к лучшему!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше трудолюбие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я уже говорила вам, вы моя гордость, моя мотивация!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы вносите большой вклад в становление Системы Мияби.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы многому меня учите. Именно благодаря вам &nbsp;Мияби совершенствуется.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо вам за это!🙏🏻❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мне с вами было очень приятно работать! 😇 Не скажу, что легко, но очень интересно!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Очень хочется надеяться, что после окончания марафона наше с вами общение будет продолжаться. 🙏🏼😇😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">На своей основной страничке @vita_bodylove сегодня я опубликую пост &nbsp;&quot;Отзывы&quot; с обращением к моим подписчикам и вам, участникам последнего марафона.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Дорогие мои, я буду вам очень благодарна, если вы найдёте минутку, и оставите в комментариях к публикации свой отзыв о марафоне, напишите о своих впечатлениях, или о своих изменениях. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Ваши отзывы очень важны не только мне, но всем, кто только встал на путь омоложения.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">А я в свою очередь, в благодарность за оставленные отзывы, между всеми, кто меня поддержит, разыграю 10 призовых билетов в любые из марафонов Мияби. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">И пишите мне почаще в основной аккаунт! Делитесь своими результатами! Я буду ждать!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Ваш Тренер.&nbsp;<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/2764.svg);">&nbsp;</span> </span></p><p data-f-id="pbf" style="text-align: center; font-size: 0px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Edi</a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a></p>`,
            "day": `13`,
            "week": `Saturday`
          },
          {
            "details" :`<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Привет, мои дорогие!😍</span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Как вы там у меня?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Каков ваш настрой? Боевой?</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Или всё, решили расслабиться? 😄</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Наш марафон подошёл к концу и теперь вам предстоит самостоятельная работа.&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я очень надеюсь, что за эти три недели вы узнали для себя что-то новое и приобрели знания, которые пойдут вам на пользу.🙏🏼😇</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы мне написали так много тёплых слов!!!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мои дорогие, я тоже хочу сказать вам всем от себя</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">БОЛЬШОЕ СПАСИБО ❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше доверие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше терпение, за вашу силу воли, за ваше стремление к лучшему!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо за ваше трудолюбие!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Я уже говорила вам, вы моя гордость, моя мотивация!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы вносите большой вклад в становление Системы Мияби.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Вы многому меня учите. Именно благодаря вам &nbsp;Мияби совершенствуется.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Спасибо вам за это!🙏🏻❤️</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Мне с вами было очень приятно работать! 😇 Не скажу, что легко, но очень интересно!&nbsp;</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Очень хочется надеяться, что после окончания марафона наше с вами общение будет продолжаться. 🙏🏼😇😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">На своей основной страничке @vita_bodylove сегодня я опубликую пост &nbsp;&quot;Отзывы&quot; с обращением к моим подписчикам и вам, участникам последнего марафона.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Дорогие мои, я буду вам очень благодарна, если вы найдёте минутку, и оставите в комментариях к публикации свой отзыв о марафоне, напишите о своих впечатлениях, или о своих изменениях. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">Ваши отзывы очень важны не только мне, но всем, кто только встал на путь омоложения.</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">А я в свою очередь, в благодарность за оставленные отзывы, между всеми, кто меня поддержит, разыграю 10 призовых билетов в любые из марафонов Мияби. 🙏🏼😘</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">⠀</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;">И пишите мне почаще в основной аккаунт! Делитесь своими результатами! Я буду ждать!</span></span></p><p><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><br></span></span></p><p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Ваш Тренер.&nbsp;<span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/2764.svg);">&nbsp;</span> </span></p><p data-f-id="pbf" style="text-align: center; font-size: 0px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Edi</a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a><a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor"></a></p>`,
            "day": `14`,
            "week": `Sunday`
          },
        ]
        }
        //>...............
      
        getDayDetails(day){
          this.daydetails = this.daydata[day]; 
        }


      }
