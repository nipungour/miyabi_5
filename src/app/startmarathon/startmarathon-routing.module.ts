import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StartmarathonPage } from './startmarathon.page';

const routes: Routes = [
  {
    path: '',
    component: StartmarathonPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StartmarathonPageRoutingModule {}
