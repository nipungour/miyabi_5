import { LanguageServiceProvider } from '../language-service/language-service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MarathanProvider } from '../../../marathan/marathanService';
import { NavController, ModalController } from '@ionic/angular';
import { NavParams, MenuController } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage{
  getMarathon: any[] =[];
  apiError :any ;
  languageType: any;
  
  showdetails :any;
  langCode:any;
  constructor(private router: Router,public navCtrl: NavController, public navParams: NavParams , public _MarathonService : MarathanProvider ,
    public languageService : LanguageServiceProvider , public modalCtrl : ModalController,public menuCtrl:MenuController) {
      this.langCode=localStorage.getItem("languageType");
      var languageCode = localStorage.getItem("languageType");
      this.languageType = this.languageService.languageCode(languageCode);
    }

    ionViewDidLoad() {
      this.langCode = 'Rus';
        this._MarathonService.GetMarathons().subscribe(response => {
        
          this.getMarathon = response.json();
          console.log("get new marathon TEts"   , this.getMarathon);
          localStorage.setItem("MarathonName", this.getMarathon[0].title)
           localStorage.setItem("startDate" , this.getMarathon[0].startDate);
           localStorage.setItem("currentMarathonId", this.getMarathon[0].id);
         let image=  localStorage.setItem("imagePath", this.getMarathon[0].imagePath);
         console.log(image);
         
        }, error => { 
          console.log("errrrr" , error.text());
          this.apiError =error.text()
          
        });
      }
    
      ionViewWillEnter() {
        this.menuCtrl.enable(false);
      }
  start(){
    this.router.navigate(['startmarathon']);
  }
  login(){
    this.router.navigate(['login']);
  }
  register(){
    this.router.navigate(['register']);
  }
  
  english() {
    localStorage.setItem("languageType", "Eng")
    this.langCode = "Eng"
    this.languageType = this.languageService.languageCode("Eng");
  }

  russian() {
    localStorage.setItem("languageType", "Rus")
    this.langCode = "Rus"
    this.languageType = this.languageService.languageCode("Rus");
  }
  italian() {
    localStorage.setItem("languageType", "Ita")
    this.langCode = "Ita"
    this.languageType = this.languageService.languageCode("Ita");
  }
  
  modalClose(){
    this.showdetails =null;
  }
  daySpelling(day) {
    if (this.langCode == 'Rus') {
      if (day == 1 || day == 21 || day == 31 || day == 41 || day == 51 || day == 61 || day == 71 || day == 81 || day == 91) {
        return 'день';
      } else if ((day > 1 && day < 5) || (day > 21 && day < 25) || (day > 31 && day < 35) || (day > 41 && day < 45) || (day > 51 && day < 55) || (day > 61 && day < 65) || (day > 71 && day < 75) || (day > 81 && day < 85) || (day > 91 && day < 95)) {
        return 'дня';
      } else {
        return 'дней';
      }
    } else if (this.langCode == 'Eng') {
      if (day == 1) {
        return 'day';
      } else {
        return 'days';
      }
    } else {
      if (day == 1) {
        return 'giorno';
      } else {
        return 'giorni';
      }
    }
  }
  dateFormatConvert(date){
    var dateFormated= date.split('T').reverse()[1];
    var reverseDate = dateFormated.split('-').reverse().join().replace(/,/g , '.');
    return reverseDate ;
    }
    readMore(description){
      this.showdetails = description;
       }
}
