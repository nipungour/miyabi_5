import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import language from '../../language.json'

@Injectable()
export class LanguageServiceProvider {

  languageType :any ;
  currentLanguage : any;

  constructor(public http: HttpClient) {
   this.languageType = localStorage.getItem("languageType");
 
  }


  languageCode(languageCode){
    if(languageCode == "Rus"){
   return language.Rus ;
    }else if(languageCode == "Ita"){
         return language.Ita;                                                     
    }
  else{
    return language.Eng  ; 
  }
  }

}
