
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HttpClientModule }    from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'startmarathon',
    loadChildren: () => import('./startmarathon/startmarathon.module').then( m => m.StartmarathonPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'usermarathon',
    loadChildren: () => import('./usermarathon/usermarathon.module').then( m => m.UsermarathonPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },
 
  {
    path: 'orderlist',
    loadChildren: () => import('./orderlist/orderlist.module').then( m => m.OrderlistPageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./payment/payment.module').then( m => m.PaymentPageModule)
  },
 
];

@NgModule({
  imports: [
    HttpClientModule,RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  providers: [ NativeStorage],
  exports: [RouterModule]
})


export class AppRoutingModule { }
