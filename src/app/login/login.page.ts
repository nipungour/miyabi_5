import { AuthenticationService } from '../services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, MenuController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { Router } from '@angular/router';
import { LanguageServiceProvider } from '../language-service/language-service';
import { MarathanProvider } from '../../../marathan/marathanService';
import {  NavParams } from '@ionic/angular';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
   password: any;
   email: any;
  instagramId: any;
  lname: any;
  fname: any;
  activePage: any;
  form:any;
  languageType:any;
  whichPage:any;
  loginpage:boolean;
  registerpage:boolean;
  loader:any
  constructor(
    public authService: AuthenticationService,
    private modalController: ModalController,
    private navCtrl: NavController,
    private alertService: AlertService,
    private router: Router,
    public MarathonService : MarathanProvider ,
     public navParams: NavParams ,
    public languageService :LanguageServiceProvider,
    public menuCtrl:MenuController) {
    this.activePage = 'login';
    this.setLanguage();
  }
   
  setLanguage(){
    var  languageCode =localStorage.getItem("languageType");
    this.languageType =  this.languageService.languageCode(languageCode);
    console.log(this.languageType,'test');
  }
 
  ngOnInit() {
  }

  // lang start.................


  ionViewDidLoad() {
    var  languageCode =localStorage.getItem("languageType");
    this.languageType =  this.languageService.languageCode(languageCode);
    console.log(this.languageType,'test');
    this.whichPage = this.navParams.get('page');
    if (this.whichPage == 'login') {
      this.loginpage = true;
    } else {
      this.registerpage = true;
    }

  }
  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }
  presentLoading() {
    var loaderMessage :any ;
    if(localStorage.getItem("languageType") == 'Eng'){
      loaderMessage = 'Please wait a bit...';
      }  else if(localStorage.getItem("languageType") == 'Rus') {
        loaderMessage = 'чуточку подождите...';
      } else{
        loaderMessage = 'per favore aspetta un po ...';
      }
    
      let loader = this.loader.create({
        content: loaderMessage
      })
      loader.present();
  };


  // lang finish...................

  dismissRegister() {
    this.modalController.dismiss();
  }
  // On Login button tap, dismiss Register modal and open login Modal
  async logindis() {
    this.dismissRegister();
    const loginModal = await this.modalController.create({
      component: LoginPage,
    });
    return await loginModal.present();
  }
  // Dismiss Login Modal
  dismissLogin() {
    this.modalController.dismiss();
  }
  // On Register button tap, dismiss login modal and open register modal
  async registerdis() {
    this.dismissLogin();
    const registerModal = await this.modalController.create({
      component: LoginPage
    });
    return await registerModal.present();
  }
  activepage(page) {
    this.activePage = page;
  }

 
  login(form: NgForm) { 
    this.authService.login(form.value.email, form.value.password, 'password').subscribe(
      data => {
        let token_Detail = data.json()
        console.log('token_Detail',token_Detail);
        localStorage.setItem("access_token", token_Detail.access_token);
        localStorage.setItem("email", token_Detail.email);
        localStorage.setItem("password", token_Detail.password);
        localStorage.setItem("role", token_Detail.role);
        localStorage.setItem("user_name", token_Detail.username);
        localStorage.setItem("user_email", token_Detail.email);
        localStorage.setItem("expires_in", token_Detail.expires_in);
        localStorage.setItem("refresh_token", token_Detail.refresh_token);
        this.router.navigate(['orderlist']);
        
      },
      error => {
        console.log('error',error);
      }
    );
  }
  register(form: NgForm) {
    let data= {'email': form.value.email, 'fname': form.value.fname,'lname' :form.value.lname, 'instagramId': form.value.instagramId};
    this.authService.UserRegister(data).subscribe(
      data => {
        console.log(data,'data');
      },
      error => {
        let err =  error.json();
        alert(err._body);
        console.log(error);
      },
      () => {
        
      }
    );
  }
}

