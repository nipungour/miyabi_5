import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LanguageServiceProvider } from '../language-service/language-service';
import { MarathanProvider } from '../../../marathan/marathanService';
import { NavController, ModalController } from '@ionic/angular';
import { NavParams, MenuController } from '@ionic/angular';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  languageType:any;
  langCode:any;
  constructor(private router: Router,public navCtrl: NavController, public navParams: NavParams , public _MarathonService : MarathanProvider ,
    public languageService : LanguageServiceProvider , public modalCtrl : ModalController,public menuCtrl:MenuController) {
      this.langCode=localStorage.getItem("languageType");
      var languageCode = localStorage.getItem("languageType");
      this.languageType = this.languageService.languageCode(languageCode);
    }
  ngOnInit() {
  }
  editprofile(){
    this.router.navigate(['editprofile']);
  }
  // contest(){
  //   this.router.navigate(['']);
  // }
 orderlist(){
    this.router.navigate(['orderlist']);
  }
  logout(){
    this.router.navigate(['login']);
  }
}
