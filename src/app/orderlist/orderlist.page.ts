import { Component, OnInit,ViewChild } from '@angular/core'; 
import { LanguageServiceProvider } from '../language-service/language-service';
import { Router } from '@angular/router';
import { MarathanProvider } from '../../../marathan/marathanService';
import { NavController, ModalController,IonContent } from '@ionic/angular';
import { NavParams, MenuController,LoadingController,AlertController } from '@ionic/angular';
import { StartmarathonPage } from '../startmarathon/startmarathon.page';
import { UsermarathonPage } from '../usermarathon/usermarathon.page';
import { MarathonData } from '../../marathonDataInterface.model';
import { PaymentPage } from '../payment/payment.page';
@Component({
  selector: 'app-orderlist',
  templateUrl: './orderlist.page.html',
  styleUrls: ['./orderlist.page.scss'],
})
export class OrderlistPage implements OnInit {
  @ViewChild(IonContent ,{static:false}) private container: IonContent;
  langCode;any;
  languageType:any;
  show: boolean;
  iserror: boolean;
  error: any;
  marathonlistdata: [];
  allMarathon: any;
  textCopy: boolean;
  contestShow: boolean;
  showdetails: any;
  itemsDetails: any;
  extensiondetail: any;
  greatextensiondetail: any;
  discription: any;
  paymet: any;
  finalDescription: any;
  purchaseDescription: any;
  item:any;
  toastCtrl:any;
  MarathonData:any;

  
   constructor(private router: Router,public navCtrl: NavController, public navParams: NavParams , public marathonService : MarathanProvider ,
    public languageService : LanguageServiceProvider , public modalCtrl : ModalController,public menuCtrl:MenuController,public loader: LoadingController, public alrtCtrl: AlertController,) {
      this.langCode=localStorage.getItem("languageType");
      var languageCode = localStorage.getItem("languageType");
      this.languageType = this.languageService.languageCode(languageCode);
      this.english();
    }

  ngOnInit() {
  }
  menu(){
    this.router.navigate(['menu']);
  }
  
  english() {
    localStorage.setItem("languageType", "Eng")
    this.langCode = "Eng";
    this.marathonList()
    var languageCode = localStorage.getItem("languageType");
    this.languageType = this.languageService.languageCode(languageCode);
    this.langCode = localStorage.getItem("languageType");
  }
  getUserOrderList
  russian() {
    localStorage.setItem("languageType", "Rus")
    this.langCode = "Rus";
    this.marathonList()
    var languageCode = localStorage.getItem("languageType");
    this.languageType = this.languageService.languageCode(languageCode);
    this.langCode = localStorage.getItem("languageType");

  }
  italian() {
    localStorage.setItem("languageType", "Ita")
    this.langCode = "Ita";
    this.marathonList()
    var languageCode = localStorage.getItem("languageType");
    this.languageType = this.languageService.languageCode(languageCode);
    this.langCode = localStorage.getItem("languageType");
  }
  
 


  marathonList() {
    this.show = true;
    this.marathonService.getUserOrderList().subscribe(response => {
      console.log('response New orders', response.json())
      this.show = false;
      this.iserror = false;
      this.allMarathon = response.json();
    }, error => {
      this.error = error.text();
    });
  }
  copyCoupon(val: string) {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.textCopy = true;
    let toast = this.toastCtrl.create({
      message: 'Код купона скопирован',
      duration: 3000
      , position: "bottom"
    })
    toast.present();
  }
  readMore(description) {
    this.showdetails = description;
  }
  dateFormatConvert(date) {
    var dateFormated = date.split('T').reverse()[1];
    var reverseDate = dateFormated.split('-').reverse().join().replace(/,/g, '.');
    return reverseDate;
  }
  pay(){
    this.router.navigate(['payment']);
  }
  purchaseMarathonWithModel(id, cost, item, extension) {
    localStorage.removeItem("orderStatus");
    localStorage.removeItem("marathonId");
    localStorage.removeItem("extension");
    localStorage.setItem("marathonId", id);
    localStorage.setItem('exteension', item.isExtension);
    localStorage.setItem('cost', cost);
    localStorage.setItem('extension', extension);
    // this.navCtrl.setRoot(PaypalPaymentPage, { showpayment: true });
    this.router.navigate([PaymentPage,{ showpayment: true }]);
  }
  
  purchaseMarathon(id, cost, item, extension) {
    localStorage.removeItem("orderStatus");
    localStorage.removeItem("marathonId");
    localStorage.removeItem("extension");
    localStorage.setItem("marathonId", id);
    localStorage.setItem('exteension', item.isExtension);
    localStorage.setItem('cost', cost);
    localStorage.setItem('extension', extension);
    // this.navCtrl.setRoot(PaypalPaymentPage, { showpayment: true });
    this.router.navigate([PaymentPage,{ showpayment: true }]);
  }
  setItemsDetaisl(data){
   
    this.itemsDetails = data;
  }
  daySpelling(day) {
    if (localStorage.getItem("languageType") == 'Rus') {
      if (day == 1 || day == 21 || day == 31 || day == 41 || day == 51 || day == 61 || day == 71 || day == 81 || day == 91) {
        return 'день';
      } else if ((day > 1 && day < 5) || (day > 21 && day < 25) || (day > 31 && day < 35) || (day > 41 && day < 45) || (day > 51 && day < 55) || (day > 61 && day < 65) || (day > 71 && day < 75) || (day > 81 && day < 85) || (day > 91 && day < 95)) {
        return 'дня';
      } else {
        return 'дней';
      }
    } else if (localStorage.getItem("languageType") == 'Eng') {
      if (day == 1) {
        return 'day';
      } else {
        return 'days';
      }
    } else {
      if (day == 1) {
        return 'giorno';
      } else {
        return 'giorni';
      }
    }
  }
  async goToMarathon(id, extension) {

    localStorage.setItem('extension', extension)
    var loaderMessage: any;
    if (localStorage.getItem("languageType") == 'Eng') {
      loaderMessage = 'Please wait a bit...';
    } else if (localStorage.getItem("languageType") == 'Rus') {
      loaderMessage = 'чуточку подождите...';
    } else {
      loaderMessage = 'per favore aspetta un po ...';
    }

    let loading = this.loader.create({
      message: 'Loading...'
    });
  
    // await loading.present();
    localStorage.removeItem("marathonId");
    localStorage.setItem("marathonId", id);
    // localStorage.setItem("currentContestMarathonId" , id);
    localStorage.setItem("orderStatus", 'Approved');


    this.marathonService.getUserMarathonDetail(id).subscribe(response => {

      var data: MarathonData = response.json()
      console.log("marathonlist", data);

      // loader.dismissAll();
      if (data.marathonDays == null) {
        
        this.router.navigate([StartmarathonPage,{ user_marathan_id: id }]);
      }
      if (data.marathonDays != null) {

        if (data.marathonDays.length > 0) {
          let isActive = false;
          for (let index = 0; index < data.marathonDays.length; index++) {
            if (data.marathonDays[index].isActive == true) {
              isActive = true;
              var dayId = data.marathonDays[index].id;
              localStorage.setItem("day_id", dayId);             
              this.router.navigate([UsermarathonPage,{ user_marathan_id: id }]);

            }
          }
          if (!isActive) {
            this.router.navigate([StartmarathonPage,{ user_marathan_id: id }]);
          }
        }
      }
    });
  }
  menubar(){
    this.router.navigate(['login']);
  }
  goToStartMarathon() {
    let id = localStorage.getItem("marathonId");
    var loaderMessage: any;
    if (localStorage.getItem("languageType") == 'Eng') {
      loaderMessage = 'Please wait a bit...';
    } else if (localStorage.getItem("languageType") == 'Rus') {
      loaderMessage = 'чуточку подождите...';
    } else {
      loaderMessage = 'per favore aspetta un po ...';
    }
    
    let loading = this.loader.create({
      message: 'Loading...'
    });


    if (localStorage.getItem('orderStatus') == 'Approved') {
      // loader.present();
      this.marathonService.getUserMarathonDetail(id).subscribe(response => {

        var data: MarathonData = response.json()
        localStorage.setItem('marathonTitle', data.title);
        console.log("startMarathondetail", data);
        // loader.dismissAll();
        if (data.marathonDays == null) {         
          this.router.navigate([StartmarathonPage,{ user_marathan_id: id }]);
        }
        if (data.marathonDays != null) {

          if (data.marathonDays.length > 0) {
            let isActive = false;
            for (let index = 0; index < data.marathonDays.length; index++) {
              if (data.marathonDays[index].isActive == true) {
                isActive = true;
                var dayId = data.marathonDays[index].id;
                localStorage.setItem("day_id", dayId);
                this.router.navigate([UsermarathonPage,{ user_marathan_id: id }]);

              }

            }

            if (!isActive) {
              this.router.navigate([StartmarathonPage,{ user_marathan_id: id }]);
            }
          }
        }
      }, error => {
      });
    } else {
      this.router.navigate([PaymentPage]);
    }


  }
}
