import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsermarathonPage } from './usermarathon.page';

const routes: Routes = [
  {
    path: '',
    component: UsermarathonPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsermarathonPageRoutingModule {}
