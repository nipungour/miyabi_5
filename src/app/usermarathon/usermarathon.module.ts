import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UsermarathonPageRoutingModule } from './usermarathon-routing.module';

import { UsermarathonPage } from './usermarathon.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UsermarathonPageRoutingModule
  ],
  declarations: [UsermarathonPage]
})
export class UsermarathonPageModule {}
