import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UsermarathonPage } from './usermarathon.page';

describe('UsermarathonPage', () => {
  let component: UsermarathonPage;
  let fixture: ComponentFixture<UsermarathonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsermarathonPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UsermarathonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
