(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content  >\n   <div class=\"miyabi-container login-body martahon-bg\">\n        <div>\n            <div class=\"logo-detail\" >\n              <img  style=\"text-align: center; margin-top:20px \" class=\"\" src=\"../../assets/icon/logo-details-sm.svg\"/>\n            </div>\n        </div> \n         <form #form=\"ngForm\" (ngSubmit)=\"login(form)\">\n            <div class=\"container\">\n                <div class=\"login-section\">\n                    <div class=\"login-block\">\n                      <ion-grid>\n                        <ion-row class=\"nav nav-tabs\">\n                        <ion-col size=\"6\" class=\"nav-item\" >\n                          <a  style=\"border-top-left-radius: 14px;\"\n                          data-toggle=\"tab\" [ngClass]=\"(activePage == 'login')?'head active':'head '\" (click)=\"activepage('login')\" >\n                          {{languageType?.registerPage?.loginButtonTittle}}\n                          </a>\n                       </ion-col>\n                       <ion-col size=\"6\" class=\"nav-item\" >\n                        <a style=\"border-top-right-radius: 14px;\" data-toggle=\"tab\" [ngClass]=\"(activePage == 'register')?'head active':'head '\" (click)=\"activepage('register')\">\n                          {{languageType?.registerPage?.registerButtonTitle}}\n                         </a>\n                    </ion-col>\n                  </ion-row>\n               </ion-grid>\n               \n               <!-- login form -->\n               \n                 <div *ngIf=\"activePage == 'login'\" >\n                      <div  class=\"form\" (ngSubmit)=\"login(form)\">\n                        <input type=\"text\" [(ngModel)]=\"email\"  class=\"form-control ng-untouched ng-pristine ng-valid\" placeholder=\"Enter Username\" name=\"email\" required >\n                        <input type=\"password\" [(ngModel)]=\"password\"  class=\"form-control ng-untouched ng-pristine ng-valid\" placeholder=\"Enter password\" name=\"password\" required>\n                      </div>\n                        <a  class=\"forget\" >\n                          {{languageType?.registerPage?.forgotPasswordTitle}}\n                        </a>\n                        <button type=\"submit\"  expand=\"full\" class=\"btn btn-login\"  >\n                                  {{languageType?.registerPage?.loginButtonTittle}}\n                        </button>\n                </div>\n                \n              <!-- Registration form -->\n                    <div *ngIf=\"activePage == 'register'\"  >\n                      <div class=\"form\" (ngSubmit)=\"register(form)\">\n                          <input type=\"text\" [(ngModel)]=\"email\" class=\"form-control ng-untouched ng-pristine ng-valid\" placeholder=\"{{languageType?.registerPage?.emailPlaceholder}}\" name=\"email\" pattern=\".*\\\\S.*[a-zA-Zа-яА-Я0-9.-_!#$%^&*()]{1,}@[a-zA-Zа-яА-Я.-]{2,}[.]{1}[a-zA-Zа-яА-Я]{1,}.*\\\\S.*\"  required >\n                               <span style=\"color: #5296aa;\">\n                                {{languageType?.registerPage?.passwordSentToEmailAddressMessage}}\n                                </span>\n                                    <input type=\"text\" [(ngModel)]=\"fname\" class=\"form-control ng-untouched ng-pristine ng-valid\" placeholder=\"{{languageType?.registerPage?.firstNamePlaceholder}}\" name=\"fname\" required >\n                                    <input type=\"text\" [(ngModel)]=\"lname\" class=\"form-control ng-untouched ng-pristine ng-valid\" placeholder=\"{{languageType?.registerPage?.lastNamePlaceholder}}\" name=\"lname\" required >\n                                    <input type=\"text\" [(ngModel)]=\"instagramId\" class=\"form-control ng-untouched ng-pristine ng-valid\" placeholder=\"{{languageType?.registerPage?.instagrameIdPlaceholder}}\" name=\"instagramId\" required >\n                      </div>\n                       <button   type=\"submit\" class=\"btn btn-login\">\n                        {{languageType?.registerPage?.registerButtonTitle}}\n                      </button>\n                      </div>\n                    </div>\n                </div>\n           </div>\n       </form>\n   </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/login/login-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/login/login-routing.module.ts ***!
  \***********************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/login/login-routing.module.ts");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("body {\n  background-color: #fefefe;\n}\n\nbody {\n  background-color: #fefefe;\n  font-family: \"MyriadProRegular\";\n  min-width: 320px;\n  max-width: 100vw;\n  overflow-x: hidden;\n}\n\na {\n  color: #70d1cc;\n}\n\na:not([href]):not([tabindex]) {\n  text-decoration: none;\n}\n\n.login-body {\n  color: #717171;\n  overflow-x: hidden;\n  position: relative;\n  background-image: url('mma-background-375.png');\n  background-size: 100% 100%;\n  min-height: 100vh;\n}\n\n.logo-detail {\n  text-align: center;\n  padding: 0 0;\n  max-width: 80%;\n  width: calc(100vw - 30px);\n  margin-left: 14%;\n}\n\na.agreement {\n  color: #70d1cc;\n  display: flex;\n  align-items: flex-end;\n  justify-content: center;\n  text-align: center;\n  padding-top: 6px;\n  line-height: 15px;\n}\n\na.agreement img {\n  height: 23px;\n  margin-left: 10px;\n}\n\n.miyabi-container {\n  min-width: 320px;\n  margin: 0 auto;\n}\n\n.martahon-bg {\n  position: relative;\n  display: block;\n}\n\n.martahon-bg .bg-img-right {\n  position: absolute;\n  left: 100%;\n  top: 0;\n  max-height: 100%;\n}\n\n.martahon-bg .bg-img-left {\n  position: absolute;\n  right: 100%;\n  top: 0;\n  max-height: 100%;\n}\n\n.btn-login {\n  background-image: linear-gradient(#70d1cc, #007e9e);\n  padding: 18px 30px;\n  color: white;\n  font-size: 37.4px;\n  box-shadow: none;\n  line-height: 44.88px;\n  border-radius: 60px;\n  border: 0;\n  display: block;\n  margin: 0 auto;\n  min-width: 283px;\n}\n\n.login-section {\n  text-align: center;\n  height: 100%;\n  display: flex;\n  justify-content: space-between;\n  align-items: flex-start;\n  flex-direction: column;\n}\n\n.login-block {\n  background-color: #f7f7f7;\n  padding: 17px 0;\n  width: auto;\n  display: inline-block;\n  margin: 0 auto;\n  text-align: center;\n  padding-top: 0;\n  border-radius: 14px;\n  max-width: 350px;\n  width: calc(100vw - 30px);\n  margin-top: 2vh;\n  margin-bottom: 0;\n  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.1607843137);\n}\n\n.head,\n.head:hover {\n  font-size: 20.9px;\n  line-height: 31.08px;\n  background-color: #e567ac;\n  color: white !important;\n  padding: 11px 5px;\n  display: block;\n  background-image: none;\n  box-shadow: none;\n  margin-bottom: 0 !important;\n  width: 100%;\n  font-family: \"MyriadProRegular\";\n}\n\n.nav-tabs .nav-item {\n  margin-bottom: 0 !important;\n}\n\n.head.active,\n.head.active:hover {\n  font-size: 22.9px;\n  line-height: 31.08px;\n  color: #e567ac !important;\n  background-color: transparent;\n  padding: 11px 5px;\n  font-family: \"MyriadProRegular\";\n}\n\n.login-section .nav {\n  min-height: 53px;\n}\n\n.btn:hover {\n  color: #fff;\n  text-decoration: none;\n}\n\n.form {\n  padding: 0 30px;\n}\n\n.form-control {\n  height: 50px;\n  font-size: 24px;\n  line-height: 30px;\n  color: #717171;\n  border: 1px solid #70d1cc;\n  border-radius: 80px;\n  display: block;\n  text-align: center;\n  margin-bottom: 10px;\n  width: 300px;\n  padding: 20px 10px;\n  max-width: 100%;\n}\n\n.form-control::-webkit-input-placeholder {\n  text-align: center;\n}\n\n.form-control::-moz-placeholder {\n  text-align: center;\n}\n\n.form-control::-ms-input-placeholder {\n  text-align: center;\n}\n\n.form-control::placeholder {\n  text-align: center;\n}\n\n.forget {\n  color: #70d1cc;\n  font-size: 18px;\n  line-height: 19px;\n  text-decoration: underline;\n  margin-top: 15px;\n  margin-bottom: 12px;\n  display: inline-block;\n}\n\n.nav {\n  padding: 0;\n}\n\n.nav {\n  border-bottom: 0;\n  margin-bottom: 15px;\n}\n\n.nav-tabs .nav-item {\n  margin-bottom: -1px;\n  width: 50%;\n  text-align: center;\n  height: 100%;\n}\n\n.scroll-content {\n  margin-top: 0px !important;\n}\n\n.miyabi-container.martahon-bg {\n  box-shadow: 0px 0px 21px #33333330;\n  min-height: 100vh;\n  min-width: 100vw !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3Bpem9uZS9teWRheXMvc3JjL2FwcC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLHlCQUFBO0FDQUo7O0FER0U7RUFDRSx5QkFBQTtFQUNBLCtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDQUo7O0FER0U7RUFDRSxjQUFBO0FDQUo7O0FER0U7RUFFRSxxQkFBQTtBQ0RKOztBRElFO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSwrQ0FBQTtFQUNBLDBCQUFBO0VBQ0EsaUJBQUE7QUNESjs7QURJRTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0FDREo7O0FESUU7RUFDRSxjQUFBO0VBQ0EsYUFBQTtFQUNBLHFCQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUNESjs7QURHSTtFQUNFLFlBQUE7RUFDQSxpQkFBQTtBQ0ROOztBREtFO0VBQ0UsZ0JBQUE7RUFDQSxjQUFBO0FDRko7O0FES0U7RUFDRSxrQkFBQTtFQUNBLGNBQUE7QUNGSjs7QURLRTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLE1BQUE7RUFDQSxnQkFBQTtBQ0ZKOztBREtFO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsTUFBQTtFQUNBLGdCQUFBO0FDRko7O0FES0U7RUFDRSxtREFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EsU0FBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUNGSjs7QURLRTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSw4QkFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7QUNGSjs7QURLRTtFQUNFLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpREFBQTtBQ0ZKOztBREtFOztFQUVFLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSx5QkFBQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQkFBQTtFQUNBLDJCQUFBO0VBQ0EsV0FBQTtFQUNBLCtCQUFBO0FDRko7O0FES0U7RUFDRSwyQkFBQTtBQ0ZKOztBREtFOztFQUVFLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSx5QkFBQTtFQUNBLDZCQUFBO0VBQ0EsaUJBQUE7RUFDQSwrQkFBQTtBQ0ZKOztBREtFO0VBQ0UsZ0JBQUE7QUNGSjs7QURLRTtFQUNFLFdBQUE7RUFDQSxxQkFBQTtBQ0ZKOztBREtFO0VBQ0UsZUFBQTtBQ0ZKOztBREtFO0VBQ0UsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDRko7O0FESUk7RUFDRSxrQkFBQTtBQ0ZOOztBRENJO0VBQ0Usa0JBQUE7QUNGTjs7QURDSTtFQUNFLGtCQUFBO0FDRk47O0FEQ0k7RUFDRSxrQkFBQTtBQ0ZOOztBRE1FO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLDBCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0FDSEo7O0FETUU7RUFDRSxVQUFBO0FDSEo7O0FEUUU7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FDTEo7O0FEUUU7RUFDRSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNMSjs7QURRRTtFQUNFLDBCQUFBO0FDTEo7O0FET0U7RUFFRSxrQ0FBQTtFQUNBLGlCQUFBO0VBQ0EsMkJBQUE7QUNKSiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuICBib2R5IHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjU0LCAyNTQsIDI1NCk7XG4gIH1cblxuICBib2R5IHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjU0LCAyNTQsIDI1NCk7XG4gICAgZm9udC1mYW1pbHk6IFwiTXlyaWFkUHJvUmVndWxhclwiO1xuICAgIG1pbi13aWR0aDogMzIwcHg7XG4gICAgbWF4LXdpZHRoOiAxMDB2dztcbiAgICBvdmVyZmxvdy14OiBoaWRkZW47XG4gIH1cblxuICBhIHtcbiAgICBjb2xvcjogIzcwZDFjYztcbiAgfVxuXG4gIGE6bm90KFtocmVmXSk6bm90KFt0YWJpbmRleF0pIHtcbiAgIFxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgfVxuXG4gIC5sb2dpbi1ib2R5IHtcbiAgICBjb2xvcjogIzcxNzE3MTtcbiAgICBvdmVyZmxvdy14OiBoaWRkZW47XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uL2Fzc2V0cy9pY29uL21tYS1iYWNrZ3JvdW5kLTM3NS5wbmdcIik7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgbWluLWhlaWdodDogMTAwdmg7XG4gIH1cblxuICAubG9nby1kZXRhaWwge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAwIDA7XG4gICAgbWF4LXdpZHRoOiA4MCU7XG4gICAgd2lkdGg6IGNhbGMoMTAwdncgLSAzMHB4KTtcbiAgICBtYXJnaW4tbGVmdDogMTQlO1xuICB9XG5cbiAgYS5hZ3JlZW1lbnQge1xuICAgIGNvbG9yOiAjNzBkMWNjO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogNnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNXB4O1xuXG4gICAgaW1nIHtcbiAgICAgIGhlaWdodDogMjNweDtcbiAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgIH1cbiAgfVxuXG4gIC5taXlhYmktY29udGFpbmVyIHtcbiAgICBtaW4td2lkdGg6IDMyMHB4O1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICB9XG5cbiAgLm1hcnRhaG9uLWJnIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cblxuICAubWFydGFob24tYmcgLmJnLWltZy1yaWdodCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDEwMCU7XG4gICAgdG9wOiAwO1xuICAgIG1heC1oZWlnaHQ6IDEwMCU7XG4gIH1cblxuICAubWFydGFob24tYmcgLmJnLWltZy1sZWZ0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDEwMCU7XG4gICAgdG9wOiAwO1xuICAgIG1heC1oZWlnaHQ6IDEwMCU7XG4gIH1cblxuICAuYnRuLWxvZ2luIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoIzcwZDFjYywgIzAwN2U5ZSk7XG4gICAgcGFkZGluZzogMThweCAzMHB4O1xuICAgIGNvbG9yOiByZ2IoMjU1LCAyNTUsIDI1NSk7XG4gICAgZm9udC1zaXplOiAzNy40cHg7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBsaW5lLWhlaWdodDogNDQuODhweDtcbiAgICBib3JkZXItcmFkaXVzOiA2MHB4O1xuICAgIGJvcmRlcjogMDtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICBtaW4td2lkdGg6IDI4M3B4O1xuICB9XG5cbiAgLmxvZ2luLXNlY3Rpb24ge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgfVxuXG4gIC5sb2dpbi1ibG9jayB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcbiAgICBwYWRkaW5nOiAxN3B4IDA7XG4gICAgd2lkdGg6IGF1dG87XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICBib3JkZXItcmFkaXVzOiAxNHB4O1xuICAgIG1heC13aWR0aDogMzUwcHg7XG4gICAgd2lkdGg6IGNhbGMoMTAwdncgLSAzMHB4KTtcbiAgICBtYXJnaW4tdG9wOiAydmg7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICBib3gtc2hhZG93OiAwIDNweCA2cHggcmdiYSgwLCAwLCAwLCAwLjE2MDc4NDMxMzcyNTQ5MDIpO1xuICB9XG5cbiAgLmhlYWQsXG4gIC5oZWFkOmhvdmVyIHtcbiAgICBmb250LXNpemU6IDIwLjlweDtcbiAgICBsaW5lLWhlaWdodDogMzEuMDhweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjI5LCAxMDMsIDE3Mik7XG4gICAgY29sb3I6IHJnYigyNTUsIDI1NSwgMjU1KSAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmc6IDExcHggNXB4O1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IG5vbmU7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZm9udC1mYW1pbHk6IFwiTXlyaWFkUHJvUmVndWxhclwiO1xuICB9XG5cbiAgLm5hdi10YWJzIC5uYXYtaXRlbSB7XG4gICAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmhlYWQuYWN0aXZlLFxuICAuaGVhZC5hY3RpdmU6aG92ZXIge1xuICAgIGZvbnQtc2l6ZTogMjIuOXB4O1xuICAgIGxpbmUtaGVpZ2h0OiAzMS4wOHB4O1xuICAgIGNvbG9yOiByZ2IoMjI5LCAxMDMsIDE3MikgIWltcG9ydGFudDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBwYWRkaW5nOiAxMXB4IDVweDtcbiAgICBmb250LWZhbWlseTogXCJNeXJpYWRQcm9SZWd1bGFyXCI7XG4gIH1cblxuICAubG9naW4tc2VjdGlvbiAubmF2IHtcbiAgICBtaW4taGVpZ2h0OiA1M3B4O1xuICB9XG5cbiAgLmJ0bjpob3ZlciB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICB9XG5cbiAgLmZvcm0ge1xuICAgIHBhZGRpbmc6IDAgMzBweDtcbiAgfVxuXG4gIC5mb3JtLWNvbnRyb2wge1xuICAgIGhlaWdodDogNTBweDtcbiAgICBmb250LXNpemU6IDI0cHg7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgY29sb3I6ICM3MTcxNzE7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzcwZDFjYztcbiAgICBib3JkZXItcmFkaXVzOiA4MHB4O1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIHdpZHRoOiAzMDBweDtcbiAgICBwYWRkaW5nOiAyMHB4IDEwcHg7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xuXG4gICAgJjo6cGxhY2Vob2xkZXIge1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbiAgfVxuXG4gIC5mb3JnZXQge1xuICAgIGNvbG9yOiAjNzBkMWNjO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBsaW5lLWhlaWdodDogMTlweDtcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEycHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB9XG5cbiAgLm5hdiB7XG4gICAgcGFkZGluZzogMDtcbiAgfVxuXG5cblxuICAubmF2IHtcbiAgICBib3JkZXItYm90dG9tOiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIH1cblxuICAubmF2LXRhYnMgLm5hdi1pdGVtIHtcbiAgICBtYXJnaW4tYm90dG9tOiAtMXB4O1xuICAgIHdpZHRoOiA1MCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGhlaWdodDogMTAwJTtcbiAgfVxuXG4gIC5zY3JvbGwtY29udGVudCB7XG4gICAgbWFyZ2luLXRvcDogMHB4ICFpbXBvcnRhbnQgO1xuICB9XG4gIC5taXlhYmktY29udGFpbmVyLm1hcnRhaG9uLWJnIHtcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAwcHggMjFweCAjMzMzMzMzMzA7XG4gICAgYm94LXNoYWRvdzogMHB4IDBweCAyMXB4ICMzMzMzMzMzMDtcbiAgICBtaW4taGVpZ2h0OiAxMDB2aDtcbiAgICBtaW4td2lkdGg6IDEwMHZ3ICFpbXBvcnRhbnQ7XG4gIH1cblxuIiwiYm9keSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZWZlZmU7XG59XG5cbmJvZHkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmVmZWZlO1xuICBmb250LWZhbWlseTogXCJNeXJpYWRQcm9SZWd1bGFyXCI7XG4gIG1pbi13aWR0aDogMzIwcHg7XG4gIG1heC13aWR0aDogMTAwdnc7XG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcbn1cblxuYSB7XG4gIGNvbG9yOiAjNzBkMWNjO1xufVxuXG5hOm5vdChbaHJlZl0pOm5vdChbdGFiaW5kZXhdKSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuLmxvZ2luLWJvZHkge1xuICBjb2xvcjogIzcxNzE3MTtcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uL2Fzc2V0cy9pY29uL21tYS1iYWNrZ3JvdW5kLTM3NS5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICBtaW4taGVpZ2h0OiAxMDB2aDtcbn1cblxuLmxvZ28tZGV0YWlsIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiAwIDA7XG4gIG1heC13aWR0aDogODAlO1xuICB3aWR0aDogY2FsYygxMDB2dyAtIDMwcHgpO1xuICBtYXJnaW4tbGVmdDogMTQlO1xufVxuXG5hLmFncmVlbWVudCB7XG4gIGNvbG9yOiAjNzBkMWNjO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmctdG9wOiA2cHg7XG4gIGxpbmUtaGVpZ2h0OiAxNXB4O1xufVxuYS5hZ3JlZW1lbnQgaW1nIHtcbiAgaGVpZ2h0OiAyM3B4O1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cblxuLm1peWFiaS1jb250YWluZXIge1xuICBtaW4td2lkdGg6IDMyMHB4O1xuICBtYXJnaW46IDAgYXV0bztcbn1cblxuLm1hcnRhaG9uLWJnIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuLm1hcnRhaG9uLWJnIC5iZy1pbWctcmlnaHQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDEwMCU7XG4gIHRvcDogMDtcbiAgbWF4LWhlaWdodDogMTAwJTtcbn1cblxuLm1hcnRhaG9uLWJnIC5iZy1pbWctbGVmdCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDEwMCU7XG4gIHRvcDogMDtcbiAgbWF4LWhlaWdodDogMTAwJTtcbn1cblxuLmJ0bi1sb2dpbiB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgjNzBkMWNjLCAjMDA3ZTllKTtcbiAgcGFkZGluZzogMThweCAzMHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMzcuNHB4O1xuICBib3gtc2hhZG93OiBub25lO1xuICBsaW5lLWhlaWdodDogNDQuODhweDtcbiAgYm9yZGVyLXJhZGl1czogNjBweDtcbiAgYm9yZGVyOiAwO1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luOiAwIGF1dG87XG4gIG1pbi13aWR0aDogMjgzcHg7XG59XG5cbi5sb2dpbi1zZWN0aW9uIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG5cbi5sb2dpbi1ibG9jayB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XG4gIHBhZGRpbmc6IDE3cHggMDtcbiAgd2lkdGg6IGF1dG87XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy10b3A6IDA7XG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XG4gIG1heC13aWR0aDogMzUwcHg7XG4gIHdpZHRoOiBjYWxjKDEwMHZ3IC0gMzBweCk7XG4gIG1hcmdpbi10b3A6IDJ2aDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgYm94LXNoYWRvdzogMCAzcHggNnB4IHJnYmEoMCwgMCwgMCwgMC4xNjA3ODQzMTM3KTtcbn1cblxuLmhlYWQsXG4uaGVhZDpob3ZlciB7XG4gIGZvbnQtc2l6ZTogMjAuOXB4O1xuICBsaW5lLWhlaWdodDogMzEuMDhweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U1NjdhYztcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDExcHggNXB4O1xuICBkaXNwbGF5OiBibG9jaztcbiAgYmFja2dyb3VuZC1pbWFnZTogbm9uZTtcbiAgYm94LXNoYWRvdzogbm9uZTtcbiAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1mYW1pbHk6IFwiTXlyaWFkUHJvUmVndWxhclwiO1xufVxuXG4ubmF2LXRhYnMgLm5hdi1pdGVtIHtcbiAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xufVxuXG4uaGVhZC5hY3RpdmUsXG4uaGVhZC5hY3RpdmU6aG92ZXIge1xuICBmb250LXNpemU6IDIyLjlweDtcbiAgbGluZS1oZWlnaHQ6IDMxLjA4cHg7XG4gIGNvbG9yOiAjZTU2N2FjICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBwYWRkaW5nOiAxMXB4IDVweDtcbiAgZm9udC1mYW1pbHk6IFwiTXlyaWFkUHJvUmVndWxhclwiO1xufVxuXG4ubG9naW4tc2VjdGlvbiAubmF2IHtcbiAgbWluLWhlaWdodDogNTNweDtcbn1cblxuLmJ0bjpob3ZlciB7XG4gIGNvbG9yOiAjZmZmO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi5mb3JtIHtcbiAgcGFkZGluZzogMCAzMHB4O1xufVxuXG4uZm9ybS1jb250cm9sIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICBmb250LXNpemU6IDI0cHg7XG4gIGxpbmUtaGVpZ2h0OiAzMHB4O1xuICBjb2xvcjogIzcxNzE3MTtcbiAgYm9yZGVyOiAxcHggc29saWQgIzcwZDFjYztcbiAgYm9yZGVyLXJhZGl1czogODBweDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgd2lkdGg6IDMwMHB4O1xuICBwYWRkaW5nOiAyMHB4IDEwcHg7XG4gIG1heC13aWR0aDogMTAwJTtcbn1cbi5mb3JtLWNvbnRyb2w6OnBsYWNlaG9sZGVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uZm9yZ2V0IHtcbiAgY29sb3I6ICM3MGQxY2M7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDE5cHg7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuICBtYXJnaW4tdG9wOiAxNXB4O1xuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbi5uYXYge1xuICBwYWRkaW5nOiAwO1xufVxuXG4ubmF2IHtcbiAgYm9yZGVyLWJvdHRvbTogMDtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cblxuLm5hdi10YWJzIC5uYXYtaXRlbSB7XG4gIG1hcmdpbi1ib3R0b206IC0xcHg7XG4gIHdpZHRoOiA1MCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4uc2Nyb2xsLWNvbnRlbnQge1xuICBtYXJnaW4tdG9wOiAwcHggIWltcG9ydGFudDtcbn1cblxuLm1peWFiaS1jb250YWluZXIubWFydGFob24tYmcge1xuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAwcHggMjFweCAjMzMzMzMzMzA7XG4gIGJveC1zaGFkb3c6IDBweCAwcHggMjFweCAjMzMzMzMzMzA7XG4gIG1pbi1oZWlnaHQ6IDEwMHZoO1xuICBtaW4td2lkdGg6IDEwMHZ3ICFpbXBvcnRhbnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/alert.service */ "./src/app/services/alert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _language_service_language_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../language-service/language-service */ "./src/app/language-service/language-service.ts");
/* harmony import */ var _marathan_marathanService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../marathan/marathanService */ "./marathan/marathanService.ts");

var LoginPage_1;








let LoginPage = LoginPage_1 = class LoginPage {
    constructor(authService, modalController, navCtrl, alertService, router, MarathonService, navParams, languageService, menuCtrl) {
        this.authService = authService;
        this.modalController = modalController;
        this.navCtrl = navCtrl;
        this.alertService = alertService;
        this.router = router;
        this.MarathonService = MarathonService;
        this.navParams = navParams;
        this.languageService = languageService;
        this.menuCtrl = menuCtrl;
        this.activePage = 'login';
        this.setLanguage();
    }
    setLanguage() {
        var languageCode = localStorage.getItem("languageType");
        this.languageType = this.languageService.languageCode(languageCode);
        console.log(this.languageType, 'test');
    }
    ngOnInit() {
    }
    // lang start.................
    ionViewDidLoad() {
        var languageCode = localStorage.getItem("languageType");
        this.languageType = this.languageService.languageCode(languageCode);
        console.log(this.languageType, 'test');
        this.whichPage = this.navParams.get('page');
        if (this.whichPage == 'login') {
            this.loginpage = true;
        }
        else {
            this.registerpage = true;
        }
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(false);
    }
    presentLoading() {
        var loaderMessage;
        if (localStorage.getItem("languageType") == 'Eng') {
            loaderMessage = 'Please wait a bit...';
        }
        else if (localStorage.getItem("languageType") == 'Rus') {
            loaderMessage = 'чуточку подождите...';
        }
        else {
            loaderMessage = 'per favore aspetta un po ...';
        }
        let loader = this.loader.create({
            content: loaderMessage
        });
        loader.present();
    }
    ;
    // lang finish...................
    dismissRegister() {
        this.modalController.dismiss();
    }
    // On Login button tap, dismiss Register modal and open login Modal
    logindis() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.dismissRegister();
            const loginModal = yield this.modalController.create({
                component: LoginPage_1,
            });
            return yield loginModal.present();
        });
    }
    // Dismiss Login Modal
    dismissLogin() {
        this.modalController.dismiss();
    }
    // On Register button tap, dismiss login modal and open register modal
    registerdis() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.dismissLogin();
            const registerModal = yield this.modalController.create({
                component: LoginPage_1
            });
            return yield registerModal.present();
        });
    }
    activepage(page) {
        this.activePage = page;
    }
    login(form) {
        this.authService.login(form.value.email, form.value.password, 'password').subscribe(data => {
            let token_Detail = data.json();
            console.log('token_Detail', token_Detail);
            localStorage.setItem("access_token", token_Detail.access_token);
            localStorage.setItem("email", token_Detail.email);
            localStorage.setItem("password", token_Detail.password);
            localStorage.setItem("role", token_Detail.role);
            localStorage.setItem("user_name", token_Detail.username);
            localStorage.setItem("user_email", token_Detail.email);
            localStorage.setItem("expires_in", token_Detail.expires_in);
            localStorage.setItem("refresh_token", token_Detail.refresh_token);
            this.router.navigate(['orderlist']);
        }, error => {
            console.log('error', error);
        });
    }
    register(form) {
        let data = { 'email': form.value.email, 'fname': form.value.fname, 'lname': form.value.lname, 'instagramId': form.value.instagramId };
        this.authService.UserRegister(data).subscribe(data => {
            console.log(data, 'data');
        }, error => {
            let err = error.json();
            alert(err._body);
            console.log(error);
        }, () => {
        });
    }
};
LoginPage.ctorParameters = () => [
    { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_1__["AuthenticationService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _marathan_marathanService__WEBPACK_IMPORTED_MODULE_7__["MarathanProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"] },
    { type: _language_service_language_service__WEBPACK_IMPORTED_MODULE_6__["LanguageServiceProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] }
];
LoginPage = LoginPage_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_authentication_service__WEBPACK_IMPORTED_MODULE_1__["AuthenticationService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
        _marathan_marathanService__WEBPACK_IMPORTED_MODULE_7__["MarathanProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"],
        _language_service_language_service__WEBPACK_IMPORTED_MODULE_6__["LanguageServiceProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]])
], LoginPage);



/***/ }),

/***/ "./src/app/services/alert.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/alert.service.ts ***!
  \*******************************************/
/*! exports provided: AlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return AlertService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");



let AlertService = class AlertService {
    constructor(toastController) {
        this.toastController = toastController;
    }
    presentToast(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: message,
                duration: 2000,
                position: 'top',
                color: 'dark'
            });
            toast.present();
        });
    }
};
AlertService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
AlertService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
], AlertService);



/***/ })

}]);
//# sourceMappingURL=login-login-module-es2015.js.map