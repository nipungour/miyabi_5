function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["orderlist-orderlist-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/orderlist/orderlist.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/orderlist/orderlist.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppOrderlistOrderlistPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content>\n  <div class=\" bg-gradient-blue\" id=\"notifications-list\" style=\"cursor:pointer\">\n    <div class=\"miyabi-wrapper \" style=\"margin: 0 auto;\">\n      <nav class=\"nav \">\n        <div class=\"logo\">\n          <a class=\"brand-name\"> Go to the marathon </a>\n        </div>\n        <div class=\"menu-icon \">\n          <div class=\"dropdown hambergur \">\n            <button class=\"menu\" type=\"button\"> <img src=\"../../../assets/icon/hamburger.png\"></button>          \n          </div>\n        </div>\n      </nav>\n    </div>\n  </div>\n \n<!-- Courses============================================================================================ -->\n<div class=\"availble-block\">\n  <div class=\"head\">My Courses</div>\n     <ul class=\"offer-avail\" *ngFor=\"let item of allMarathon.currentCourses let i= index\">\n    <div>\n      <li class=\"offer-block\">\n        <div class=\"offer-about-block\">\n          <div class=\"img-sec\">\n            <img class=\"offer-img\" [src]=\"item.imagePath\" />\n        </div>\n\n        <div class=\"detail-sec\">\n             <img *ngIf=\"item.isGreatExtensionAvailable || item.isExtension\" class=\"water-mark-order\" src=\"../../../assets/icon/Group 3892.svg\" />\n            <div>\n              <p><strong>{{item.title}}</strong></p>\n              <p>{{item.subTitle}}</p>\n              <p>{{languageType.landingPage.startNameTitle}}\n                 <strong style=\"font-family: MyriadProBoldCond !important\">{{dateFormatConvert(item.startDate)}}</strong>\n              </p>\n              <p>{{languageType.landingPage.marathonLength}}\n                {{item.days}}\n                <!-- {{daySpelling(item.days)}} -->\n              </p>\n            </div>\n\n            <p class=\"Cost\">\n              {{languageType.landingPage.costForParticipation}}\n              {{item.cost}}\n              {{languageType.landingPage.costCode}}\n            </p>\n        </div>\n        </div>\n        <!-- <button class=\"btn btn-order mt-auto\" (click)=\"goToMarathon(item.id , item.isExtension)\"\n          *ngIf=\"!item.isGreatExtensionAvailable && !item.isExtension\">\n           {{languageType.myOrdersListPage.gotoMarathon}}\n        </button>\n       <div *ngIf=\"item.isGreatExtensionAvailable || item.isExtension\" class=\"order-footer\">\n          <button class=\"btn btn-order \" (click)=\"goToMarathon(item.id,item.isExtension)\">\n          {{languageType.landingPage.goOverButton}}\n          </button> -->\n       <div class=\"expiry-about\"> {{languageType.landingPage.expireAbout}}\n            <span *ngIf=\"item?.productType =='GreatExtension'\">(GE)</span>\n            <span *ngIf=\"item?.productType !='GreatExtension'\">(SE)</span>\n       </div>\n       <a (click)=\"readMore(item.description)\" data-toggle=\"modal\" data-target=\"#myModal\" class=\"read-more\">\n        {{languageType.myOrdersListPage.learnMore}}\n      </a>\n    </li>\n  </div>\n\n    <li class=\"offer-block\" *ngIf=\"item.isPurchased && !item.isOutDated && item.isGift && !item.isCouponUsed\">\n      <div class=\"offer-about-block\">\n        <div class=\"img-sec\">\n          <img class=\"offer-img\" [src]=\"item.imagePath\" />\n          <button class=\"btn btn-order mt-auto\" (click)=\"copyCoupon(item.coupon)\">\n            {{item.coupon}} <ion-icon name=\"copy\" style=\"font-size: 23px; margin-left: 6px ;\"></ion-icon>\n          </button>\n        </div>\n        <div class=\"detail-sec\">\n          <div>\n            <p>\n              <strong>{{item.title}} </strong>\n            </p>\n            <p>{{item.subTitle}}</p>\n            <p>\n              {{languageType.landingPage.startNameTitle}}\n              <strong style=\"font-family: MyriadProBoldCond !important\">{{dateFormatConvert(item.startDate)}}</strong>\n            </p>\n            <p>\n              {{languageType.landingPage.marathonLength}}\n              {{item.days}}\n              <!-- {{daySpelling(item.days)}} -->\n            </p>\n           </div>\n          <p class=\"Cost\" >\n            {{languageType.landingPage.costForParticipation}}\n            {{item.cost}}\n            {{languageType.landingPage.costCode}}\n          </p>\n        </div>\n      </div>\n      <a (click)=\"readMore(item.description)\" data-toggle=\"modal\" data-target=\"#myModal\" class=\"read-more\">\n        {{languageType.myOrdersListPage.learnMore}}\n      </a>\n    </li>\n  </ul>\n</div>\n <!-- Available============================================================================================ -->\n  <div class=\"availble-block\">\n    <div class=\"head\">\n       <span>  AVAILABLE COURSES  </span>\n   </div>\n    <!---->\n    <ion-grid>\n      <ion-row class=\"nav nav-tabs\">\n      <ion-col size=\"6\"  >\n        <div class=\"img-sec\">\n          <img class=\"offer-img\" src=\"https://marathon.miyabi.academy\\MiyabiContent\\Images\\ec03348e-5ac5-4133-9bef-2db3e6c52e16.png\">\n        </div>\n        <div class=\"footer-block \">\n          <button class=\"btn btn-order mt-auto btn-purple\" (click)=\"pay()\">to pay <i class=\"fa fa-cart-plus\"></i>\n          </button>\n        </div>\n     </ion-col>\n     <ion-col size=\"6\"  >\n      <div >\n        <img class=\"water-mark-order\" src=\"../../../assets/icon/Group 3892.svg\">\n        <div>\n          <p><strong> МОЛОДОЕ ТЕЛО </strong></p>\n          <p> Осанка. Статика шеи. Уход за кожей. </p>\n          <p> Start  Mar 2, 2020 </p><p> Duration 21 days </p>\n        </div>\n        <p class=\"Cost\"> Cost of participation 3000rubles </p>\n      </div>\n      <div> <a class=\"read-more\" data-target=\"#myModal\" data-toggle=\"modal\"> more details... </a></div>\n     \n    </ion-col>\n  </ion-row>\n  </ion-grid>\n </div>\n\n     <!-- Archive ==================================================================================================== -->\n     <div class=\"availble-block\">\n      <div class=\"head\">\n        <span>  MY ARCHIVE  </span>\n      </div>\n\n     <div class=\"availble-block\">\n\n      <ion-grid>\n        <ion-row class=\"nav nav-tabs\">\n        <ion-col size=\"6\"  >\n          <div class=\"img-sec\">\n            <img class=\"offer-img\" src=\"https://marathon.miyabi.academy\\MiyabiContent\\Images\\ec03348e-5ac5-4133-9bef-2db3e6c52e16.png\">\n          </div>\n       </ion-col>\n       <ion-col size=\"6\"  >\n        <div >\n          <img class=\"water-mark-order\" src=\"../../../assets/icon/Group 3892.svg\">\n          <div>\n            <p><strong> МОЛОДОЕ ТЕЛО </strong></p>\n            <p> Осанка. Статика шеи. Уход за кожей. </p>\n            <p> Start  Mar 2, 2020 </p><p> Duration 21 days </p>\n          </div>\n          <p class=\"Cost\"> Cost of participation 3000rubles </p>\n        </div>  \n      </ion-col>\n    </ion-row>\n    <a class=\"btn btn-login mt-auto\" data-target=\"#paymentmodel\" data-toggle=\"modal\" style=\"text-transform: initial !important\"> Access to materials expired. Click here to extend </a>\n    </ion-grid>\n   <!-- bottom -->\n     <div class=\"bg-gradient-blue\" style=\"cursor:pointer; margin-top: auto;\" *ngIf=\" allMarathon != null\">\n      <div class=\"miyabi-container \">\n        <nav class=\"nav footer\">         \n          <div class=\"hambergur\">\n            <div>\n              <p [ngClass]=\"(langCode == 'Rus')?'fabBtnActive':'language'\" (click)=\"russian()\" style=\"float: right; \">Ru</p>\n              <p [ngClass]=\"(langCode == 'Eng')?'fabBtnActive':'language'\" (click)=\"english()\" style=\"float: right;margin: 0 4px;\">En</p>\n              <p style=\"float: right;\" (click)=\"italian()\" [ngClass]=\"(langCode == 'Ita')?'fabBtnActive':'language'\">It</p>\n            </div>\n          </div>\n        </nav>\n      </div>\n    </div>\n  </div> \n\n";
    /***/
  },

  /***/
  "./src/app/orderlist/orderlist-routing.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/orderlist/orderlist-routing.module.ts ***!
    \*******************************************************/

  /*! exports provided: OrderlistPageRoutingModule */

  /***/
  function srcAppOrderlistOrderlistRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "OrderlistPageRoutingModule", function () {
      return OrderlistPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _orderlist_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./orderlist.page */
    "./src/app/orderlist/orderlist.page.ts");

    var routes = [{
      path: '',
      component: _orderlist_page__WEBPACK_IMPORTED_MODULE_3__["OrderlistPage"]
    }];

    var OrderlistPageRoutingModule = function OrderlistPageRoutingModule() {
      _classCallCheck(this, OrderlistPageRoutingModule);
    };

    OrderlistPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], OrderlistPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/orderlist/orderlist.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/orderlist/orderlist.module.ts ***!
    \***********************************************/

  /*! exports provided: OrderlistPageModule */

  /***/
  function srcAppOrderlistOrderlistModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "OrderlistPageModule", function () {
      return OrderlistPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _orderlist_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./orderlist-routing.module */
    "./src/app/orderlist/orderlist-routing.module.ts");
    /* harmony import */


    var _orderlist_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./orderlist.page */
    "./src/app/orderlist/orderlist.page.ts");

    var OrderlistPageModule = function OrderlistPageModule() {
      _classCallCheck(this, OrderlistPageModule);
    };

    OrderlistPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _orderlist_routing_module__WEBPACK_IMPORTED_MODULE_5__["OrderlistPageRoutingModule"]],
      declarations: [_orderlist_page__WEBPACK_IMPORTED_MODULE_6__["OrderlistPage"]]
    })], OrderlistPageModule);
    /***/
  },

  /***/
  "./src/app/orderlist/orderlist.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/orderlist/orderlist.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppOrderlistOrderlistPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".detailinfo {\n  display: block;\n  font-size: 38px;\n  font-family: \"Myriad Pro-Condensed\";\n  line-height: 38px;\n}\n\n.btn-fill-theme {\n  color: #ffffff;\n  font-size: 16px;\n  font-family: \"PT Sans\", Arial, sans-serif;\n  line-height: 1.55;\n  font-weight: 600;\n  border-width: 1px;\n  border-radius: 50px;\n  background-color: #6f4075;\n  background-position: center center;\n  border-color: transparent;\n  border-style: solid;\n  transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out;\n}\n\n.btn {\n  font-size: 50px;\n  line-height: 1;\n  background-color: #b077ac;\n  font-family: MyriadProCond;\n  width: 250px;\n  box-shadow: 3px 3px 6px #3333;\n  border-radius: 15px;\n  padding: 0 25px;\n  min-height: 70px;\n  margin: 10px auto;\n  font-weight: 500;\n  color: #fff;\n}\n\nbutton.btn-order {\n  border: 0;\n}\n\n.order-footer .btn-order {\n  background: linear-gradient(#70d1cc, #007e9e);\n}\n\n.btn-order.btn-gradeient {\n  background: linear-gradient(#70d1cc, #007e9e) !important;\n}\n\n.btn:hover,\n.btn-outline-theme:hover {\n  background-color: #cffcfb;\n  border-color: #ffffff;\n  color: #fff;\n}\n\n.btn-outline-theme {\n  color: #ffffff;\n  font-size: 18px;\n  font-family: \"PT Sans\", Arial, sans-serif;\n  line-height: 1.55;\n  font-weight: 600;\n  border-width: 2px;\n  border-radius: 50px;\n  background-position: center center;\n  border-color: #ffffff;\n  border-style: solid;\n  transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out;\n}\n\n.container {\n  z-index: 1;\n  position: relative;\n}\n\n.main-csection {\n  min-height: 650px;\n  height: 100vh;\n  max-width: 100%;\n  background-image: linear-gradient(to bottom, rgba(113, 46, 133, 0.3), rgba(5, 24, 69, 0.6));\n}\n\n.add-section {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  text-align: center;\n  margin-top: 50px;\n}\n\n.water-mark-order {\n  left: 0px;\n  opacity: 0px;\n  height: 100%;\n}\n\n.hambergur {\n  display: flex;\n  align-items: center;\n}\n\n.hambergur button {\n  background: transparent;\n  border: 0;\n  box-shadow: 0;\n  padding: 0;\n}\n\n.hambergur img {\n  width: 32px;\n  max-width: 8vw;\n}\n\n.hambergur .dropdown-menu {\n  padding: 0;\n  background: #262a49;\n  color: #fff;\n  text-transform: capitalize;\n  font-size: 32px;\n  line-height: 36px;\n  border-radius: 0;\n  width: 380px;\n  max-width: 70vw;\n  left: auto !important;\n  right: 0;\n  top: 100% !important;\n  transform: translate3d(0, 0, 0px) !important;\n}\n\n.hambergur .dropdown-menu li {\n  border-bottom: 1px solid #fff;\n  padding: 20px 30px;\n  color: #fff;\n}\n\n.hambergur .dropdown-menu li:first-child {\n  padding-top: 40px;\n}\n\nul.active.dropdown-menu {\n  display: block;\n  z-index: 10000;\n}\n\n.banner {\n  background-image: url(https://static.tildacdn.com/tild3231-6133-4165-b264-393537656563/fon1.jpg);\n  background-size: cover;\n  background-position: top center;\n  padding: 30px 20px;\n  margin: 20px 15px;\n}\n\n.banner .banner-about {\n  color: #fff;\n}\n\n.banner .banner-about .banner-title {\n  font-size: 32px;\n  line-height: 1.25;\n  margin-bottom: 10px;\n  color: #fff;\n}\n\n.banner .banner-about .date-details {\n  font-size: 20px;\n  line-height: 1.4;\n  margin-bottom: 10px;\n  color: #fff;\n}\n\n.banner .banner-about .date-details span {\n  color: white;\n  font-size: 22px;\n  font-weight: 700;\n}\n\n.banner .banner-about .banner-details {\n  font-size: 18px;\n  line-height: 1.5;\n  color: #fff;\n}\n\n.banner .start-btn {\n  margin-top: 15px;\n}\n\n.banner .start-btn .btn {\n  border-radius: 50px;\n  width: auto;\n  font-family: \"MyriadProBoldCond\";\n  font-size: 60px;\n  min-height: 50px;\n  padding: 9px 50px;\n  line-height: 1.2;\n}\n\n.banner .start-btn .btn .fa {\n  font-size: 50px;\n}\n\n.btn-purple {\n  background: linear-gradient(#C782C3, #795576) !important;\n  color: #fff !important;\n}\n\n.btn-color-purple {\n  background: transparent !important;\n  color: #b076ac !important;\n  box-shadow: none !important;\n  padding: 7px 27px;\n  font-size: 22px;\n  line-height: 20px;\n  min-height: 40px;\n  width: auto;\n  min-height: auto;\n  display: flex;\n  align-items: center;\n  white-space: nowrap;\n}\n\n.water-mark-order {\n  position: absolute;\n  z-index: 1000;\n  left: -10px;\n  height: 100%;\n}\n\n.login-btn {\n  width: 90%;\n  padding: 5px 10px;\n  border-radius: 25px;\n  background-color: #049fad;\n  font-size: 27px;\n  text-align: center;\n  max-width: 80vw;\n  margin: 0 auto;\n  display: block;\n  margin-bottom: 12px;\n}\n\n.btn-order {\n  padding: 7px 27px;\n  font-size: 6.667vw;\n  line-height: 6.667vw;\n  min-height: 40px;\n  color: #fff;\n  background: linear-gradient(#70d1cc, #007e9e);\n  border-radius: 50px;\n  width: 36vw;\n  min-height: auto;\n  display: flex;\n  align-items: center;\n  white-space: nowrap;\n  justify-content: center;\n}\n\n.btn-order i {\n  margin-left: 15px;\n}\n\n.btn-order.btn-full {\n  width: 100%;\n  font-size: 24px;\n  line-height: 20px;\n  min-height: 52px;\n  padding: 1px 20px;\n  white-space: initial;\n  position: relative;\n  z-index: 100;\n  height: 50px;\n}\n\n.head {\n  background: linear-gradient(#fff, #4cb4c9 270%);\n  box-shadow: 0px 1px 5px #b5c0de;\n  margin-bottom: 10px;\n  font-size: 36px;\n  line-height: 44px;\n  font-family: \"MyriadProCond\";\n  text-align: center;\n  color: #049fad;\n  padding: 16px 25px 17px;\n}\n\n.offer-block {\n  display: block;\n  background-color: #fff;\n  text-align: center;\n  padding: 10px 5px 5px;\n  position: relative;\n  margin-bottom: 4px;\n  box-shadow: 0 1px 5px #b5c0de;\n}\n\n.offer-block .offer-about-block {\n  display: flex;\n  justify-content: center;\n}\n\n.offer-block .read-more {\n  position: absolute;\n  z-index: 99;\n  font-size: 18px;\n  line-height: 20px;\n  color: #007e9e;\n  right: 10px;\n  bottom: 20px;\n}\n\n.offer-block .img-sec {\n  width: 50%;\n  max-width: initial;\n  display: flex;\n  align-items: flex-start;\n  flex-direction: column;\n}\n\n.offer-block .img-sec img {\n  margin: 0 auto;\n  border-radius: 50%;\n  margin-bottom: 10px;\n  width: 140px;\n  min-height: 140px;\n  max-width: -webkit-max-content;\n  max-width: -moz-max-content;\n  max-width: max-content;\n}\n\n.offer-block .detail-sec {\n  width: 50%;\n  padding: 0 0px 8px 10px;\n  position: relative;\n  text-align: left;\n  height: auto;\n}\n\n.offer-block .detail-sec .read-more {\n  position: absolute;\n  font-size: 18px;\n  line-height: 22px;\n  color: #007e9e;\n  right: 10px;\n  top: 180px;\n  font-family: \"MyriadProBoldCond\";\n}\n\n.btn-join {\n  padding: 7px 12px;\n  font-size: 18px;\n  line-height: 25px;\n  color: #fff;\n  background: #049fad;\n  border-radius: 50px;\n  margin: 0 auto;\n}\n\n.offer-block .order-footer {\n  margin: 0px -5px -10px -5px;\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  padding: 2px 5px;\n  position: relative;\n  z-index: 1000;\n  min-height: initial;\n  background-color: #2f9fac;\n  background-image: linear-gradient(#8dccd4, #2f9fac);\n}\n\n.offer-block .order-footer .btn.btn-order {\n  border: 1px solid #fff;\n  margin: 0 auto;\n  box-shadow: none;\n  width: 100%;\n  white-space: nowrap;\n  width: auto;\n  height: auto;\n}\n\n.offer-block .order-footer > * {\n  width: 40%;\n  text-align: center;\n}\n\n.offer-block .order-footer .expiry-about {\n  color: #fff;\n  font-size: 5.2vw;\n  letter-spacing: -0.5px;\n  font-family: \"MyriadProCond\";\n  line-height: 4.6vw;\n  width: 49% !important;\n}\n\n.offer-block .order-footer .expiry-about > span {\n  display: inline-block;\n}\n\n.btn-order {\n  margin-left: 0;\n  min-width: 146px;\n  width: 126px;\n  white-space: nowrap;\n  height: 45px;\n}\n\n.offer-block .offer-about-block {\n  justify-content: flex-start;\n}\n\n.language-con {\n  top: 0;\n  right: 0;\n  padding-top: 5px;\n  padding-bottom: 5px;\n  background: transparent;\n  box-shadow: none;\n  position: absolute;\n  left: 0;\n  width: 100%;\n  padding: 3px 5px;\n  z-index: 1000;\n}\n\n.fabBtnActive {\n  height: 30px;\n  width: 30px;\n  border-radius: 50px;\n  background-color: rgba(4, 159, 173, 0.73) !important;\n  border: 1px solid rgba(4, 159, 173, 0.73) !important;\n  color: #fff;\n  font-size: 20px;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  cursor: pointer;\n}\n\n.modal {\n  display: none;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 0;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(250, 248, 248, 0.4);\n  /* Black w/ opacity */\n}\n\n/* Modal Content */\n\n.modal-content {\n  position: relative;\n  background-color: #fefefe;\n  margin: auto;\n  padding: 0;\n  border: 0px solid #888;\n  width: 100%;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n  -webkit-animation-name: animatetop;\n  -webkit-animation-duration: 0.4s;\n  animation-name: animatetop;\n  animation-duration: 0.4s;\n  height: 100%;\n}\n\n/* Add Animation */\n\n@-webkit-keyframes animatetop {\n  from {\n    top: -300px;\n    opacity: 0;\n  }\n  to {\n    top: 0;\n    opacity: 1;\n  }\n}\n\n@keyframes animatetop {\n  from {\n    top: -300px;\n    opacity: 0;\n  }\n  to {\n    top: 0;\n    opacity: 1;\n  }\n}\n\n/* The Close Button */\n\n.close {\n  color: white;\n  float: right;\n  font-size: 28px;\n  font-weight: bold;\n}\n\n.modal-body {\n  max-height: 100%;\n  overflow-y: hidden;\n  background-color: #b7d8e2;\n  color: #fff;\n  padding: 0;\n  overflow: scroll;\n}\n\n.close:hover,\n.close:focus {\n  color: #000;\n  text-decoration: none;\n  cursor: pointer;\n}\n\n.modal-header {\n  padding: 2px 5px;\n  background-color: #77a2af;\n  color: white;\n}\n\n.modal-body {\n  padding: 2px 0px;\n}\n\n.modal-close {\n  cursor: pointer;\n  background-color: #77a2af;\n}\n\n.modal1 {\n  display: none;\n  position: fixed;\n  z-index: 1;\n  padding-top: 48px;\n  left: 0;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  overflow: auto;\n  background-color: black;\n  background-color: rgba(0, 0, 0, 0.4);\n}\n\n.modal {\n  display: none;\n  position: fixed;\n  z-index: 1;\n  padding-top: 4px;\n  left: 0;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  overflow: auto;\n  background-color: black;\n  background-color: rgba(0, 0, 0, 0.4);\n}\n\n.btn-modal {\n  box-shadow: 6px 6px 6px #00000029;\n}\n\n.offer-block .offer-block-btn-group .read-more {\n  bottom: 2px;\n}\n\n.offer-block-btn-group {\n  position: relative;\n  min-height: 30px;\n}\n\n.btn-order1 {\n  padding: 7px 27px;\n  font-size: 6.6667vw;\n  line-height: 6.66667vw;\n  min-height: 40px;\n  color: #fff;\n  background: #b076ac;\n  border-radius: 50px;\n  width: auto;\n  max-width: auto;\n  min-height: auto;\n  display: flex;\n  align-items: center;\n  white-space: nowrap;\n  justify-content: center;\n}\n\n.btn-order1.btn-full {\n  width: 100%;\n  font-size: 22px;\n  line-height: 20px;\n  min-height: 40px;\n  padding: 1px 20px;\n  white-space: initial;\n  position: relative;\n  z-index: 100;\n}\n\n.btn.btn-order1 {\n  border: 1px solid #fff;\n  box-shadow: none;\n  white-space: nowrap;\n  width: auto;\n}\n\n.btn.btn-order1 {\n  border: 1px solid #fff;\n  box-shadow: none;\n  white-space: nowrap;\n  min-width: 140px;\n}\n\n.order-footer {\n  align-items: center;\n  height: auto;\n}\n\n.miyabi-read-more .modal-body {\n  background-color: #a9cedb;\n  font-size: 24px;\n  line-height: 32px;\n  padding: 50px 10px 15px;\n}\n\n.miyabi-read-more .modal-body p span {\n  font-size: 24px !important;\n  line-height: 32px;\n}\n\n.btn.btn-login {\n  width: calc(100% - 20px);\n  margin: 0 10px 10px;\n  min-width: auto;\n  font-size: 24px;\n  line-height: 20px;\n  font-family: MyriadPro-BoldCond;\n  padding: 5px 20px;\n}\n\n.btn-login {\n  background-image: linear-gradient(#70d1cc, #007e9e);\n  padding: 10px 30px;\n  font-size: 24px;\n  box-shadow: none;\n  line-height: 24.88px;\n  border-radius: 60px;\n  border: 0;\n  color: #fff !important;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin: 0 auto;\n  min-width: 283px;\n  font-family: MyriadPro-Cond;\n}\n\n.bg-gradient-blue {\n  background-image: linear-gradient(45deg, #b4b0db, #b7d8e2);\n  color: #fff;\n}\n\n.menu {\n  border: 0;\n  border-radius: 0;\n  font-family: inherit;\n  font-style: inherit;\n  font-variant: inherit;\n  line-height: 1;\n  text-transform: none;\n  cursor: pointer;\n  -webkit-appearance: button;\n  margin-left: 242pt;\n  margin-bottom: 10px;\n}\n\n.logo .brand-name {\n  font-size: 31px;\n  font-family: \"MyriadProRegular\";\n  white-space: nowrap;\n  max-width: 74vw;\n  min-width: 220px;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3Bpem9uZS9teWRheXMvc3JjL2FwcC9vcmRlcmxpc3Qvb3JkZXJsaXN0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvb3JkZXJsaXN0L29yZGVybGlzdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUk7RUFDSSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG1DQUFBO0VBQ0EsaUJBQUE7QUNDUjs7QURDSTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0EseUNBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0NBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0dBQUE7QUNFTjs7QURDSTtFQUNFLGVBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSwwQkFBQTtFQUNBLFlBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUNFTjs7QURBSTtFQUVFLFNBQUE7QUNFTjs7QURBRTtFQUNFLDZDQUFBO0FDR0o7O0FEREU7RUFDRSx3REFBQTtBQ0lKOztBREFNOztFQUNFLHlCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0FDSVI7O0FEQUk7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLHlDQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQ0FBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxvR0FBQTtBQ0dOOztBRERJO0VBQ0UsVUFBQTtFQUNBLGtCQUFBO0FDSU47O0FEREk7RUFDRSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0VBU0EsMkZBQUE7QUNGTjs7QURLSTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDRk47O0FES0k7RUFDRSxTQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUNGTjs7QURJSTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtBQ0ROOztBRElJO0VBQ0UsdUJBQUE7RUFDQSxTQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7QUNETjs7QURJSTtFQUNFLFdBQUE7RUFDQSxjQUFBO0FDRE47O0FER0U7RUFDSSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EscUJBQUE7RUFDQSxRQUFBO0VBQ0Esb0JBQUE7RUFDQSw0Q0FBQTtBQ0FOOztBRENNO0VBQ0UsNkJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUNDUjs7QURBUTtFQUNFLGlCQUFBO0FDRVY7O0FERUk7RUFDRSxjQUFBO0VBQ0EsY0FBQTtBQ0NOOztBREVJO0VBQ0UsZ0dBQUE7RUFDQSxzQkFBQTtFQUNBLCtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0NOOztBRENNO0VBQ0UsV0FBQTtBQ0NSOztBREFRO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0FDRVY7O0FEQ1E7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7QUNDVjs7QURBVTtFQUNFLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNFWjs7QURFUTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUNBVjs7QURJTTtFQUNFLGdCQUFBO0FDRlI7O0FER1E7RUFDRSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxnQ0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUNEVjs7QURFVTtFQUNFLGVBQUE7QUNBWjs7QURPSTtFQUNFLHdEQUFBO0VBQ0Esc0JBQUE7QUNKTjs7QURNSTtFQUNFLGtDQUFBO0VBQ0EseUJBQUE7RUFDQSwyQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0hOOztBRE1JO0VBQ0Usa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNITjs7QURLSTtFQUNFLFVBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQ0ZOOztBRElJO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFFQSxXQUFBO0VBQ0EsNkNBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUNGTjs7QURLSTtFQUNFLGlCQUFBO0FDRk47O0FESUk7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQ0ROOztBREdJO0VBQ0UsK0NBQUE7RUFDQSwrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSx1QkFBQTtBQ0FOOztBREdJO0VBQ0UsY0FBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtBQ0FOOztBRENNO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0FDQ1I7O0FEQ007RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNDUjs7QURFTTtFQUNFLFVBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHNCQUFBO0FDQVI7O0FERVE7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLDhCQUFBO0VBQUEsMkJBQUE7RUFBQSxzQkFBQTtBQ0FWOztBRElNO0VBQ0UsVUFBQTtFQUNBLHVCQUFBO0VBRUEsa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUNIUjs7QURNUTtFQUNFLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsZ0NBQUE7QUNKVjs7QURTSTtFQUNFLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0FDTk47O0FEU0k7RUFDRSwyQkFBQTtFQUNBLGFBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsbURBQUE7QUNOTjs7QURRTTtFQUNFLHNCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNOUjs7QURRTTtFQUNFLFVBQUE7RUFDQSxrQkFBQTtBQ05SOztBRFFNO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7QUNOUjs7QURPUTtFQUNFLHFCQUFBO0FDTFY7O0FEVUk7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FDUE47O0FEU0k7RUFDRSwyQkFBQTtBQ05OOztBRFNJO0VBQ0UsTUFBQTtFQUNBLFFBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7RUFFQSxnQkFBQTtFQUNBLGFBQUE7QUNQTjs7QURVSTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxvREFBQTtFQUNBLG9EQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7QUNQTjs7QURTSTtFQUNFLGFBQUE7RUFBZSxzQkFBQTtFQUNmLGVBQUE7RUFBaUIsa0JBQUE7RUFDakIsVUFBQTtFQUFZLGVBQUE7RUFDWixrQkFBQTtFQUFvQix3QkFBQTtFQUNwQixPQUFBO0VBQ0EsTUFBQTtFQUNBLFdBQUE7RUFBYSxlQUFBO0VBQ2IsWUFBQTtFQUFjLGdCQUFBO0VBQ2QsY0FBQTtFQUFnQiw0QkFBQTtFQUNoQix1QkFBQTtFQUFnQyxtQkFBQTtFQUNoQywwQ0FBQTtFQUE0QyxxQkFBQTtBQ0dsRDs7QURBSSxrQkFBQTs7QUFDQTtFQUNFLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLDRFQUFBO0VBQ0Esa0NBQUE7RUFDQSxnQ0FBQTtFQUNBLDBCQUFBO0VBQ0Esd0JBQUE7RUFDQSxZQUFBO0FDR047O0FEQUksa0JBQUE7O0FBQ0E7RUFDRTtJQUNFLFdBQUE7SUFDQSxVQUFBO0VDR047RURESTtJQUNFLE1BQUE7SUFDQSxVQUFBO0VDR047QUFDRjs7QURBSTtFQUNFO0lBQ0UsV0FBQTtJQUNBLFVBQUE7RUNFTjtFREFJO0lBQ0UsTUFBQTtJQUNBLFVBQUE7RUNFTjtBQUNGOztBRENJLHFCQUFBOztBQUNBO0VBQ0UsWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNDTjs7QURDSTtFQUNFLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUNFTjs7QURBSTs7RUFFRSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0FDR047O0FEQUk7RUFDRSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtBQ0dOOztBREFJO0VBQ0UsZ0JBQUE7QUNHTjs7QURBSTtFQUNFLGVBQUE7RUFFQSx5QkFBQTtBQ0VOOztBREFJO0VBQ0UsYUFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxPQUFBO0VBQ0EsTUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLHVCQUFBO0VBQ0Esb0NBQUE7QUNHTjs7QURESTtFQUNFLGFBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSx1QkFBQTtFQUNBLG9DQUFBO0FDSU47O0FERkk7RUFFRSxpQ0FBQTtBQ0lOOztBRERJO0VBQ0UsV0FBQTtBQ0lOOztBREZJO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtBQ0tOOztBREhJO0VBQ0UsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFFQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FDS047O0FESEk7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDTU47O0FESkk7RUFDRSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0FDT047O0FETEk7RUFDRSxzQkFBQTtFQUVBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQ1FOOztBRE5JO0VBSUUsbUJBQUE7RUFDQSxZQUFBO0FDU047O0FETEk7RUFDRSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0FDUU47O0FESkk7RUFDRSwwQkFBQTtFQUVBLGlCQUFBO0FDTU47O0FESkk7RUFDRSx3QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLCtCQUFBO0VBQ0EsaUJBQUE7QUNPTjs7QURMRTtFQUNFLG1EQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EsU0FBQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSwyQkFBQTtBQ1FKOztBRE5BO0VBQ0UsMERBQUE7RUFDQSxXQUFBO0FDU0Y7O0FETkE7RUFDRSxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0EsMEJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDU0Y7O0FEUEE7RUFDRSxlQUFBO0VBQ0EsK0JBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7QUNVRiIsImZpbGUiOiJzcmMvYXBwL29yZGVybGlzdC9vcmRlcmxpc3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiICAgIC5kZXRhaWxpbmZve1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgZm9udC1zaXplOiAzOHB4O1xuICAgICAgICBmb250LWZhbWlseTogJ015cmlhZCBQcm8tQ29uZGVuc2VkJztcbiAgICAgICAgbGluZS1oZWlnaHQ6IDM4cHg7IFxuICAgIH1cbiAgICAuYnRuLWZpbGwtdGhlbWUge1xuICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICBmb250LWZhbWlseTogXCJQVCBTYW5zXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuICAgICAgbGluZS1oZWlnaHQ6IDEuNTU7XG4gICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgYm9yZGVyLXdpZHRoOiAxcHg7XG4gICAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzZmNDA3NTtcbiAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gICAgICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xuICAgICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgICAgIHRyYW5zaXRpb246IGJhY2tncm91bmQtY29sb3IgMC4ycyBlYXNlLWluLW91dCwgY29sb3IgMC4ycyBlYXNlLWluLW91dCwgYm9yZGVyLWNvbG9yIDAuMnMgZWFzZS1pbi1vdXQ7XG4gICAgfVxuICBcbiAgICAuYnRuIHtcbiAgICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiAxO1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2IwNzdhYztcbiAgICAgIGZvbnQtZmFtaWx5OiBNeXJpYWRQcm9Db25kO1xuICAgICAgd2lkdGg6IDI1MHB4O1xuICAgICAgYm94LXNoYWRvdzogM3B4IDNweCA2cHggIzMzMzM7XG4gICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgICAgcGFkZGluZzogMCAyNXB4O1xuICAgICAgbWluLWhlaWdodDogNzBweDtcbiAgICAgIG1hcmdpbjogMTBweCBhdXRvO1xuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgIGNvbG9yOiAjZmZmO1xuICAgIH1cbiAgICBidXR0b24uYnRuLW9yZGVyIHtcbiAgICAgIC8vIGJhY2tncm91bmQ6ICMwNDlmYWRiYTtcbiAgICAgIGJvcmRlcjowO1xuICB9XG4gIC5vcmRlci1mb290ZXIgLmJ0bi1vcmRlcntcbiAgICBiYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCgjNzBkMWNjLCAjMDA3ZTllKTtcbiAgfVxuICAuYnRuLW9yZGVyLmJ0bi1ncmFkZWllbnQge1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgjNzBkMWNjLCAjMDA3ZTllKSAhaW1wb3J0YW50O1xuICB9XG4gICAgLmJ0bixcbiAgICAuYnRuLW91dGxpbmUtdGhlbWUge1xuICAgICAgJjpob3ZlciB7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNjZmZjZmI7XG4gICAgICAgIGJvcmRlci1jb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICB9XG4gICAgfVxuICBcbiAgICAuYnRuLW91dGxpbmUtdGhlbWUge1xuICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICBmb250LWZhbWlseTogXCJQVCBTYW5zXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuICAgICAgbGluZS1oZWlnaHQ6IDEuNTU7XG4gICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgYm9yZGVyLXdpZHRoOiAycHg7XG4gICAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcbiAgICAgIGJvcmRlci1jb2xvcjogI2ZmZmZmZjtcbiAgICAgIGJvcmRlci1zdHlsZTogc29saWQ7XG4gICAgICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kLWNvbG9yIDAuMnMgZWFzZS1pbi1vdXQsIGNvbG9yIDAuMnMgZWFzZS1pbi1vdXQsIGJvcmRlci1jb2xvciAwLjJzIGVhc2UtaW4tb3V0O1xuICAgIH1cbiAgICAuY29udGFpbmVyIHtcbiAgICAgIHotaW5kZXg6IDE7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgfVxuICBcbiAgICAubWFpbi1jc2VjdGlvbiB7XG4gICAgICBtaW4taGVpZ2h0OiA2NTBweDtcbiAgICAgIGhlaWdodDogMTAwdmg7XG4gICAgICBtYXgtd2lkdGg6IDEwMCU7XG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWdyYWRpZW50KFxuICAgICAgICBsaW5lYXIsXG4gICAgICAgIGxlZnQgdG9wLFxuICAgICAgICBsZWZ0IGJvdHRvbSxcbiAgICAgICAgZnJvbShyZ2JhKDExMywgNDYsIDEzMywgMC4zKSksXG4gICAgICAgIHRvKHJnYmEoNSwgMjQsIDY5LCAwLjYpKVxuICAgICAgKTtcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvcCwgcmdiYSgxMTMsIDQ2LCAxMzMsIDAuMyksIHJnYmEoNSwgMjQsIDY5LCAwLjYpKTtcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byBib3R0b20sIHJnYmEoMTEzLCA0NiwgMTMzLCAwLjMpLCByZ2JhKDUsIDI0LCA2OSwgMC42KSk7XG4gICAgfVxuICBcbiAgICAuYWRkLXNlY3Rpb24ge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBtYXJnaW4tdG9wOiA1MHB4O1xuICAgIH1cbiAgXG4gICAgLndhdGVyLW1hcmstb3JkZXIge1xuICAgICAgbGVmdDogMHB4O1xuICAgICAgb3BhY2l0eTogMHB4O1xuICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuaGFtYmVyZ3VyIHtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIH1cbiAgXG4gICAgLmhhbWJlcmd1ciBidXR0b24ge1xuICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgICBib3JkZXI6IDA7XG4gICAgICBib3gtc2hhZG93OiAwO1xuICAgICAgcGFkZGluZzogMDtcbiAgICB9XG4gIFxuICAgIC5oYW1iZXJndXIgaW1nIHtcbiAgICAgIHdpZHRoOiAzMnB4O1xuICAgICAgbWF4LXdpZHRoOiA4dnc7XG4gICAgfVxuICAuaGFtYmVyZ3VyIC5kcm9wZG93bi1tZW51IHtcbiAgICAgIHBhZGRpbmc6IDA7XG4gICAgICBiYWNrZ3JvdW5kOiAjMjYyYTQ5O1xuICAgICAgY29sb3I6ICNmZmY7XG4gICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICAgIGZvbnQtc2l6ZTogMzJweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiAzNnB4O1xuICAgICAgYm9yZGVyLXJhZGl1czogMDtcbiAgICAgIHdpZHRoOiAzODBweDtcbiAgICAgIG1heC13aWR0aDogNzB2dztcbiAgICAgIGxlZnQ6IGF1dG8gIWltcG9ydGFudDtcbiAgICAgIHJpZ2h0OiAwO1xuICAgICAgdG9wOiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIDAsIDBweCkgIWltcG9ydGFudDtcbiAgICAgIGxpIHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNmZmY7XG4gICAgICAgIHBhZGRpbmc6IDIwcHggMzBweDtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgICY6Zmlyc3QtY2hpbGQge1xuICAgICAgICAgIHBhZGRpbmctdG9wOiA0MHB4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHVsLmFjdGl2ZS5kcm9wZG93bi1tZW51IHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgei1pbmRleDogMTAwMDA7XG4gICAgfVxuICBcbiAgICAuYmFubmVyIHtcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3N0YXRpYy50aWxkYWNkbi5jb20vdGlsZDMyMzEtNjEzMy00MTY1LWIyNjQtMzkzNTM3NjU2NTYzL2ZvbjEuanBnKTtcbiAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiB0b3AgY2VudGVyO1xuICAgICAgcGFkZGluZzogMzBweCAyMHB4O1xuICAgICAgbWFyZ2luOiAyMHB4IDE1cHg7XG4gIFxuICAgICAgLmJhbm5lci1hYm91dCB7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICAuYmFubmVyLXRpdGxlIHtcbiAgICAgICAgICBmb250LXNpemU6IDMycHg7XG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDEuMjU7XG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgfVxuICBcbiAgICAgICAgLmRhdGUtZGV0YWlscyB7XG4gICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxLjQ7XG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgICBzcGFuIHtcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gIFxuICAgICAgICAuYmFubmVyLWRldGFpbHMge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgICBsaW5lLWhlaWdodDogMS41O1xuICAgICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICB9XG4gICAgICB9XG4gIFxuICAgICAgLnN0YXJ0LWJ0biB7XG4gICAgICAgIG1hcmdpbi10b3A6IDE1cHg7XG4gICAgICAgIC5idG4ge1xuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gICAgICAgICAgd2lkdGg6IGF1dG87XG4gICAgICAgICAgZm9udC1mYW1pbHk6IFwiTXlyaWFkUHJvQm9sZENvbmRcIjtcbiAgICAgICAgICBmb250LXNpemU6IDYwcHg7XG4gICAgICAgICAgbWluLWhlaWdodDogNTBweDtcbiAgICAgICAgICBwYWRkaW5nOiA5cHggNTBweDtcbiAgICAgICAgICBsaW5lLWhlaWdodDogMS4yO1xuICAgICAgICAgIC5mYSB7XG4gICAgICAgICAgICBmb250LXNpemU6IDUwcHg7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICBcbiAgXG4gICAgLmJ0bi1wdXJwbGUge1xuICAgICAgYmFja2dyb3VuZDpsaW5lYXItZ3JhZGllbnQoICNDNzgyQzMsIzc5NTU3NikgIWltcG9ydGFudDtcbiAgICAgIGNvbG9yOiAjZmZmICFpbXBvcnRhbnQ7XG4gICAgfVxuICAgIC5idG4tY29sb3ItcHVycGxlIHtcbiAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG4gICAgICBjb2xvcjogI2IwNzZhYyAhaW1wb3J0YW50O1xuICAgICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xuICAgICAgcGFkZGluZzogN3B4IDI3cHg7XG4gICAgICBmb250LXNpemU6IDIycHg7XG4gICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICAgIG1pbi1oZWlnaHQ6IDQwcHg7XG4gICAgICB3aWR0aDogYXV0bztcbiAgICAgIG1pbi1oZWlnaHQ6IGF1dG87XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgfVxuICBcbiAgICAud2F0ZXItbWFyay1vcmRlciB7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICB6LWluZGV4OiAxMDAwO1xuICAgICAgbGVmdDogLTEwcHg7XG4gICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuICAgIC5sb2dpbi1idG4ge1xuICAgICAgd2lkdGg6IDkwJTtcbiAgICAgIHBhZGRpbmc6IDVweCAxMHB4O1xuICAgICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMwNDlmYWQ7XG4gICAgICBmb250LXNpemU6IDI3cHg7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBtYXgtd2lkdGg6IDgwdnc7XG4gICAgICBtYXJnaW46IDAgYXV0bztcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgbWFyZ2luLWJvdHRvbTogMTJweDtcbiAgICB9XG4gICAgLmJ0bi1vcmRlciB7XG4gICAgICBwYWRkaW5nOiA3cHggMjdweDtcbiAgICAgIGZvbnQtc2l6ZTogNi42Njd2dztcbiAgICAgIGxpbmUtaGVpZ2h0OiA2LjY2N3Z3O1xuICAgICAgbWluLWhlaWdodDogNDBweDtcbiAgICAgXG4gICAgICBjb2xvcjogI2ZmZjtcbiAgICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgjNzBkMWNjLCAjMDA3ZTllKTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gICAgICB3aWR0aDogMzZ2dztcbiAgICAgIG1pbi1oZWlnaHQ6IGF1dG87XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICB9XG4gIFxuICAgIC5idG4tb3JkZXIgaSB7XG4gICAgICBtYXJnaW4tbGVmdDogMTVweDtcbiAgICB9XG4gICAgLmJ0bi1vcmRlci5idG4tZnVsbCB7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgICAgbWluLWhlaWdodDogNTJweDtcbiAgICAgIHBhZGRpbmc6IDFweCAyMHB4O1xuICAgICAgd2hpdGUtc3BhY2U6IGluaXRpYWw7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICB6LWluZGV4OiAxMDA7XG4gICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgfVxuICAgIC5oZWFkIHtcbiAgICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgjZmZmLCAjNGNiNGM5IDI3MCUpO1xuICAgICAgYm94LXNoYWRvdzogMHB4IDFweCA1cHggI2I1YzBkZTtcbiAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICBmb250LXNpemU6IDM2cHg7XG4gICAgICBsaW5lLWhlaWdodDogNDRweDtcbiAgICAgIGZvbnQtZmFtaWx5OiBcIk15cmlhZFByb0NvbmRcIjtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIGNvbG9yOiAjMDQ5ZmFkO1xuICAgICAgcGFkZGluZzogMTZweCAyNXB4IDE3cHg7XG4gICAgfVxuICBcbiAgICAub2ZmZXItYmxvY2sge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgcGFkZGluZzogMTBweCA1cHggNXB4O1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgbWFyZ2luLWJvdHRvbTogNHB4O1xuICAgICAgYm94LXNoYWRvdzogMCAxcHggNXB4ICNiNWMwZGU7XG4gICAgICAub2ZmZXItYWJvdXQtYmxvY2sge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIH1cbiAgICAgIC5yZWFkLW1vcmUge1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHotaW5kZXg6IDk5O1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgICAgICBjb2xvcjogIzAwN2U5ZTtcbiAgICAgICAgcmlnaHQ6IDEwcHg7XG4gICAgICAgIGJvdHRvbTogMjBweDtcbiAgICAgICAgXG4gICAgICB9XG4gICAgICAuaW1nLXNlYyB7XG4gICAgICAgIHdpZHRoOiA1MCU7XG4gICAgICAgIG1heC13aWR0aDogaW5pdGlhbDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIFxuICAgICAgICBpbWcge1xuICAgICAgICAgIG1hcmdpbjogMCBhdXRvO1xuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICAgIHdpZHRoOiAxNDBweDtcbiAgICAgICAgICBtaW4taGVpZ2h0OiAxNDBweDtcbiAgICAgICAgICBtYXgtd2lkdGg6IG1heC1jb250ZW50O1xuICAgICAgICB9XG4gICAgICB9XG4gIFxuICAgICAgLmRldGFpbC1zZWMge1xuICAgICAgICB3aWR0aDogNTAlO1xuICAgICAgICBwYWRkaW5nOiAwIDBweCA4cHggMTBweDtcbiAgICAgICAgLy8gcGFkZGluZy1ib3R0b206IDI0cHg7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgaGVpZ2h0OiBhdXRvO1xuICBcbiAgICAgICAgXG4gICAgICAgIC5yZWFkLW1vcmUge1xuICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDIycHg7XG4gICAgICAgICAgY29sb3I6ICMwMDdlOWU7XG4gICAgICAgICAgcmlnaHQ6IDEwcHg7XG4gICAgICAgICAgdG9wOiAxODBweDtcbiAgICAgICAgICBmb250LWZhbWlseTogXCJNeXJpYWRQcm9Cb2xkQ29uZFwiO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICBcbiAgICAuYnRuLWpvaW4ge1xuICAgICAgcGFkZGluZzogN3B4IDEycHg7XG4gICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICBsaW5lLWhlaWdodDogMjVweDtcbiAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgYmFja2dyb3VuZDogIzA0OWZhZDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gICAgICBtYXJnaW46IDAgYXV0bztcbiAgICB9XG4gIFxuICAgIC5vZmZlci1ibG9jayAub3JkZXItZm9vdGVyIHtcbiAgICAgIG1hcmdpbjogMHB4IC01cHggLTEwcHggLTVweDtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgcGFkZGluZzogMnB4IDVweDtcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgIHotaW5kZXg6IDEwMDA7XG4gICAgICBtaW4taGVpZ2h0OiBpbml0aWFsO1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzJmOWZhYztcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgjOGRjY2Q0LCAjMmY5ZmFjKTsgXG4gIFxuICAgICAgLmJ0bi5idG4tb3JkZXIge1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZmO1xuICAgICAgICBtYXJnaW46IDAgYXV0bztcbiAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgIHdpZHRoOiBhdXRvO1xuICAgICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICB9XG4gICAgICAmID4gKiB7XG4gICAgICAgIHdpZHRoOiA0MCU7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIH1cbiAgICAgIC5leHBpcnktYWJvdXQge1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgZm9udC1zaXplOiA1LjJ2dztcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IC0wLjVweDtcbiAgICAgICAgZm9udC1mYW1pbHk6IFwiTXlyaWFkUHJvQ29uZFwiO1xuICAgICAgICBsaW5lLWhlaWdodDogNC42dnc7XG4gICAgICAgIHdpZHRoOiA0OSUgIWltcG9ydGFudDtcbiAgICAgICAgJiA+IHNwYW4ge1xuICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIFxuICAgIC5idG4tb3JkZXIge1xuICAgICAgbWFyZ2luLWxlZnQ6IDA7XG4gICAgICBtaW4td2lkdGg6IDE0NnB4O1xuICAgICAgd2lkdGg6IDEyNnB4O1xuICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAgIGhlaWdodDogNDVweDtcbiAgICB9XG4gICAgLm9mZmVyLWJsb2NrIC5vZmZlci1hYm91dC1ibG9jayB7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgfVxuICBcbiAgICAubGFuZ3VhZ2UtY29uIHtcbiAgICAgIHRvcDogMDtcbiAgICAgIHJpZ2h0OiAwO1xuICAgICAgcGFkZGluZy10b3A6IDVweDtcbiAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICBsZWZ0OiAwO1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgIFxuICAgICAgcGFkZGluZzogM3B4IDVweDtcbiAgICAgIHotaW5kZXg6IDEwMDA7XG4gIFxuICAgIH1cbiAgICAuZmFiQnRuQWN0aXZlIHtcbiAgICAgIGhlaWdodDogMzBweDtcbiAgICAgIHdpZHRoOiAzMHB4O1xuICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoNCwgMTU5LCAxNzMsIDAuNzMpICFpbXBvcnRhbnQ7XG4gICAgICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDQsIDE1OSwgMTczLCAwLjczKSAhaW1wb3J0YW50O1xuICAgICAgY29sb3I6ICNmZmY7XG4gICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIH1cbiAgICAubW9kYWwge1xuICAgICAgZGlzcGxheTogbm9uZTsgLyogSGlkZGVuIGJ5IGRlZmF1bHQgKi9cbiAgICAgIHBvc2l0aW9uOiBmaXhlZDsgLyogU3RheSBpbiBwbGFjZSAqL1xuICAgICAgei1pbmRleDogMTsgLyogU2l0IG9uIHRvcCAqL1xuICAgICAgcGFkZGluZy10b3A6IDEwMHB4OyAvKiBMb2NhdGlvbiBvZiB0aGUgYm94ICovXG4gICAgICBsZWZ0OiAwO1xuICAgICAgdG9wOiAwO1xuICAgICAgd2lkdGg6IDEwMCU7IC8qIEZ1bGwgd2lkdGggKi9cbiAgICAgIGhlaWdodDogMTAwJTsgLyogRnVsbCBoZWlnaHQgKi9cbiAgICAgIG92ZXJmbG93OiBhdXRvOyAvKiBFbmFibGUgc2Nyb2xsIGlmIG5lZWRlZCAqL1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDAsIDAsIDApOyAvKiBGYWxsYmFjayBjb2xvciAqL1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTAsIDI0OCwgMjQ4LCAwLjQpOyAvKiBCbGFjayB3LyBvcGFjaXR5ICovXG4gICAgfVxuICBcbiAgICAvKiBNb2RhbCBDb250ZW50ICovXG4gICAgLm1vZGFsLWNvbnRlbnQge1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZlZmVmZTtcbiAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgIHBhZGRpbmc6IDA7XG4gICAgICBib3JkZXI6IDBweCBzb2xpZCAjODg4O1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xuICAgICAgLXdlYmtpdC1hbmltYXRpb24tbmFtZTogYW5pbWF0ZXRvcDtcbiAgICAgIC13ZWJraXQtYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjRzO1xuICAgICAgYW5pbWF0aW9uLW5hbWU6IGFuaW1hdGV0b3A7XG4gICAgICBhbmltYXRpb24tZHVyYXRpb246IDAuNHM7XG4gICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuICBcbiAgICAvKiBBZGQgQW5pbWF0aW9uICovXG4gICAgQC13ZWJraXQta2V5ZnJhbWVzIGFuaW1hdGV0b3Age1xuICAgICAgZnJvbSB7XG4gICAgICAgIHRvcDogLTMwMHB4O1xuICAgICAgICBvcGFjaXR5OiAwO1xuICAgICAgfVxuICAgICAgdG8ge1xuICAgICAgICB0b3A6IDA7XG4gICAgICAgIG9wYWNpdHk6IDE7XG4gICAgICB9XG4gICAgfVxuICBcbiAgICBAa2V5ZnJhbWVzIGFuaW1hdGV0b3Age1xuICAgICAgZnJvbSB7XG4gICAgICAgIHRvcDogLTMwMHB4O1xuICAgICAgICBvcGFjaXR5OiAwO1xuICAgICAgfVxuICAgICAgdG8ge1xuICAgICAgICB0b3A6IDA7XG4gICAgICAgIG9wYWNpdHk6IDE7XG4gICAgICB9XG4gICAgfVxuICBcbiAgICAvKiBUaGUgQ2xvc2UgQnV0dG9uICovXG4gICAgLmNsb3NlIHtcbiAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgIGZsb2F0OiByaWdodDtcbiAgICAgIGZvbnQtc2l6ZTogMjhweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIH1cbiAgICAubW9kYWwtYm9keSB7XG4gICAgICBtYXgtaGVpZ2h0OiAxMDAlO1xuICAgICAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2I3ZDhlMjtcbiAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgcGFkZGluZzogMDtcbiAgICAgIG92ZXJmbG93OiBzY3JvbGw7XG4gICAgfVxuICAgIC5jbG9zZTpob3ZlcixcbiAgICAuY2xvc2U6Zm9jdXMge1xuICAgICAgY29sb3I6ICMwMDA7XG4gICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgfVxuICBcbiAgICAubW9kYWwtaGVhZGVyIHtcbiAgICAgIHBhZGRpbmc6IDJweCA1cHg7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNzdhMmFmO1xuICAgICAgY29sb3I6IHdoaXRlO1xuICAgIH1cbiAgXG4gICAgLm1vZGFsLWJvZHkge1xuICAgICAgcGFkZGluZzogMnB4IDBweDtcbiAgICB9XG4gIFxuICAgIC5tb2RhbC1jbG9zZSB7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gIFxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzc3YTJhZjtcbiAgICB9XG4gICAgLm1vZGFsMSB7XG4gICAgICBkaXNwbGF5OiBub25lO1xuICAgICAgcG9zaXRpb246IGZpeGVkO1xuICAgICAgei1pbmRleDogMTtcbiAgICAgIHBhZGRpbmctdG9wOiA0OHB4O1xuICAgICAgbGVmdDogMDtcbiAgICAgIHRvcDogMDtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgb3ZlcmZsb3c6IGF1dG87XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgICB9XG4gICAgLm1vZGFsIHtcbiAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgICB6LWluZGV4OiAxO1xuICAgICAgcGFkZGluZy10b3A6IDRweDtcbiAgICAgIGxlZnQ6IDA7XG4gICAgICB0b3A6IDA7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgIG92ZXJmbG93OiBhdXRvO1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gICAgfVxuICAgIC5idG4tbW9kYWwge1xuICAgICBcbiAgICAgIGJveC1zaGFkb3c6IDZweCA2cHggNnB4ICMwMDAwMDAyOTtcbiAgICAgIFxuICAgIH1cbiAgICAub2ZmZXItYmxvY2sgLm9mZmVyLWJsb2NrLWJ0bi1ncm91cCAucmVhZC1tb3JlIHtcbiAgICAgIGJvdHRvbTogMnB4O1xuICAgIH1cbiAgICAub2ZmZXItYmxvY2stYnRuLWdyb3VwIHtcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgIG1pbi1oZWlnaHQ6IDMwcHg7XG4gICAgfVxuICAgIC5idG4tb3JkZXIxIHtcbiAgICAgIHBhZGRpbmc6IDdweCAyN3B4O1xuICAgICAgZm9udC1zaXplOiA2LjY2Njd2dztcbiAgICAgIGxpbmUtaGVpZ2h0OiA2LjY2NjY3dnc7XG4gICAgICBtaW4taGVpZ2h0OiA0MHB4O1xuICAgICBcbiAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgYmFja2dyb3VuZDogI2IwNzZhYztcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gICAgICB3aWR0aDogYXV0bztcbiAgICAgIG1heC13aWR0aDogYXV0bztcbiAgICAgIG1pbi1oZWlnaHQ6IGF1dG87XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICB9XG4gICAgLmJ0bi1vcmRlcjEuYnRuLWZ1bGwge1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBmb250LXNpemU6IDIycHg7XG4gICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICAgIG1pbi1oZWlnaHQ6IDQwcHg7XG4gICAgICBwYWRkaW5nOiAxcHggMjBweDtcbiAgICAgIHdoaXRlLXNwYWNlOiBpbml0aWFsO1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgei1pbmRleDogMTAwO1xuICAgIH1cbiAgICAuYnRuLmJ0bi1vcmRlcjEge1xuICAgICAgYm9yZGVyOiAxcHggc29saWQgI2ZmZjtcbiAgICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgd2lkdGg6IGF1dG87XG4gICAgfVxuICAgIC5idG4uYnRuLW9yZGVyMSB7XG4gICAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZmO1xuICAgICAgLXdlYmtpdC1ib3gtc2hhZG93OiBub25lO1xuICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICBtaW4td2lkdGg6IDE0MHB4O1xuICAgIH1cbiAgICAub3JkZXItZm9vdGVyIHtcbiAgICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XG4gICAgICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIGhlaWdodDogYXV0bztcbiAgICB9XG4gXG4gICAgXG4gICAgLm1peWFiaS1yZWFkLW1vcmUgLm1vZGFsLWJvZHkge1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2E5Y2VkYjtcbiAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xuICAgICAgcGFkZGluZzogNTBweCAxMHB4IDE1cHg7XG4gICAgXG4gICAgIFxuICAgIH1cbiAgICAubWl5YWJpLXJlYWQtbW9yZSAubW9kYWwtYm9keSBwIHNwYW4ge1xuICAgICAgZm9udC1zaXplOiAyNHB4ICFpbXBvcnRhbnQ7XG4gICAgIFxuICAgICAgbGluZS1oZWlnaHQ6IDMycHg7XG4gICAgfVxuICAgIC5idG4uYnRuLWxvZ2luIHtcbiAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAyMHB4KTtcbiAgICAgIG1hcmdpbjogMCAxMHB4IDEwcHg7XG4gICAgICBtaW4td2lkdGg6IGF1dG87XG4gICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICAgIGZvbnQtZmFtaWx5OiBNeXJpYWRQcm8tQm9sZENvbmQ7XG4gICAgICBwYWRkaW5nOiA1cHggMjBweDtcbiAgfVxuICAuYnRuLWxvZ2luIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoIzcwZDFjYywjMDA3ZTllKTtcbiAgICBwYWRkaW5nOiAxMHB4IDMwcHg7XG4gICAgZm9udC1zaXplOiAyNHB4O1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgbGluZS1oZWlnaHQ6IDI0Ljg4cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNjBweDtcbiAgICBib3JkZXI6IDA7XG4gICAgY29sb3I6ICNmZmYhaW1wb3J0YW50O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICBtaW4td2lkdGg6IDI4M3B4O1xuICAgIGZvbnQtZmFtaWx5OiBNeXJpYWRQcm8tQ29uZDtcbn1cbi5iZy1ncmFkaWVudC1ibHVlIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCNiNGIwZGIsI2I3ZDhlMik7XG4gIGNvbG9yOiAjZmZmO1xufVxuXG4ubWVudSB7XG4gIGJvcmRlcjogMDtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XG4gIGZvbnQtc3R5bGU6IGluaGVyaXQ7XG4gIGZvbnQtdmFyaWFudDogaW5oZXJpdDtcbiAgbGluZS1oZWlnaHQ6IDE7XG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIC13ZWJraXQtYXBwZWFyYW5jZTogYnV0dG9uO1xuICBtYXJnaW4tbGVmdDogMjQycHQ7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG4ubG9nbyAuYnJhbmQtbmFtZSB7XG4gIGZvbnQtc2l6ZTogMzFweDtcbiAgZm9udC1mYW1pbHk6IFwiTXlyaWFkUHJvUmVndWxhclwiO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBtYXgtd2lkdGg6IDc0dnc7XG4gIG1pbi13aWR0aDogMjIwcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xufVxuICAiLCIuZGV0YWlsaW5mbyB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IDM4cHg7XG4gIGZvbnQtZmFtaWx5OiBcIk15cmlhZCBQcm8tQ29uZGVuc2VkXCI7XG4gIGxpbmUtaGVpZ2h0OiAzOHB4O1xufVxuXG4uYnRuLWZpbGwtdGhlbWUge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuICBsaW5lLWhlaWdodDogMS41NTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgYm9yZGVyLXdpZHRoOiAxcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM2ZjQwNzU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIHRyYW5zaXRpb246IGJhY2tncm91bmQtY29sb3IgMC4ycyBlYXNlLWluLW91dCwgY29sb3IgMC4ycyBlYXNlLWluLW91dCwgYm9yZGVyLWNvbG9yIDAuMnMgZWFzZS1pbi1vdXQ7XG59XG5cbi5idG4ge1xuICBmb250LXNpemU6IDUwcHg7XG4gIGxpbmUtaGVpZ2h0OiAxO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjA3N2FjO1xuICBmb250LWZhbWlseTogTXlyaWFkUHJvQ29uZDtcbiAgd2lkdGg6IDI1MHB4O1xuICBib3gtc2hhZG93OiAzcHggM3B4IDZweCAjMzMzMztcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgcGFkZGluZzogMCAyNXB4O1xuICBtaW4taGVpZ2h0OiA3MHB4O1xuICBtYXJnaW46IDEwcHggYXV0bztcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgY29sb3I6ICNmZmY7XG59XG5cbmJ1dHRvbi5idG4tb3JkZXIge1xuICBib3JkZXI6IDA7XG59XG5cbi5vcmRlci1mb290ZXIgLmJ0bi1vcmRlciB7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgjNzBkMWNjLCAjMDA3ZTllKTtcbn1cblxuLmJ0bi1vcmRlci5idG4tZ3JhZGVpZW50IHtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KCM3MGQxY2MsICMwMDdlOWUpICFpbXBvcnRhbnQ7XG59XG5cbi5idG46aG92ZXIsXG4uYnRuLW91dGxpbmUtdGhlbWU6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2ZmY2ZiO1xuICBib3JkZXItY29sb3I6ICNmZmZmZmY7XG4gIGNvbG9yOiAjZmZmO1xufVxuXG4uYnRuLW91dGxpbmUtdGhlbWUge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuICBsaW5lLWhlaWdodDogMS41NTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgYm9yZGVyLXdpZHRoOiAycHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gIGJvcmRlci1jb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgdHJhbnNpdGlvbjogYmFja2dyb3VuZC1jb2xvciAwLjJzIGVhc2UtaW4tb3V0LCBjb2xvciAwLjJzIGVhc2UtaW4tb3V0LCBib3JkZXItY29sb3IgMC4ycyBlYXNlLWluLW91dDtcbn1cblxuLmNvbnRhaW5lciB7XG4gIHotaW5kZXg6IDE7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLm1haW4tY3NlY3Rpb24ge1xuICBtaW4taGVpZ2h0OiA2NTBweDtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWdyYWRpZW50KGxpbmVhciwgbGVmdCB0b3AsIGxlZnQgYm90dG9tLCBmcm9tKHJnYmEoMTEzLCA0NiwgMTMzLCAwLjMpKSwgdG8ocmdiYSg1LCAyNCwgNjksIDAuNikpKTtcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQodG9wLCByZ2JhKDExMywgNDYsIDEzMywgMC4zKSwgcmdiYSg1LCAyNCwgNjksIDAuNikpO1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gYm90dG9tLCByZ2JhKDExMywgNDYsIDEzMywgMC4zKSwgcmdiYSg1LCAyNCwgNjksIDAuNikpO1xufVxuXG4uYWRkLXNlY3Rpb24ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiA1MHB4O1xufVxuXG4ud2F0ZXItbWFyay1vcmRlciB7XG4gIGxlZnQ6IDBweDtcbiAgb3BhY2l0eTogMHB4O1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbi5oYW1iZXJndXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4uaGFtYmVyZ3VyIGJ1dHRvbiB7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXI6IDA7XG4gIGJveC1zaGFkb3c6IDA7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5oYW1iZXJndXIgaW1nIHtcbiAgd2lkdGg6IDMycHg7XG4gIG1heC13aWR0aDogOHZ3O1xufVxuXG4uaGFtYmVyZ3VyIC5kcm9wZG93bi1tZW51IHtcbiAgcGFkZGluZzogMDtcbiAgYmFja2dyb3VuZDogIzI2MmE0OTtcbiAgY29sb3I6ICNmZmY7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBmb250LXNpemU6IDMycHg7XG4gIGxpbmUtaGVpZ2h0OiAzNnB4O1xuICBib3JkZXItcmFkaXVzOiAwO1xuICB3aWR0aDogMzgwcHg7XG4gIG1heC13aWR0aDogNzB2dztcbiAgbGVmdDogYXV0byAhaW1wb3J0YW50O1xuICByaWdodDogMDtcbiAgdG9wOiAxMDAlICFpbXBvcnRhbnQ7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgMCwgMHB4KSAhaW1wb3J0YW50O1xufVxuLmhhbWJlcmd1ciAuZHJvcGRvd24tbWVudSBsaSB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZmZmO1xuICBwYWRkaW5nOiAyMHB4IDMwcHg7XG4gIGNvbG9yOiAjZmZmO1xufVxuLmhhbWJlcmd1ciAuZHJvcGRvd24tbWVudSBsaTpmaXJzdC1jaGlsZCB7XG4gIHBhZGRpbmctdG9wOiA0MHB4O1xufVxuXG51bC5hY3RpdmUuZHJvcGRvd24tbWVudSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB6LWluZGV4OiAxMDAwMDtcbn1cblxuLmJhbm5lciB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL3N0YXRpYy50aWxkYWNkbi5jb20vdGlsZDMyMzEtNjEzMy00MTY1LWIyNjQtMzkzNTM3NjU2NTYzL2ZvbjEuanBnKTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogdG9wIGNlbnRlcjtcbiAgcGFkZGluZzogMzBweCAyMHB4O1xuICBtYXJnaW46IDIwcHggMTVweDtcbn1cbi5iYW5uZXIgLmJhbm5lci1hYm91dCB7XG4gIGNvbG9yOiAjZmZmO1xufVxuLmJhbm5lciAuYmFubmVyLWFib3V0IC5iYW5uZXItdGl0bGUge1xuICBmb250LXNpemU6IDMycHg7XG4gIGxpbmUtaGVpZ2h0OiAxLjI1O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBjb2xvcjogI2ZmZjtcbn1cbi5iYW5uZXIgLmJhbm5lci1hYm91dCAuZGF0ZS1kZXRhaWxzIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBsaW5lLWhlaWdodDogMS40O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBjb2xvcjogI2ZmZjtcbn1cbi5iYW5uZXIgLmJhbm5lci1hYm91dCAuZGF0ZS1kZXRhaWxzIHNwYW4ge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cbi5iYW5uZXIgLmJhbm5lci1hYm91dCAuYmFubmVyLWRldGFpbHMge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiAxLjU7XG4gIGNvbG9yOiAjZmZmO1xufVxuLmJhbm5lciAuc3RhcnQtYnRuIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cbi5iYW5uZXIgLnN0YXJ0LWJ0biAuYnRuIHtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgd2lkdGg6IGF1dG87XG4gIGZvbnQtZmFtaWx5OiBcIk15cmlhZFByb0JvbGRDb25kXCI7XG4gIGZvbnQtc2l6ZTogNjBweDtcbiAgbWluLWhlaWdodDogNTBweDtcbiAgcGFkZGluZzogOXB4IDUwcHg7XG4gIGxpbmUtaGVpZ2h0OiAxLjI7XG59XG4uYmFubmVyIC5zdGFydC1idG4gLmJ0biAuZmEge1xuICBmb250LXNpemU6IDUwcHg7XG59XG5cbi5idG4tcHVycGxlIHtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KCNDNzgyQzMsICM3OTU1NzYpICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjZmZmICFpbXBvcnRhbnQ7XG59XG5cbi5idG4tY29sb3ItcHVycGxlIHtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbiAgY29sb3I6ICNiMDc2YWMgIWltcG9ydGFudDtcbiAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiA3cHggMjdweDtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBsaW5lLWhlaWdodDogMjBweDtcbiAgbWluLWhlaWdodDogNDBweDtcbiAgd2lkdGg6IGF1dG87XG4gIG1pbi1oZWlnaHQ6IGF1dG87XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG59XG5cbi53YXRlci1tYXJrLW9yZGVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiAxMDAwO1xuICBsZWZ0OiAtMTBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4ubG9naW4tYnRuIHtcbiAgd2lkdGg6IDkwJTtcbiAgcGFkZGluZzogNXB4IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwNDlmYWQ7XG4gIGZvbnQtc2l6ZTogMjdweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXgtd2lkdGg6IDgwdnc7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLWJvdHRvbTogMTJweDtcbn1cblxuLmJ0bi1vcmRlciB7XG4gIHBhZGRpbmc6IDdweCAyN3B4O1xuICBmb250LXNpemU6IDYuNjY3dnc7XG4gIGxpbmUtaGVpZ2h0OiA2LjY2N3Z3O1xuICBtaW4taGVpZ2h0OiA0MHB4O1xuICBjb2xvcjogI2ZmZjtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KCM3MGQxY2MsICMwMDdlOWUpO1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICB3aWR0aDogMzZ2dztcbiAgbWluLWhlaWdodDogYXV0bztcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5idG4tb3JkZXIgaSB7XG4gIG1hcmdpbi1sZWZ0OiAxNXB4O1xufVxuXG4uYnRuLW9yZGVyLmJ0bi1mdWxsIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtc2l6ZTogMjRweDtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIG1pbi1oZWlnaHQ6IDUycHg7XG4gIHBhZGRpbmc6IDFweCAyMHB4O1xuICB3aGl0ZS1zcGFjZTogaW5pdGlhbDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB6LWluZGV4OiAxMDA7XG4gIGhlaWdodDogNTBweDtcbn1cblxuLmhlYWQge1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoI2ZmZiwgIzRjYjRjOSAyNzAlKTtcbiAgYm94LXNoYWRvdzogMHB4IDFweCA1cHggI2I1YzBkZTtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgZm9udC1zaXplOiAzNnB4O1xuICBsaW5lLWhlaWdodDogNDRweDtcbiAgZm9udC1mYW1pbHk6IFwiTXlyaWFkUHJvQ29uZFwiO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjMDQ5ZmFkO1xuICBwYWRkaW5nOiAxNnB4IDI1cHggMTdweDtcbn1cblxuLm9mZmVyLWJsb2NrIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZzogMTBweCA1cHggNXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1ib3R0b206IDRweDtcbiAgYm94LXNoYWRvdzogMCAxcHggNXB4ICNiNWMwZGU7XG59XG4ub2ZmZXItYmxvY2sgLm9mZmVyLWFib3V0LWJsb2NrIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4ub2ZmZXItYmxvY2sgLnJlYWQtbW9yZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogOTk7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIGNvbG9yOiAjMDA3ZTllO1xuICByaWdodDogMTBweDtcbiAgYm90dG9tOiAyMHB4O1xufVxuLm9mZmVyLWJsb2NrIC5pbWctc2VjIHtcbiAgd2lkdGg6IDUwJTtcbiAgbWF4LXdpZHRoOiBpbml0aWFsO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cbi5vZmZlci1ibG9jayAuaW1nLXNlYyBpbWcge1xuICBtYXJnaW46IDAgYXV0bztcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICB3aWR0aDogMTQwcHg7XG4gIG1pbi1oZWlnaHQ6IDE0MHB4O1xuICBtYXgtd2lkdGg6IG1heC1jb250ZW50O1xufVxuLm9mZmVyLWJsb2NrIC5kZXRhaWwtc2VjIHtcbiAgd2lkdGg6IDUwJTtcbiAgcGFkZGluZzogMCAwcHggOHB4IDEwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuLm9mZmVyLWJsb2NrIC5kZXRhaWwtc2VjIC5yZWFkLW1vcmUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDIycHg7XG4gIGNvbG9yOiAjMDA3ZTllO1xuICByaWdodDogMTBweDtcbiAgdG9wOiAxODBweDtcbiAgZm9udC1mYW1pbHk6IFwiTXlyaWFkUHJvQm9sZENvbmRcIjtcbn1cblxuLmJ0bi1qb2luIHtcbiAgcGFkZGluZzogN3B4IDEycHg7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDI1cHg7XG4gIGNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kOiAjMDQ5ZmFkO1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICBtYXJnaW46IDAgYXV0bztcbn1cblxuLm9mZmVyLWJsb2NrIC5vcmRlci1mb290ZXIge1xuICBtYXJnaW46IDBweCAtNXB4IC0xMHB4IC01cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZzogMnB4IDVweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB6LWluZGV4OiAxMDAwO1xuICBtaW4taGVpZ2h0OiBpbml0aWFsO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmY5ZmFjO1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoIzhkY2NkNCwgIzJmOWZhYyk7XG59XG4ub2ZmZXItYmxvY2sgLm9yZGVyLWZvb3RlciAuYnRuLmJ0bi1vcmRlciB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZmY7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBib3gtc2hhZG93OiBub25lO1xuICB3aWR0aDogMTAwJTtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgd2lkdGg6IGF1dG87XG4gIGhlaWdodDogYXV0bztcbn1cbi5vZmZlci1ibG9jayAub3JkZXItZm9vdGVyID4gKiB7XG4gIHdpZHRoOiA0MCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5vZmZlci1ibG9jayAub3JkZXItZm9vdGVyIC5leHBpcnktYWJvdXQge1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC1zaXplOiA1LjJ2dztcbiAgbGV0dGVyLXNwYWNpbmc6IC0wLjVweDtcbiAgZm9udC1mYW1pbHk6IFwiTXlyaWFkUHJvQ29uZFwiO1xuICBsaW5lLWhlaWdodDogNC42dnc7XG4gIHdpZHRoOiA0OSUgIWltcG9ydGFudDtcbn1cbi5vZmZlci1ibG9jayAub3JkZXItZm9vdGVyIC5leHBpcnktYWJvdXQgPiBzcGFuIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuXG4uYnRuLW9yZGVyIHtcbiAgbWFyZ2luLWxlZnQ6IDA7XG4gIG1pbi13aWR0aDogMTQ2cHg7XG4gIHdpZHRoOiAxMjZweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgaGVpZ2h0OiA0NXB4O1xufVxuXG4ub2ZmZXItYmxvY2sgLm9mZmVyLWFib3V0LWJsb2NrIHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xufVxuXG4ubGFuZ3VhZ2UtY29uIHtcbiAgdG9wOiAwO1xuICByaWdodDogMDtcbiAgcGFkZGluZy10b3A6IDVweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDNweCA1cHg7XG4gIHotaW5kZXg6IDEwMDA7XG59XG5cbi5mYWJCdG5BY3RpdmUge1xuICBoZWlnaHQ6IDMwcHg7XG4gIHdpZHRoOiAzMHB4O1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDQsIDE1OSwgMTczLCAwLjczKSAhaW1wb3J0YW50O1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDQsIDE1OSwgMTczLCAwLjczKSAhaW1wb3J0YW50O1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4ubW9kYWwge1xuICBkaXNwbGF5OiBub25lO1xuICAvKiBIaWRkZW4gYnkgZGVmYXVsdCAqL1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIC8qIFN0YXkgaW4gcGxhY2UgKi9cbiAgei1pbmRleDogMTtcbiAgLyogU2l0IG9uIHRvcCAqL1xuICBwYWRkaW5nLXRvcDogMTAwcHg7XG4gIC8qIExvY2F0aW9uIG9mIHRoZSBib3ggKi9cbiAgbGVmdDogMDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgLyogRnVsbCB3aWR0aCAqL1xuICBoZWlnaHQ6IDEwMCU7XG4gIC8qIEZ1bGwgaGVpZ2h0ICovXG4gIG92ZXJmbG93OiBhdXRvO1xuICAvKiBFbmFibGUgc2Nyb2xsIGlmIG5lZWRlZCAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgLyogRmFsbGJhY2sgY29sb3IgKi9cbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTAsIDI0OCwgMjQ4LCAwLjQpO1xuICAvKiBCbGFjayB3LyBvcGFjaXR5ICovXG59XG5cbi8qIE1vZGFsIENvbnRlbnQgKi9cbi5tb2RhbC1jb250ZW50IHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmVmZWZlO1xuICBtYXJnaW46IGF1dG87XG4gIHBhZGRpbmc6IDA7XG4gIGJvcmRlcjogMHB4IHNvbGlkICM4ODg7XG4gIHdpZHRoOiAxMDAlO1xuICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xuICAtd2Via2l0LWFuaW1hdGlvbi1uYW1lOiBhbmltYXRldG9wO1xuICAtd2Via2l0LWFuaW1hdGlvbi1kdXJhdGlvbjogMC40cztcbiAgYW5pbWF0aW9uLW5hbWU6IGFuaW1hdGV0b3A7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC40cztcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4vKiBBZGQgQW5pbWF0aW9uICovXG5ALXdlYmtpdC1rZXlmcmFtZXMgYW5pbWF0ZXRvcCB7XG4gIGZyb20ge1xuICAgIHRvcDogLTMwMHB4O1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cbiAgdG8ge1xuICAgIHRvcDogMDtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG59XG5Aa2V5ZnJhbWVzIGFuaW1hdGV0b3Age1xuICBmcm9tIHtcbiAgICB0b3A6IC0zMDBweDtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG4gIHRvIHtcbiAgICB0b3A6IDA7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxufVxuLyogVGhlIENsb3NlIEJ1dHRvbiAqL1xuLmNsb3NlIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5tb2RhbC1ib2R5IHtcbiAgbWF4LWhlaWdodDogMTAwJTtcbiAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjdkOGUyO1xuICBjb2xvcjogI2ZmZjtcbiAgcGFkZGluZzogMDtcbiAgb3ZlcmZsb3c6IHNjcm9sbDtcbn1cblxuLmNsb3NlOmhvdmVyLFxuLmNsb3NlOmZvY3VzIHtcbiAgY29sb3I6ICMwMDA7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4ubW9kYWwtaGVhZGVyIHtcbiAgcGFkZGluZzogMnB4IDVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzc3YTJhZjtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4ubW9kYWwtYm9keSB7XG4gIHBhZGRpbmc6IDJweCAwcHg7XG59XG5cbi5tb2RhbC1jbG9zZSB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzc3YTJhZjtcbn1cblxuLm1vZGFsMSB7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgei1pbmRleDogMTtcbiAgcGFkZGluZy10b3A6IDQ4cHg7XG4gIGxlZnQ6IDA7XG4gIHRvcDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgb3ZlcmZsb3c6IGF1dG87XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XG59XG5cbi5tb2RhbCB7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgei1pbmRleDogMTtcbiAgcGFkZGluZy10b3A6IDRweDtcbiAgbGVmdDogMDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBvdmVyZmxvdzogYXV0bztcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC40KTtcbn1cblxuLmJ0bi1tb2RhbCB7XG4gIGJveC1zaGFkb3c6IDZweCA2cHggNnB4ICMwMDAwMDAyOTtcbn1cblxuLm9mZmVyLWJsb2NrIC5vZmZlci1ibG9jay1idG4tZ3JvdXAgLnJlYWQtbW9yZSB7XG4gIGJvdHRvbTogMnB4O1xufVxuXG4ub2ZmZXItYmxvY2stYnRuLWdyb3VwIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtaW4taGVpZ2h0OiAzMHB4O1xufVxuXG4uYnRuLW9yZGVyMSB7XG4gIHBhZGRpbmc6IDdweCAyN3B4O1xuICBmb250LXNpemU6IDYuNjY2N3Z3O1xuICBsaW5lLWhlaWdodDogNi42NjY2N3Z3O1xuICBtaW4taGVpZ2h0OiA0MHB4O1xuICBjb2xvcjogI2ZmZjtcbiAgYmFja2dyb3VuZDogI2IwNzZhYztcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgd2lkdGg6IGF1dG87XG4gIG1heC13aWR0aDogYXV0bztcbiAgbWluLWhlaWdodDogYXV0bztcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5idG4tb3JkZXIxLmJ0bi1mdWxsIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIG1pbi1oZWlnaHQ6IDQwcHg7XG4gIHBhZGRpbmc6IDFweCAyMHB4O1xuICB3aGl0ZS1zcGFjZTogaW5pdGlhbDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB6LWluZGV4OiAxMDA7XG59XG5cbi5idG4uYnRuLW9yZGVyMSB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZmY7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIHdpZHRoOiBhdXRvO1xufVxuXG4uYnRuLmJ0bi1vcmRlcjEge1xuICBib3JkZXI6IDFweCBzb2xpZCAjZmZmO1xuICAtd2Via2l0LWJveC1zaGFkb3c6IG5vbmU7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG1pbi13aWR0aDogMTQwcHg7XG59XG5cbi5vcmRlci1mb290ZXIge1xuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGhlaWdodDogYXV0bztcbn1cblxuLm1peWFiaS1yZWFkLW1vcmUgLm1vZGFsLWJvZHkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTljZWRiO1xuICBmb250LXNpemU6IDI0cHg7XG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xuICBwYWRkaW5nOiA1MHB4IDEwcHggMTVweDtcbn1cblxuLm1peWFiaS1yZWFkLW1vcmUgLm1vZGFsLWJvZHkgcCBzcGFuIHtcbiAgZm9udC1zaXplOiAyNHB4ICFpbXBvcnRhbnQ7XG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xufVxuXG4uYnRuLmJ0bi1sb2dpbiB7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSAyMHB4KTtcbiAgbWFyZ2luOiAwIDEwcHggMTBweDtcbiAgbWluLXdpZHRoOiBhdXRvO1xuICBmb250LXNpemU6IDI0cHg7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBmb250LWZhbWlseTogTXlyaWFkUHJvLUJvbGRDb25kO1xuICBwYWRkaW5nOiA1cHggMjBweDtcbn1cblxuLmJ0bi1sb2dpbiB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgjNzBkMWNjLCAjMDA3ZTllKTtcbiAgcGFkZGluZzogMTBweCAzMHB4O1xuICBmb250LXNpemU6IDI0cHg7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIGxpbmUtaGVpZ2h0OiAyNC44OHB4O1xuICBib3JkZXItcmFkaXVzOiA2MHB4O1xuICBib3JkZXI6IDA7XG4gIGNvbG9yOiAjZmZmICFpbXBvcnRhbnQ7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW46IDAgYXV0bztcbiAgbWluLXdpZHRoOiAyODNweDtcbiAgZm9udC1mYW1pbHk6IE15cmlhZFByby1Db25kO1xufVxuXG4uYmctZ3JhZGllbnQtYmx1ZSB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2I0YjBkYiwgI2I3ZDhlMik7XG4gIGNvbG9yOiAjZmZmO1xufVxuXG4ubWVudSB7XG4gIGJvcmRlcjogMDtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XG4gIGZvbnQtc3R5bGU6IGluaGVyaXQ7XG4gIGZvbnQtdmFyaWFudDogaW5oZXJpdDtcbiAgbGluZS1oZWlnaHQ6IDE7XG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIC13ZWJraXQtYXBwZWFyYW5jZTogYnV0dG9uO1xuICBtYXJnaW4tbGVmdDogMjQycHQ7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi5sb2dvIC5icmFuZC1uYW1lIHtcbiAgZm9udC1zaXplOiAzMXB4O1xuICBmb250LWZhbWlseTogXCJNeXJpYWRQcm9SZWd1bGFyXCI7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG1heC13aWR0aDogNzR2dztcbiAgbWluLXdpZHRoOiAyMjBweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/orderlist/orderlist.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/orderlist/orderlist.page.ts ***!
    \*********************************************/

  /*! exports provided: OrderlistPage */

  /***/
  function srcAppOrderlistOrderlistPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "OrderlistPage", function () {
      return OrderlistPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _language_service_language_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../language-service/language-service */
    "./src/app/language-service/language-service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _marathan_marathanService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../marathan/marathanService */
    "./marathan/marathanService.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

    var OrderlistPage = /*#__PURE__*/function () {
      function OrderlistPage(router, navCtrl, navParams, marathanService, languageService, modalCtrl, menuCtrl) {
        _classCallCheck(this, OrderlistPage);

        this.router = router;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.marathanService = marathanService;
        this.languageService = languageService;
        this.modalCtrl = modalCtrl;
        this.menuCtrl = menuCtrl;
        this.langCode = localStorage.getItem("languageType");
        var languageCode = localStorage.getItem("languageType");
        this.languageType = this.languageService.languageCode(languageCode);
        this.getMarathanList();
      }

      _createClass(OrderlistPage, [{
        key: "getMarathanList",
        value: function getMarathanList() {
          var _this = this;

          var event = {
            pageSize: 10,
            pageIndex: 0
          };
          this.marathanService.getUserOrderList().subscribe(function (response) {
            console.log('response New orders', response.json());
            _this.marathonlistdata = response.json();
          });
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "menu",
        value: function menu() {
          this.router.navigate(['menu']);
        }
      }, {
        key: "english",
        value: function english() {
          localStorage.setItem("languageType", "Eng");
          this.langCode = "Eng";
          this.marathonList();
          var languageCode = localStorage.getItem("languageType");
          this.languageType = this.languageService.languageCode(languageCode);
          this.langCode = localStorage.getItem("languageType");
        }
      }, {
        key: "russian",
        value: function russian() {
          localStorage.setItem("languageType", "Rus");
          this.langCode = "Rus";
          this.marathonList();
          var languageCode = localStorage.getItem("languageType");
          this.languageType = this.languageService.languageCode(languageCode);
          this.langCode = localStorage.getItem("languageType");
        }
      }, {
        key: "italian",
        value: function italian() {
          localStorage.setItem("languageType", "Ita");
          this.langCode = "Ita";
          this.marathonList();
          var languageCode = localStorage.getItem("languageType");
          this.languageType = this.languageService.languageCode(languageCode);
          this.langCode = localStorage.getItem("languageType");
        }
      }, {
        key: "marathonList",
        value: function marathonList() {
          var _this2 = this;

          this.show = true;
          this.marathonService.getUserOrderList().subscribe(function (response) {
            console.log('response New orders', response.json());
            _this2.show = false;
            _this2.iserror = false;
            _this2.allMarathon = response.json();
          }, function (error) {
            _this2.error = error.text();
          });
        } // goToMarathon(id, extension) {
        //   console.log("extension", extension)
        //   localStorage.setItem('extension', extension)
        //   var loaderMessage: any;
        //   if (localStorage.getItem("languageType") == 'Eng') {
        //     loaderMessage = 'Please wait a bit...';
        //   } else if (localStorage.getItem("languageType") == 'Rus') {
        //     loaderMessage = 'чуточку подождите...';
        //   } else {
        //     loaderMessage = 'per favore aspetta un po ...';
        //   }
        //   // let loader = this.loader.create({
        //   //   content: loaderMessage
        //   // })
        //   // loader.present();
        //   localStorage.removeItem("marathonId");
        //   localStorage.setItem("marathonId", id);
        //   // localStorage.setItem("currentContestMarathonId" , id);
        //   localStorage.setItem("orderStatus", 'Approved');
        //   this.marathonService.getUserMarathonDetail(id).subscribe(response => {
        //     var data: MarathonData = response.json();
        //     console.log("marathonlist", data);
        //     loader.dismissAll();
        //     if (data.productType == "GreatExtension") {
        //       if (data.greatExtensionDays == null) {
        //         // this.navCtrl.setRoot(StartmarathonPage, { user_marathan_id: id })
        //         this.router.navigateByUrl('/(StartmarathonPage:{user_marathan_id: id}');
        //       }
        //       if (data.greatExtensionDays != null) {
        //         if (data.greatExtensionDays.length > 0) {
        //           let isActive = false;
        //           for (let index = 0; index < data.greatExtensionDays.length; index++) {
        //             if (data.greatExtensionDays[index].isActive == true) {
        //               isActive = true;
        //               var dayId = data.greatExtensionDays[index].id;
        //               localStorage.setItem("day_id", dayId);
        //               // this.navCtrl.setRoot(UsermarathonPage, { dayId: dayId, user_marathan_id: id })
        //               this.router.navigateByUrl('/(StartmarathonPage:{user_marathan_id: id}');
        //             }
        //           }
        //           if (!isActive) {
        //             // this.navCtrl.setRoot(StartmarathonPage, { user_marathan_id: id });
        //             this.router.navigateByUrl('/(StartmarathonPage:{user_marathan_id: id}');
        //           }
        //         }
        //       }
        //     }
        //     else {
        //       if (data.marathonDays == null) {
        //         // this.navCtrl.setRoot(StartmarathonPage, { user_marathan_id: id })
        //         this.router.navigateByUrl('/(StartmarathonPage:{user_marathan_id: id}');
        //       }
        //       if (data.marathonDays != null) {
        //         if (data.marathonDays.length > 0) {
        //           let isActive = false;
        //           for (let index = 0; index < data.marathonDays.length; index++) {
        //             if (data.marathonDays[index].isActive == true) {
        //               isActive = true;
        //               var dayId = data.marathonDays[index].id;
        //               localStorage.setItem("day_id", dayId);
        //               // this.navCtrl.setRoot(UsermarathonPage, { dayId: dayId, user_marathan_id: id });
        //               this.router.navigateByUrl('/(StartmarathonPage:{user_marathan_id: id}');
        //             }
        //           }
        //           if (!isActive) {
        //             // this.navCtrl.setRoot(StartmarathonPage, { user_marathan_id: id });
        //             this.router.navigateByUrl('/(StartmarathonPage:{user_marathan_id: id}');
        //           }
        //         }
        //       }
        //     }
        //   });
        // }

      }, {
        key: "copyCoupon",
        value: function copyCoupon(val) {
          var selBox = document.createElement('textarea');
          selBox.style.position = 'fixed';
          selBox.style.left = '0';
          selBox.style.top = '0';
          selBox.style.opacity = '0';
          selBox.value = val;
          document.body.appendChild(selBox);
          selBox.focus();
          selBox.select();
          document.execCommand('copy');
          document.body.removeChild(selBox);
          this.textCopy = true;
          var toast = this.toastCtrl.create({
            message: 'Код купона скопирован',
            duration: 3000,
            position: "bottom"
          });
          toast.present();
        }
      }, {
        key: "readMore",
        value: function readMore(description) {
          this.showdetails = description;
        }
      }, {
        key: "dateFormatConvert",
        value: function dateFormatConvert(date) {
          var dateFormated = date.split('T').reverse()[1];
          var reverseDate = dateFormated.split('-').reverse().join().replace(/,/g, '.');
          return reverseDate;
        }
      }, {
        key: "pay",
        value: function pay() {
          this.router.navigate(['payment']);
        }
      }]);

      return OrderlistPage;
    }();

    OrderlistPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavParams"]
      }, {
        type: _marathan_marathanService__WEBPACK_IMPORTED_MODULE_4__["MarathanProvider"]
      }, {
        type: _language_service_language_service__WEBPACK_IMPORTED_MODULE_2__["LanguageServiceProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonContent"], {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonContent"])], OrderlistPage.prototype, "container", void 0);
    OrderlistPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-orderlist',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./orderlist.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/orderlist/orderlist.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./orderlist.page.scss */
      "./src/app/orderlist/orderlist.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavParams"], _marathan_marathanService__WEBPACK_IMPORTED_MODULE_4__["MarathanProvider"], _language_service_language_service__WEBPACK_IMPORTED_MODULE_2__["LanguageServiceProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"]])], OrderlistPage);
    /***/
  }
}]);
//# sourceMappingURL=orderlist-orderlist-module-es5.js.map