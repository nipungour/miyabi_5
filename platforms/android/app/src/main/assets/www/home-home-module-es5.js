function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
  /*!***************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
    \***************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHomeHomePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-content>\n     <div class=\"landing-page\">\n        <div class=\"language-con\">\n            <p [ngClass]=\"(langCode == 'Rus')?'fabBtnActive':'language'\" (click)=\"russian()\" style=\"float: right; \">Ru\n            </p>\n            <p [ngClass]=\"(langCode == 'Eng')?'fabBtnActive':'language'\" (click)=\"english()\"\n                style=\"float: right;margin: 0 4px;\">En</p>\n            <p style=\"float: right;\" (click)=\"italian()\" [ngClass]=\"(langCode == 'Ita')?'fabBtnActive':'language'\">It\n            </p>\n        </div>\n     <div class=\"miyabi-contianer\">\n             <div class=\"logo-detail\"><img src=\"../../assets/icon/logo-details-sm.svg\" /></div>\n             <div class=\"\">\n                 <button class=\"btn login-btn\" style=\"font-family: MyriadProBoldCond ; color : #fff\" (click)=\"login()\">{{languageType?.landingPage?.loginButtonTittle}}</button>\n             </div>\n             \n         </div>\n  </div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/home/home-routing.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/home/home-routing.module.ts ***!
    \*********************************************/

  /*! exports provided: HomePageRoutingModule */

  /***/
  function srcAppHomeHomeRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function () {
      return HomePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./home.page */
    "./src/app/home/home.page.ts");

    var routes = [{
      path: '',
      component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"]
    }];

    var HomePageRoutingModule = function HomePageRoutingModule() {
      _classCallCheck(this, HomePageRoutingModule);
    };

    HomePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], HomePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/home/home.module.ts":
  /*!*************************************!*\
    !*** ./src/app/home/home.module.ts ***!
    \*************************************/

  /*! exports provided: HomePageModule */

  /***/
  function srcAppHomeHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePageModule", function () {
      return HomePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./home.page */
    "./src/app/home/home.page.ts");
    /* harmony import */


    var _home_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./home-routing.module */
    "./src/app/home/home-routing.module.ts");

    var HomePageModule = function HomePageModule() {
      _classCallCheck(this, HomePageModule);
    };

    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _home_routing_module__WEBPACK_IMPORTED_MODULE_6__["HomePageRoutingModule"]],
      declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]]
    })], HomePageModule);
    /***/
  },

  /***/
  "./src/app/home/home.page.scss":
  /*!*************************************!*\
    !*** ./src/app/home/home.page.scss ***!
    \*************************************/

  /*! exports provided: default */

  /***/
  function srcAppHomeHomePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".bg-gradient {\n  background-image: linear-gradient(140deg, #639299, #639298);\n  box-shadow: 0px 2px 2px #00000024;\n}\n\n.row {\n  margin-left: 0px !important;\n  margin-right: 0 !important;\n}\n\n.landing-page {\n  background-image: url('mma-background-375.png');\n  background-size: 100% 100%;\n  background-position: top center;\n  font-family: \"MyriadProRegular\";\n  padding-bottom: 48vh;\n  min-height: calc(100vh - 60px);\n  background-attachment: fixed;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.logo-detail {\n  text-align: right;\n  padding: 0 0;\n  max-width: 80%;\n  width: calc(100vw - 30px);\n  margin-left: 14%;\n}\n\n.miyabi-contianer {\n  max-width: 720px;\n  margin: 0 auto;\n  z-index: 1;\n  position: relative;\n}\n\n.login-btn {\n  z-index: 1000;\n  width: 75.734%;\n  padding: 2px 10px 8px;\n  border-radius: 25px;\n  background-color: rgba(4, 159, 173, 0.73);\n  font-size: 27px;\n  text-align: center;\n  max-width: 80vw;\n  margin: 0 auto;\n  display: block;\n  margin-bottom: 12px;\n  box-shadow: none;\n}\n\n.btn {\n  line-height: 1;\n  border: none;\n}\n\n.language-con {\n  top: 0;\n  right: 0;\n  padding-top: 5px;\n  padding-bottom: 5px;\n  background: transparent;\n  box-shadow: none;\n  position: absolute;\n  left: 0;\n  height: auto;\n  width: 100%;\n  padding: 3px 5px;\n  z-index: 1000;\n}\n\n.fabBtnActive {\n  height: 30px;\n  width: 30px;\n  border-radius: 50px;\n  background-color: rgba(4, 159, 173, 0.73) !important;\n  border: 1px solid rgba(4, 159, 173, 0.73);\n  color: #fff;\n  font-size: 20px;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  cursor: pointer;\n}\n\n.modal-open .modal {\n  overflow-x: hidden;\n  overflow-y: auto;\n  background: #3333;\n}\n\n.offer-block .img-sec img {\n  width: 100%;\n  border-radius: 50%;\n  margin-bottom: 10px;\n  max-width: 140px;\n  max-height: 140px;\n  height: 40vw;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3Bpem9uZS9teWRheXMvc3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFFO0VBQ0ksMkRBQUE7RUFDQSxpQ0FBQTtBQ0NOOztBREVJO0VBQ0UsMkJBQUE7RUFDQSwwQkFBQTtBQ0NOOztBREVJO0VBQWUsK0NBQUE7RUFDYiwwQkFBQTtFQUNBLCtCQUFBO0VBQ0EsK0JBQUE7RUFFQSxvQkFBQTtFQUNBLDhCQUFBO0VBQ0EsNEJBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7QUNDTjs7QURFSTtFQUNFLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0FDQ047O0FERUk7RUFDRSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7QUNDTjs7QURFSTtFQUNFLGFBQUE7RUFDQSxjQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlDQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQ0NOOztBREVJO0VBQ0UsY0FBQTtFQUNBLFlBQUE7QUNDTjs7QURFSTtFQUNFLE1BQUE7RUFDQSxRQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUVBLGdCQUFBO0VBQ0EsYUFBQTtBQ0FOOztBREdJO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLG9EQUFBO0VBQ0EseUNBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQ0FOOztBREdDO0VBQ0ssa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FDQU47O0FESUk7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0FDRE4iLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiICAuYmctZ3JhZGllbnQge1xuICAgICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDE0MGRlZywgIzYzOTI5OSwgIzYzOTI5OCk7XG4gICAgICBib3gtc2hhZG93OiAwcHggMnB4IDJweCAjMDAwMDAwMjQ7XG4gICAgfVxuICBcbiAgICAucm93IHtcbiAgICAgIG1hcmdpbi1sZWZ0OiAwcHggIWltcG9ydGFudDtcbiAgICAgIG1hcmdpbi1yaWdodDogMCAhaW1wb3J0YW50O1xuICAgIH1cbiAgXG4gICAgLmxhbmRpbmctcGFnZSB7YmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzL2ljb24vbW1hLWJhY2tncm91bmQtMzc1LnBuZ1wiKTsgXG4gICAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IHRvcCBjZW50ZXI7XG4gICAgICBmb250LWZhbWlseTogXCJNeXJpYWRQcm9SZWd1bGFyXCI7XG4gICAgICAvLyBwYWRkaW5nLXRvcDogMjJweDtcbiAgICAgIHBhZGRpbmctYm90dG9tOiA0OHZoO1xuICAgICAgbWluLWhlaWdodDogY2FsYygxMDB2aCAtIDYwcHgpO1xuICAgICAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIH1cbiAgXG4gICAgLmxvZ28tZGV0YWlsIHtcbiAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgcGFkZGluZzogMCAwO1xuICAgICAgbWF4LXdpZHRoOiA4MCU7XG4gICAgICB3aWR0aDogY2FsYygxMDB2dyAtIDMwcHgpO1xuICAgICAgbWFyZ2luLWxlZnQ6IDE0JTtcbiAgICB9XG4gIFxuICAgIC5taXlhYmktY29udGlhbmVyIHtcbiAgICAgIG1heC13aWR0aDogNzIwcHg7XG4gICAgICBtYXJnaW46IDAgYXV0bztcbiAgICAgIHotaW5kZXg6IDE7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgfVxuICBcbiAgICAubG9naW4tYnRuIHtcbiAgICAgIHotaW5kZXg6IDEwMDA7XG4gICAgICB3aWR0aDogNzUuNzM0JTtcbiAgICAgIHBhZGRpbmc6IDJweCAxMHB4IDhweDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDQsIDE1OSwgMTczLCAwLjczKTtcbiAgICAgIGZvbnQtc2l6ZTogMjdweDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIG1heC13aWR0aDogODB2dztcbiAgICAgIG1hcmdpbjogMCBhdXRvO1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xuICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICB9XG4gIFxuICAgIC5idG4ge1xuICAgICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgICBib3JkZXI6IG5vbmU7XG4gICAgfVxuICBcbiAgICAubGFuZ3VhZ2UtY29uIHtcbiAgICAgIHRvcDogMDtcbiAgICAgIHJpZ2h0OiAwO1xuICAgICAgcGFkZGluZy10b3A6IDVweDtcbiAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICBsZWZ0OiAwO1xuICAgICAgaGVpZ2h0OiBhdXRvO1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgLy8gICBmb250LWZhbWlseTogJGZvbnQtY29uZDtcbiAgICAgIHBhZGRpbmc6IDNweCA1cHg7XG4gICAgICB6LWluZGV4OiAxMDAwO1xuICAgIH1cbiAgXG4gICAgLmZhYkJ0bkFjdGl2ZSB7XG4gICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgICB3aWR0aDogMzBweDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDQsIDE1OSwgMTczLCAwLjczKSAhaW1wb3J0YW50O1xuICAgICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSg0LCAxNTksIDE3MywgMC43Myk7XG4gICAgICBjb2xvcjogI2ZmZjtcbiAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgfVxuICAgXG4gLm1vZGFsLW9wZW4gLm1vZGFsIHtcbiAgICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgICAgIG92ZXJmbG93LXk6IGF1dG87XG4gICAgICBiYWNrZ3JvdW5kOiAjMzMzMztcbiAgICB9XG4gIFxuICAgXG4gICAgLm9mZmVyLWJsb2NrIC5pbWctc2VjIGltZyB7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICBtYXgtd2lkdGg6IDE0MHB4O1xuICAgICAgbWF4LWhlaWdodDogMTQwcHg7XG4gICAgICBoZWlnaHQ6IDQwdnc7XG4gICAgfVxuICBcbiAgIiwiLmJnLWdyYWRpZW50IHtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDE0MGRlZywgIzYzOTI5OSwgIzYzOTI5OCk7XG4gIGJveC1zaGFkb3c6IDBweCAycHggMnB4ICMwMDAwMDAyNDtcbn1cblxuLnJvdyB7XG4gIG1hcmdpbi1sZWZ0OiAwcHggIWltcG9ydGFudDtcbiAgbWFyZ2luLXJpZ2h0OiAwICFpbXBvcnRhbnQ7XG59XG5cbi5sYW5kaW5nLXBhZ2Uge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvaWNvbi9tbWEtYmFja2dyb3VuZC0zNzUucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogdG9wIGNlbnRlcjtcbiAgZm9udC1mYW1pbHk6IFwiTXlyaWFkUHJvUmVndWxhclwiO1xuICBwYWRkaW5nLWJvdHRvbTogNDh2aDtcbiAgbWluLWhlaWdodDogY2FsYygxMDB2aCAtIDYwcHgpO1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi5sb2dvLWRldGFpbCB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBwYWRkaW5nOiAwIDA7XG4gIG1heC13aWR0aDogODAlO1xuICB3aWR0aDogY2FsYygxMDB2dyAtIDMwcHgpO1xuICBtYXJnaW4tbGVmdDogMTQlO1xufVxuXG4ubWl5YWJpLWNvbnRpYW5lciB7XG4gIG1heC13aWR0aDogNzIwcHg7XG4gIG1hcmdpbjogMCBhdXRvO1xuICB6LWluZGV4OiAxO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5sb2dpbi1idG4ge1xuICB6LWluZGV4OiAxMDAwO1xuICB3aWR0aDogNzUuNzM0JTtcbiAgcGFkZGluZzogMnB4IDEwcHggOHB4O1xuICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDQsIDE1OSwgMTczLCAwLjczKTtcbiAgZm9udC1zaXplOiAyN3B4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1heC13aWR0aDogODB2dztcbiAgbWFyZ2luOiAwIGF1dG87XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xuICBib3gtc2hhZG93OiBub25lO1xufVxuXG4uYnRuIHtcbiAgbGluZS1oZWlnaHQ6IDE7XG4gIGJvcmRlcjogbm9uZTtcbn1cblxuLmxhbmd1YWdlLWNvbiB7XG4gIHRvcDogMDtcbiAgcmlnaHQ6IDA7XG4gIHBhZGRpbmctdG9wOiA1cHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3gtc2hhZG93OiBub25lO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIGhlaWdodDogYXV0bztcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDNweCA1cHg7XG4gIHotaW5kZXg6IDEwMDA7XG59XG5cbi5mYWJCdG5BY3RpdmUge1xuICBoZWlnaHQ6IDMwcHg7XG4gIHdpZHRoOiAzMHB4O1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDQsIDE1OSwgMTczLCAwLjczKSAhaW1wb3J0YW50O1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDQsIDE1OSwgMTczLCAwLjczKTtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLm1vZGFsLW9wZW4gLm1vZGFsIHtcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xuICBvdmVyZmxvdy15OiBhdXRvO1xuICBiYWNrZ3JvdW5kOiAjMzMzMztcbn1cblxuLm9mZmVyLWJsb2NrIC5pbWctc2VjIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIG1heC13aWR0aDogMTQwcHg7XG4gIG1heC1oZWlnaHQ6IDE0MHB4O1xuICBoZWlnaHQ6IDQwdnc7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/home/home.page.ts":
  /*!***********************************!*\
    !*** ./src/app/home/home.page.ts ***!
    \***********************************/

  /*! exports provided: HomePage */

  /***/
  function srcAppHomeHomePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePage", function () {
      return HomePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _language_service_language_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../language-service/language-service */
    "./src/app/language-service/language-service.ts");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _marathan_marathanService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../marathan/marathanService */
    "./marathan/marathanService.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

    var HomePage = /*#__PURE__*/function () {
      function HomePage(router, navCtrl, navParams, _MarathonService, languageService, modalCtrl, menuCtrl) {
        _classCallCheck(this, HomePage);

        this.router = router;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._MarathonService = _MarathonService;
        this.languageService = languageService;
        this.modalCtrl = modalCtrl;
        this.menuCtrl = menuCtrl;
        this.getMarathon = [];
        this.langCode = localStorage.getItem("languageType");
        var languageCode = localStorage.getItem("languageType");
        this.languageType = this.languageService.languageCode(languageCode);
      }

      _createClass(HomePage, [{
        key: "ionViewDidLoad",
        value: function ionViewDidLoad() {
          var _this = this;

          this.langCode = 'Rus';

          this._MarathonService.GetMarathons().subscribe(function (response) {
            _this.getMarathon = response.json();
            console.log("get new marathon TEts", _this.getMarathon);
            localStorage.setItem("MarathonName", _this.getMarathon[0].title);
            localStorage.setItem("startDate", _this.getMarathon[0].startDate);
            localStorage.setItem("currentMarathonId", _this.getMarathon[0].id);
            var image = localStorage.setItem("imagePath", _this.getMarathon[0].imagePath);
            console.log(image);
          }, function (error) {
            console.log("errrrr", error.text());
            _this.apiError = error.text();
          });
        }
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.menuCtrl.enable(false);
        }
      }, {
        key: "start",
        value: function start() {
          this.router.navigate(['startmarathon']);
        }
      }, {
        key: "login",
        value: function login() {
          this.router.navigate(['login']);
        }
      }, {
        key: "register",
        value: function register() {
          this.router.navigate(['register']);
        }
      }, {
        key: "english",
        value: function english() {
          localStorage.setItem("languageType", "Eng");
          this.langCode = "Eng";
          this.languageType = this.languageService.languageCode("Eng");
        }
      }, {
        key: "russian",
        value: function russian() {
          localStorage.setItem("languageType", "Rus");
          this.langCode = "Rus";
          this.languageType = this.languageService.languageCode("Rus");
        }
      }, {
        key: "italian",
        value: function italian() {
          localStorage.setItem("languageType", "Ita");
          this.langCode = "Ita";
          this.languageType = this.languageService.languageCode("Ita");
        }
      }]);

      return HomePage;
    }();

    HomePage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavParams"]
      }, {
        type: _marathan_marathanService__WEBPACK_IMPORTED_MODULE_4__["MarathanProvider"]
      }, {
        type: _language_service_language_service__WEBPACK_IMPORTED_MODULE_1__["LanguageServiceProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"]
      }];
    };

    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
      selector: 'app-home',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./home.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./home.page.scss */
      "./src/app/home/home.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavParams"], _marathan_marathanService__WEBPACK_IMPORTED_MODULE_4__["MarathanProvider"], _language_service_language_service__WEBPACK_IMPORTED_MODULE_1__["LanguageServiceProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"]])], HomePage);
    /***/
  }
}]);
//# sourceMappingURL=home-home-module-es5.js.map